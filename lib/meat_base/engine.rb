require 'fog'
require 'carrierwave'
require 'pg'
require 'pg_search'
require 'geoutm'
require 'uuidtools'
require 'mini_exiftool'
require 'mime-types'
require 'gmaps4rails'
require 'rack-pjax'
require 'gon'

module MeatBase
  class Engine < ::Rails::Engine
    config.generators do |g|
      g.test_framework      :rspec,        :fixture => false
      g.fixture_replacement :factory_girl, :dir => 'spec/factories'
      g.assets false
      g.helper false
    end
    config.autoload_paths += %W(#{config.root}/lib)
    config.to_prepare do
      Dir.glob(Rails.root + "app/decorators/**/*_decorator*.rb").each do |c|
        require_dependency(c)
      end
    end
    config.app_middleware.use(Rack::Pjax)
    #allows us to keep migrations local to the engine, while being able to run
    #rake db:migrate from wrapper app. see http://pivotallabs.com/leave-your-migrations-in-your-rails-engines/
    initializer :append_migrations do |app|
      unless app.root.to_s.match root.to_s
        config.paths["db/migrate"].expanded.each do |expanded_path|
          app.config.paths["db/migrate"] << expanded_path
        end
      end
    end
  end
end
