module ImageHelper

  #used in capture_image, historic_capture_image
  def config_for_remote_upload(remote_state)
    if remote_state
      self.image_remote = FilelessIO.new(self.image.file.path)
    else
      #delete remote image and clear field. this is a dynamically created method (meta programming) by the carrierwave uploader
      self.remove_image_remote = true
    end
  end

end
