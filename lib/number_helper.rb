module NumberHelper


  #float? examples: 12.1 is true, 12.0 is true, "12.1" is true, "0012.1" is true, "0.0123" is true, 12 is false
  def float?(flt)
    return false if flt.blank?
    if flt.class.name=='Float'
      return true
    elsif flt.class.name=='Fixnum'
      return false
    elsif flt.class.name=='String'
      if flt=~/^\d+\.\d+$/
        return true
      end
    end
  end

  #returns true if argument is integer like: 12 is true, 12.0 is true, "12" is true, "0012" is true, 12.1 is false
  def integer?(int)
    return false if int.blank?
    if int.class.name=='Fixnum'
      return true
    elsif int.class.name=='Float'
      if int!=0.0 && (int.to_i/int)==1.0
        return true
      elsif int==0.0
        return true
      else
        return false
      end
    elsif int.class.name=='String'
      if int=~/^\d+[\.0+]*$/
        return true
      end
    end
  end

  def int_part(float_var)
    if float_var>0
      return float_var.floor
    else
      return float_var.ceil
    end
  end

  def fractional_part(float_var)
    if float_var>0
      return float_var-int_part(float_var)
    else
      return int_part(float_var)-float_var
    end
  end

  #calculates the number of decimal places in a float 123.1234567 would return 7
  def num_of_decimals(a)
    a = BigDecimal(a.to_s)
    num = 0
    while(a != a.to_i)
      num += 1
      a *= 10
    end
    num
  end


end

#This allows module methods to be called without have to include the whole module e.g. NumberHelper.num_of_decimals
NumberHelper.module_eval do
  module_function(:num_of_decimals)
  #module_function makes the function private, but we want it to be public
  public :num_of_decimals
end
