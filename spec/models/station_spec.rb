# == Schema Information
#
# Table name: stations
#
#  id               :integer          not null, primary key
#  survey_season_id :integer
#  name             :string(255)
#  lat              :float
#  long             :float
#  elevation        :float
#  nts_sheet        :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'spec_helper'

describe Station do
  let(:station) { FactoryGirl.create(:station) }

  it "has a valid default Factory Girl object" do
    expect(station).to be_valid
  end

  it { should allow_value("Station 1").for(:name) }

  describe "validations" do
    it { should_not allow_value("a"*256).for(:name) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:station_owner) }
    specify "a station can have a project as a parent" do
      station.station_owner = FactoryGirl.create(:project)
      expect(station).to be_valid
    end
    specify "a station can have a project as a parent" do
      station.station_owner = FactoryGirl.create(:survey_season)
      expect(station).to be_valid
    end
    it { should validate_uniqueness_of(:name).scoped_to(:station_owner_id) }
  end

  describe do
    subject { FactoryGirl.create(:station, :decimal_lat_long) }
    include_examples "bad input of coordinates configurations"
    include_examples "objects that can have coordinates in decimal format"
  end

  describe do
    subject { FactoryGirl.create(:station, :DMS_lat_long)}
    include_examples "objects that can have coordinates in DMS format"
  end

  describe do
    subject {FactoryGirl.create(:station, :MinDec_lat_long)}
    include_examples "objects that can have coordinates in MinDec format"
  end

  describe do
    subject {FactoryGirl.create(:station, :UTM)}
    include_examples "objects that can have coordinates in UTM format"
  end

  describe "associations" do
    it { should have_many(:visits).dependent(:destroy) }
    it { should have_one(:historic_visit).dependent(:destroy) }
    it { should have_many(:locations).through(:visits) }
    it { should have_many(:unsorted_captures).class_name("Capture").dependent(:destroy) }
    it { should have_many(:located_captures).through(:locations).dependent(:destroy) }
    it { should have_many(:unsorted_in_visit_captures).through(:visits).dependent(:destroy) }
    it { should have_many(:historic_captures).through(:historic_visit) }
    it { should have_many(:unsorted_location_photos).class_name("LocationImage").dependent(:destroy) }
    it { should have_many(:visits_location_photos).through(:visits).dependent(:destroy) }
    it { should have_many(:sorted_location_photos).through(:visits).dependent(:destroy) }
    it { should have_many(:metadata_files).dependent(:destroy) }
  end



  describe "#captures" do
    context "when a capture exists in a location, visit, and station" do
      it "has all the captures under this station" do
        #this will also create capture, location, station, etc
        cap1 = FactoryGirl.create(:capture)
        cap2 = FactoryGirl.create(:capture, capture_owner: cap1.capture_owner)
        cap3 = FactoryGirl.create(:capture, capture_owner: cap1.capture_owner.visit)
        cap4 = FactoryGirl.create(:capture, capture_owner: cap1.capture_owner.visit.station)
        stn = cap4.capture_owner
        expect(stn.captures).to match_array([cap1,cap2,cap3,cap4])
      end
    end
    context "when a capture exists in a location and visit" do
      it "has all the captures under this station" do
        cap1 = FactoryGirl.create(:capture)
        cap2 = FactoryGirl.create(:capture, capture_owner: cap1.capture_owner.visit)
        stn = cap1.capture_owner.visit.station
        expect(stn.captures).to match_array([cap1,cap2])
      end
    end
    context "when a capture exists in a location and station" do
      it "has all the captures under this station" do
        cap1 = FactoryGirl.create(:capture)
        cap2 = FactoryGirl.create(:capture, capture_owner: cap1.capture_owner.visit.station)
        stn = cap1.capture_owner.visit.station
        expect(stn.captures).to match_array([cap1,cap2])
      end
    end
    context "when a capture exists in a location" do
      it "has all the captures under this station" do
        cap1 = FactoryGirl.create(:capture)
        stn = cap1.capture_owner.visit.station
        expect(stn.captures).to match_array([cap1])
      end
    end
    context "when a capture exists in a station only" do
      it "has all the captures under this station" do
        cap1 = FactoryGirl.create(:capture, :owned_by_visit)
        stn = cap1.capture_owner.station
        expect(stn.captures).to match_array([cap1])
      end
    end
    context "when a capture exists in two separate locations" do
      it "has all the captures under this station" do
        cap1 = FactoryGirl.create(:capture)
        visit = FactoryGirl.create(:visit, station: cap1.capture_owner.visit.station)
        location = FactoryGirl.create(:location, visit: visit)
        cap2 = FactoryGirl.create(:capture, capture_owner: location)
        stn = cap1.capture_owner.visit.station
        expect(stn.captures).to match_array([cap1,cap2])
      end
    end
  end


  describe "scopes" do
    let(:visit) { FactoryGirl.create(:visit, station: station)}
    let(:location) { FactoryGirl.create(:location, visit: visit)}
    let(:historic_visit) { FactoryGirl.create(:historic_visit, station: station)}
    #create another station that doesn't have any captures/hcaptures underneath it
    before(:each) { FactoryGirl.create(:station)}
    describe "remotely_uploaded" do
      context "capture is sorted under location and is remote" do
        before(:each) do
          c = FactoryGirl.create(:capture, :with_images, capture_owner: location)
          c.capture_images.first.update_attributes({remote: true})
        end
        it "returns the station" do
          expect(Station.remotely_uploaded).to eq([station])
        end
      end
      context "capture is unsorted under visit and is remote" do
        before(:each) do
          c = FactoryGirl.create(:capture, :with_images, capture_owner: visit)
          c.capture_images.first.update_attributes({remote: true})
        end
        it "returns the station" do
          expect(Station.remotely_uploaded).to eq([station])
        end
      end
      context "historic capture is remote" do
        before(:each) do
          h = FactoryGirl.create(:historic_capture, :with_images, capture_owner: historic_visit)
          h.capture_images.first.update_attributes({remote: true})
        end
        it "returns the station" do
          expect(Station.remotely_uploaded).to eq([station])
        end
      end
    end
    describe "has_caps_with_missing_comparisons" do
      context "capture is sorted under location" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: location) }
        it "returns the station" do
          expect(Station.has_caps_with_missing_comparisons).to eq([station])
        end
      end
      context "capture is unsorted under visit" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: visit) }
        it "returns the station" do
          expect(Station.has_caps_with_missing_comparisons).to eq([station])
        end
      end
      context "capture is unsorted under station" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: station) }
        it "returns the station" do
          expect(Station.has_caps_with_missing_comparisons).to eq([station])
        end
      end
      context "no captures" do
        it "returns the no stations" do
          expect(Station.has_caps_with_missing_comparisons).to be_empty
        end
      end
      context "captures with comparisons" do
        before(:each) { FactoryGirl.create(:capture, :with_comparisons, capture_owner: station) }
        it "returns the no stations" do
          expect(Station.has_caps_with_missing_comparisons).to be_empty
        end
      end
    end
    describe "has_hcaps_with_missing_comparisons" do
      context "when station has hcaps without comparisons, but the station hasn't be repeated" do
        before(:each) { FactoryGirl.create(:historic_capture, capture_owner: historic_visit) }
        it "returns no stations" do
          expect(Station.has_hcaps_with_missing_comparisons).to be_empty
        end
      end
      context "when station has hcaps missing comparisons and it has been repeated" do
        before(:each) do
          FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
          FactoryGirl.create(:capture, capture_owner: historic_visit.station)
        end
        it "returns the station" do
          expect(Station.has_hcaps_with_missing_comparisons).to eq([station])
        end
      end
      context "when no historic captures" do
        it "returns no stations" do
          expect(Station.has_hcaps_with_missing_comparisons).to be_empty
        end
      end
      context "when hcaps have comparisons" do
        before(:each) { FactoryGirl.create(:historic_capture, :with_comparisons, capture_owner: historic_visit) }
        it "returns no stations" do
          expect(Station.has_hcaps_with_missing_comparisons).to be_empty
        end
      end
    end
    describe "has_caps_without_capture_images" do
      context "when station has a capture without an image" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: location)}
        it "returns the station" do
          expect(Station.has_caps_without_capture_images).to eq([station])
        end
      end
      context "when station has a capture with an image" do
        before(:each) { FactoryGirl.create(:capture, :with_images, capture_owner: location)}
        it "returns no stations" do
          expect(Station.has_caps_without_capture_images).to be_empty
        end
      end
      context "when station has a capture with an image and one without an image" do
        before(:each) do
          FactoryGirl.create(:capture, capture_owner: location)
          FactoryGirl.create(:capture, :with_images, capture_owner: location)
        end
        it "returns the station" do
          expect(Station.has_caps_without_capture_images).to eq([station])
        end
      end
      context "when station has no captures" do
        it "returns no stations" do
          expect(Station.has_caps_without_capture_images).to be_empty
        end
      end
    end
    describe "has_hcaps_without_capture_images" do
      context "when station has a hcapture without an image" do
        before(:each) { FactoryGirl.create(:historic_capture, capture_owner: historic_visit)}
        it "returns the station" do
          expect(Station.has_hcaps_without_capture_images).to eq([station])
        end
      end
      context "when station has a hcapture with an image" do
        before(:each) { FactoryGirl.create(:historic_capture, :with_images, capture_owner: historic_visit)}
        it "returns no stations" do
          expect(Station.has_hcaps_without_capture_images).to be_empty
        end
      end
      context "when station has a hcapture with an image and one without an image" do
        before(:each) do
          FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
          FactoryGirl.create(:historic_capture, :with_images, capture_owner: historic_visit)
        end
        it "returns the station" do
          expect(Station.has_hcaps_without_capture_images).to eq([station])
        end
      end
      context "when station has no hcaptures" do
        it "returns no stations" do
          expect(Station.has_hcaps_without_capture_images).to be_empty
        end
      end
    end
    describe "with_captures" do
      context "when captures exist" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: location)}
        it "returns the station" do
          expect(Station.with_captures).to eq([station])
        end
      end
      context "when captures do not exist" do
        it "returns no stations" do
          expect(Station.with_captures).to be_empty
        end
      end
    end
    describe "without_captures" do
      context "when captures exist" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: location)}
        it "returns no station with captures" do
          expect(Station.without_captures).to_not include(station)
        end
      end
      context "when captures do not exist" do
        it "returns the station" do
          expect(Station.without_captures).to include(station)
        end
      end
    end
    describe "with_hcaptures" do
      context "when hcaptures exist" do
        before(:each) { FactoryGirl.create(:historic_capture, capture_owner: historic_visit)}
        it "returns the station" do
          expect(Station.with_hcaptures).to eq([station])
        end
      end
      context "when hcaptures do not exist" do
        it "returns no stations" do
          expect(Station.with_hcaptures).to be_empty
        end
      end
    end
    describe "without_hcaptures" do
      context "when hcaptures exist" do
        before(:each) { FactoryGirl.create(:historic_capture, capture_owner: historic_visit)}
        it "does not return station with historic captures" do
          expect(Station.without_hcaptures).to_not include(station)
        end
      end
      context "when hcaptures do not exist" do
        it "returns the station without hcaptures" do
          expect(Station.without_hcaptures).to include(station)
        end
      end
    end
    describe "not_repeated" do
      context "when historic captures exists and no captures exist" do
        before(:each) { FactoryGirl.create(:historic_capture, capture_owner: historic_visit) }
        it "returns the station" do
          expect(Station.not_repeated).to include(station)
        end
      end
      context "when both historic captures and captures exist" do
        before(:each) do
          FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
          FactoryGirl.create(:capture, capture_owner: location)
        end
        it "does not return station" do
          expect(Station.not_repeated).to_not include(station)
        end
      end
      context "when station does not have historic captures or captures" do
        it "does not return station" do
          expect(Station.not_repeated).to_not include(station)
        end
      end
    end
    describe "has_visits_with_not_transcribed_but_with_fns" do
      context "when station's visit has scanned field notes but not transcribed" do
        before(:each) do
          visit = FactoryGirl.create(:visit, :with_field_notes)
          visit.station = station
          visit.date = Date.today
          visit.save!
        end
        it "returns the station" do
          expect(Station.has_visits_with_not_transcribed_but_with_fns).to include(station)
        end
      end
      context "when station's visit has no scanned field notes and is not transcribed" do
        before(:each) { FactoryGirl.create(:visit, date: Date.today)}
        it "does not return the station" do
          expect(Station.has_visits_with_not_transcribed_but_with_fns).to_not include(station)
        end
      end
      context "when station's visit is transcribed" do
        before(:each) { visit }#invokes the visit
        it "does not return the station" do
          expect(Station.has_visits_with_not_transcribed_but_with_fns).to_not include(station)
        end
      end
    end
    describe "has_visits_no_scanned_fns" do
      context "when station's visit does not have scanned field notes" do
        before(:each) do
          visit = FactoryGirl.create(:visit, date: Date.today)
          visit.station = station
          visit.save!
        end
        it "returns the station" do
          expect(Station.has_visits_no_scanned_fns).to include(station)
        end
      end
      context "when station's visit has scanned field notes" do
        before(:each) do
          visit = FactoryGirl.create(:visit, :with_field_notes)
          visit.station = station
          visit.save!
        end
        it "does not return the station" do
          expect(Station.has_visits_no_scanned_fns).to_not include(station)
        end
      end
      context "when the station does not have a visit" do
        it "does not return the station" do
          expect(Station.has_visits_no_scanned_fns).to_not include(station)
        end
      end
    end
    describe "has_captures_without_azimuths" do
      context "when the station has captures without azimuths" do
        before(:each) do
          capture = FactoryGirl.create(:capture, azimuth: nil, capture_owner: station)
        end
        it "returns the station" do
          expect(Station.has_captures_without_azimuths).to include(station)
        end
      end
      context "when the station has captures with azimuths" do
        before(:each) do
          capture = FactoryGirl.create(:capture, azimuth: 150, capture_owner: station)
        end
        it "does not return the station" do
          expect(Station.has_captures_without_azimuths).to_not include(station)
        end
      end
    end
    describe "located_state" do
      context "when the station has no captures, but has a location" do
        before(:each) do
          station.lat = 90
          station.long = 90
          station.save!
        end
        it "returns the station" do
          expect(Station.located_state).to include(station)
        end
      end
      context "when station has captures and has a location" do
        before(:each) do
          capture = FactoryGirl.create(:capture, capture_owner: station)
          station.lat = 90
          station.long = 90
          station.save!
        end
        it "does not return the station" do
          expect(Station.located_state).to_not include(station)
        end
      end
    end
    describe 'grouped_state' do
      context "when the station has no captures and no location data" do
        before(:each) do
          station.lat=nil
          station.long=nil
          station.save!
        end
        it "returns the station" do
          expect(Station.grouped_state).to include(station)
        end
      end
      context "when the station has captures" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: station)}
        it "does not return the station" do
          expect(Station.grouped_state).to_not include(station)
        end
      end
      context "when the station has a location" do
        before(:each) { station.lat=90; station.long=90; station.save! }
        it "does not return the station" do
          expect(Station.grouped_state).to_not include(station)
        end
      end
    end
    describe 'repeated_state' do
      context "when the station has captures that aren't mastered" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: station) }
        it "returns the station" do
          expect(Station.repeated_state).to include(station)
        end
      end
      context "when the station has captures that are mastered" do
        before(:each) do
          hvisit = FactoryGirl.create(:historic_visit, station: station)
          capture = FactoryGirl.create(:capture, capture_owner: station)
          hcapture = FactoryGirl.create(:historic_capture, capture_owner: hvisit)
          hcapture.add_comparison(capture)
          #give both the capture and hcapture the same image, with the same dimensions
          #so that it will be mastered
          FactoryGirl.create(:repeat_image, captureable: capture)
          FactoryGirl.create(:repeat_image, captureable: hcapture)
        end
        it "does not return the station" do
          expect(Station.repeated_state).to_not include(station)
        end
      end
    end
    describe 'mastered_state' do
      context "when the station has captures that are mastered" do
        before(:each) do
          hvisit = FactoryGirl.create(:historic_visit, station: station)
          capture = FactoryGirl.create(:capture, capture_owner: station)
          hcapture = FactoryGirl.create(:historic_capture, capture_owner: hvisit)
          hcapture.add_comparison(capture)
          #give both the capture and hcapture the same image, with the same dimensions
          #so that it will be mastered
          FactoryGirl.create(:repeat_image, captureable: capture)
          FactoryGirl.create(:repeat_image, captureable: hcapture)
        end
        it "returns the station" do
          expect(Station.mastered_state).to include(station)
        end
      end
      context "when the station has captures that aren't mastered" do
        before(:each) { FactoryGirl.create(:capture, capture_owner: station)}
        it "does not return the station" do
          expect(Station.mastered_state).to_not include(station)
        end
      end
      context "when the station has one capture that is mastered, but others that aren't" do
        before(:each) do
          hvisit = FactoryGirl.create(:historic_visit, station: station)
          capture = FactoryGirl.create(:capture, capture_owner: station)
          capture2 = FactoryGirl.create(:capture, capture_owner: station)
          hcapture = FactoryGirl.create(:historic_capture, capture_owner: hvisit)
          hcapture2 = FactoryGirl.create(:historic_capture, capture_owner: hvisit)
          hcapture.add_comparison(capture)
          hcapture2.add_comparison(capture2)
          #give both the capture and hcapture the same image, with the same dimensions
          #so that it will be mastered
          FactoryGirl.create(:repeat_image, captureable: capture)
          FactoryGirl.create(:repeat_image, captureable: hcapture)
          #make sure the unmastered pair don't have the same resolution
          FactoryGirl.create(:repeat_image, captureable: capture2)
          #I'm current this factory because historic_image has the same dimensions as repeat_image
          #and this one doesn't
          FactoryGirl.create(:repeat_image_with_embedded_gps, captureable: hcapture2)
        end
        it "does not return the station" do
          expect(Station.mastered_state).to_not include(station)
        end
      end
    end
  end

  describe "gmaps4rails" do
    describe "#longitude" do
      context "station has long" do
        before (:each) do
          station.update_attributes({long: 114.829, lat: 50.3})
        end
        it "returns the longitude" do
          expect(station.longitude).to eq 114.829
        end
      end
      context "station's location has long" do
        before (:each) do
          visit = station.visits.create(date: Date.today)
          location = visit.locations.create(location_identity: "1", long: 115.829, lat: 51.3)
        end
        it "returns the longitude of the location" do
          expect(station.reload.longitude).to eq 115.829
        end
      end
      context "station's capture has long" do
        before (:each) do
          visit = station.visits.create(date: Date.today)
          location = visit.locations.create(location_identity: "1")
          capture = location.captures.create(fn_photo_reference: "asdf", long: 116.829, lat: 52.3)
        end
        it "returns the longitude of the capture" do
          expect(station.reload.longitude).to eq 116.829
        end
      end
    end
    describe "#latitude" do
      context "station has lat" do
        before (:each) do
          station.update_attributes({long: 114.829, lat: 50.3})
        end
        it "returns the latitude" do
          expect(station.latitude).to eq 50.3
        end
      end
      context "station's location has lat" do
        before (:each) do
          visit = station.visits.create(date: Date.today)
          location = visit.locations.create(location_identity: "1", long: 115.829, lat: 51.3)
        end
        it "returns the latitude of the location" do
          expect(station.reload.latitude).to eq 51.3
        end
      end
      context "station's capture has lat" do
        before (:each) do
          visit = station.visits.create(date: Date.today)
          location = visit.locations.create(location_identity: "1")
          capture = location.captures.create(fn_photo_reference: "asdf", long: 116.829, lat: 52.3)
        end
        it "returns the latitude of the capture" do
          expect(station.reload.latitude).to eq 52.3
        end
      end
    end
  end

end
