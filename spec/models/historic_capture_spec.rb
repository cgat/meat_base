
require 'spec_helper'

describe HistoricCapture do
  let(:historic_capture) {FactoryGirl.create(:historic_capture)}

  it "has a valid default FactoryGirl object" do
    expect(historic_capture).to be_valid
  end

  it { should allow_value("123").for(:fn_photo_reference)}
  it { should allow_value(2).for(:f_stop)}
  it { should allow_value("1/100").for(:shutter_speed)}
  it { should allow_value(35).for(:focal_length)}
  it { should allow_value(DateTime.now).for(:capture_datetime)}
  it { should allow_value("canada").for(:digitization_location)}
  it { should allow_value(DateTime.now).for(:digitization_datetime)}
  it { should allow_value("e12356785").for(:lac_ecopy)}
  it { should allow_value("WO55").for(:lac_wo)}
  it { should allow_value("Num2").for(:lac_collection)}
  it { should allow_value("BoxA").for(:lac_box)}
  it { should allow_value("CatB").for(:lac_catalogue)}
  it { should allow_value("good").for(:condition)}
  it { should allow_value("this is comment").for(:comments)}

  describe 'validations' do
    it { should_not allow_value("a"*256).for(:fn_photo_reference) }
    it { should validate_uniqueness_of(:fn_photo_reference).scoped_to([:capture_owner_id, :capture_owner_type]) }
    it { should_not allow_value("a"*256).for(:digitization_location) }
    it { should_not allow_value("a"*256).for(:lac_ecopy) }
    it { should_not allow_value("a"*256).for(:lac_wo) }
    it { should_not allow_value("a"*256).for(:lac_collection) }
    it { should_not allow_value("a"*256).for(:lac_box) }
    it { should_not allow_value("a"*256).for(:lac_catalogue) }
    it { should_not allow_value("a"*256).for(:comments) }
  end

  describe "associations" do
    it { should have_many(:capture_images).dependent(:destroy) }
    it { should have_many(:captures).through(:comparison_indices) }
  end

  describe '#add_comparison' do
    it "can use the add_comparison method to create comparisons" do
      historic_capture.add_comparison(FactoryGirl.create(:capture))
      historic_capture.add_comparison(FactoryGirl.create(:capture))
      expect(historic_capture.captures.size).to eq(2)
    end

    it "does not make two comparisons when adding the same capture on the association twice" do
      c = FactoryGirl.create(:capture)
      historic_capture.add_comparison(c)
      historic_capture.save!
      historic_capture.add_comparison(c)
      historic_capture.save!
      expect(historic_capture.captures).to eq([c])
    end
  end
  describe '#remove_comparison' do
    it "can use the remove_comparison method to create comparisons" do
      c = FactoryGirl.create(:capture)
      historic_capture.add_comparison(FactoryGirl.create(:capture))
      historic_capture.add_comparison(c)
      historic_capture.remove_comparison(c)
      expect(historic_capture.captures.size).to eq(1)
    end
  end
  describe "#survey_season" do
    context "when the parent is a HistoricVisit" do
      it "returns the survey_season" do
        expect(historic_capture.survey_season).to eq(historic_capture.capture_owner.station.station_owner)
      end
    end
    context "when the parent is a Station" do
      it "returns the survey_season" do
        historic_capture.capture_owner = FactoryGirl.create(:station)
        historic_capture.save!
        expect(historic_capture.survey_season).to eq(historic_capture.capture_owner.station_owner)
      end
    end
    context "when it's unsorted" do
      it "returns nil" do
        historic_capture.capture_owner = FactoryGirl.create(:project)
        historic_capture.save!
        expect(historic_capture.survey_season).to be_blank
      end
    end
  end

  describe "#year" do
    context "when it has a survey_season" do
      it "returns the year of the survey_season" do
        expect(historic_capture.year).to eq(historic_capture.survey_season.year)
      end
    end
    context "when it does not have a survey_season" do
      it "returns nil" do
        historic_capture.capture_owner = FactoryGirl.create(:project)
        historic_capture.save!
        expect(historic_capture.year).to be_blank
      end
    end
  end


  describe "scopes" do
    describe "missing_comparisons" do
      context "a historic capture without comparisons exists" do
        let!(:historic_cap_without_comp) { FactoryGirl.create(:historic_capture)}
        let!(:historic_cap_with_comp) { FactoryGirl.create(:historic_capture, :with_comparisons) }
        it "returns only the historic capture with comparisons" do
          HistoricCapture.missing_comparisons.to_a.should =~ [historic_cap_without_comp]
        end
      end
      context "all historic captures have comparisons" do
        let!(:cap_with_comp1) { FactoryGirl.create(:historic_capture, :with_comparisons) }
        let!(:cap_with_comp2) { FactoryGirl.create(:historic_capture, :with_comparisons) }
        it "returns an empty array" do
          HistoricCapture.missing_comparisons.should be_empty
        end
      end
    end
    describe "without_capture_images" do
      context "hcaptures exist and do not have capture images" do
        let!(:hcap1) { FactoryGirl.create(:historic_capture)}
        let!(:hcap2) { FactoryGirl.create(:historic_capture)}
        it "returns both captures" do
          (HistoricCapture.without_capture_images).should =~ [hcap1,hcap2]
        end
      end
      context "captures exist and have capture images" do
        let!(:cap1) { FactoryGirl.create(:historic_capture, :with_images)}
        let!(:cap2) { FactoryGirl.create(:historic_capture, :with_images)}
        it "returns an empty array" do
          expect(HistoricCapture.without_capture_images).to be_empty
        end
      end
    end
    describe "remotely_uploaded" do
      context "historic_captures exist and do not have capture images" do
        let!(:cap1) { FactoryGirl.create(:historic_capture)}
        let!(:cap2) { FactoryGirl.create(:historic_capture)}
        it "returns an empty array" do
          expect(HistoricCapture.remotely_uploaded.length).to eq(0)
        end
      end
      describe "historic_captures with capture images" do
        let!(:cap1) { FactoryGirl.create(:historic_capture, :with_images)}
        let!(:cap2) { FactoryGirl.create(:historic_capture, :with_images)}
        context "but images are not uploaded remotely" do
          it "returns an empty array" do
            expect(HistoricCapture.remotely_uploaded.length).to eq(0)
          end
        end
        context "where only one image is remotely uploaded" do
          before(:each) do
            cap1.reload.capture_images.first.update_attributes({remote: true})
            cap2.reload.capture_images.first.update_attributes({remote: true})
          end
          it "returns both historic_captures" do
            (HistoricCapture.remotely_uploaded).should =~ [cap1,cap2]
          end
        end
        context "where all images are uploaded" do
          before(:each) do
            cap1.reload.capture_images.each{|a| a.update_attributes({remote: true}) }
            cap2.reload.capture_images.each{|a| a.update_attributes({remote: true}) }
          end
          it "returns both histoirc captures" do
            (HistoricCapture.remotely_uploaded).should =~ [cap1,cap2]
          end
        end
      end
    end
    describe "mastered" do
      it "returns mastered historic captures" do
        hcap_mastered = FactoryGirl.create(:historic_capture, :mastered)
        (HistoricCapture.mastered).should =~ [hcap_mastered]
      end
      it "does not return captures with comparisons but not mastered" do
        hcap_unmastered = FactoryGirl.create(:historic_capture, :with_comparison_with_images)
        (HistoricCapture.mastered).should be_empty
      end
    end
    describe "has_comparisons_with_images" do
      it "returns only historic captures that have comparisons which have capture images associated with them"  do
        hcap_compared_with_himage = FactoryGirl.create(:historic_capture, :with_comparison_with_images)
        hcap_compared_without_himage = FactoryGirl.create(:historic_capture, :with_comparisons)
        (HistoricCapture.has_comparisons_with_images).should =~ [hcap_compared_with_himage]
      end
      it "does not return historic captures that have comparisons which do not have capture images associated with them" do
        hcap_compared_without_himage = FactoryGirl.create(:historic_capture, :with_comparisons)
        (HistoricCapture.has_comparisons_with_images).should be_empty
      end
    end
  end

  describe "merging" do
    context "a capture with attributes to a capture without" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture, :with_all_fields) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture) }
      end
    end
    context "a capture without attributes to a capture with" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture, :with_all_fields) }
      end
    end
    context "a capture with capture images to a capture without" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture, :with_images, :with_all_fields) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture) }
      end
    end
    context "a capture without capture images to a capture with" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture, :with_images, :with_all_fields) }
      end
    end
    context "both captures have capture_images" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture, :with_images, :with_all_fields) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture, :with_images) }
      end
    end
    context "a capture with comparisons to a capture without" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture, :with_comparisons)  }
      end
    end
    context "a capture without comparisons to a capture with" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture, :with_comparisons) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture) }
      end
    end
    context "unmergeable captures" do
      include_examples "mergeing unmergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:historic_capture, :with_all_fields) }
        let(:capture_argument) { FactoryGirl.create(:historic_capture, :with_all_fields) }
      end
    end
  end
end
