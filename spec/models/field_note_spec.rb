require 'spec_helper'

describe FieldNote do
  let(:field_note) { FactoryGirl.create(:field_note) }

  it "has a valid default FactoryGirl object" do
    expect(field_note).to be_valid
  end

  it { should allow_value(fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))).for(:field_note_file) }

  describe 'validates' do
    it { should validate_presence_of(:field_note_file) }
    it { should validate_presence_of(:visit) }
  end

  describe 'uploading' do
    let(:field_note_pdf) { FactoryGirl.create(:field_note_transcribed_pdf)}
    it "has a (pdf) file in the file system when it is created" do
      expect(File.exists?(field_note_pdf.field_note_file.file.path)).to eq(true)
    end
    it "does not allow duplicate field note files in the same visit" do
      expect {FactoryGirl.create(:field_note_transcribed_pdf, field_note_file: fixture_file_upload(field_note_pdf.field_note_file.file.path), visit_id: field_note_pdf.visit_id)}.to raise_error(ActiveRecord::RecordNotUnique)
    end
  end

end
