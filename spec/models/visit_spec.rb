# == Schema Information
#
# Table name: visits
#
#  id                          :integer          not null, primary key
#  station_id                  :integer
#  date                        :date
#  start_time                  :time
#  finish_time                 :time
#  pilot                       :string(255)
#  rw_call_sign                :string(255)
#  photographer_participant_id :integer
#  fn_author_participant_id    :integer
#  visit_narrative             :text
#  illustration                :boolean
#  weather_narrative           :text
#  weather_temp                :float
#  weather_ws                  :float
#  weather_gs                  :float
#  weather_pressure            :float
#  weather_rh                  :float
#  weather_wb                  :float
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#

require 'spec_helper'

describe Visit do
  let(:visit) { FactoryGirl.create(:visit) }
  it "has valid FactoryGirl object" do
    expect(visit).to be_valid
  end

  describe 'validations' do
    specify "that start time cannot come after finish time" do
      visit.finish_time = DateTime.now-200
      visit.start_time = DateTime.now
      expect(visit).to_not be_valid
    end
    it { should_not allow_value(Date.today+3.days).for(:date) }
    it { should validate_uniqueness_of(:date).scoped_to(:station_id) }
    it { should validate_presence_of(:station) }
    it { should_not allow_value(-1).for(:weather_ws) }
    it { should allow_value(0).for(:weather_ws) }
    it { should_not allow_value(-1).for(:weather_gs) }
    it { should allow_value(0).for(:weather_gs) }
    it { should_not allow_value(-1).for(:weather_pressure) }
    it { should allow_value(0).for(:weather_pressure) }
    it { should_not allow_value(-51).for(:weather_wb) }
    it { should allow_value(0).for(:weather_wb) }
    it { should_not allow_value(-1).for(:weather_rh) }
    it { should allow_value(0).for(:weather_rh) }
    it { should_not allow_value(101).for(:weather_rh) }
    it { should allow_value(100).for(:weather_rh) }
    it { should_not allow_value("a"*21).for(:pilot) }
    it { should allow_value("string").for(:pilot) }
    it { should_not allow_value("a"*31).for(:rw_call_sign) }
    it { should allow_value("string").for(:rw_call_sign) }
    it { should allow_value("Book 4, 2008, pg. 28-32").for(:fn_physical_location) }
    it { should allow_value("Binder 1, 2009").for(:fn_physical_location) }
    it { should allow_value("").for(:fn_physical_location) }
    it { should allow_value("this is a comment").for(:fn_transcription_comment)}
  end

  describe 'associations' do
    it { should have_many(:locations).dependent(:destroy) }
    it { should have_and_belong_to_many(:photographers).class_name("Participant") }
    it { should_not have_and_belong_to_many(:photographers).dependent(:destroy) }
    it { should have_and_belong_to_many(:fn_authors).class_name("Participant") }
    it { should_not have_and_belong_to_many(:fn_authors).dependent(:destroy) }
    it { should have_many(:hiking_parties).dependent(:destroy) }
    it { should have_many(:hiking_party_participants).through(:hiking_parties) }
    it { should have_many(:keyword_visit_associations).dependent(:destroy) }
    it { should have_many(:keywords).through(:keyword_visit_associations) }
    it { should have_many(:field_notes).dependent(:destroy) }
    it { should have_many(:unsorted_captures).class_name("Capture").dependent(:destroy) }
    it { should have_many(:unsorted_location_photos).class_name("LocationImage").dependent(:destroy) }
    it { should have_many(:sorted_location_photos).through(:locations) }
    it { should have_many(:located_captures).through(:locations) }
    it { should belong_to(:station) }
    it { should have_many(:metadata_files).dependent(:destroy) }
  end


  describe "scopes" do
    let(:location) { FactoryGirl.create(:location, visit_id: visit.id)}
    #create another visit that doesn't have any captures underneath it
    before(:each) { FactoryGirl.create(:visit)}
    describe "remotely_uploaded" do
      context "capture is sorted under location and is remote" do
        before(:each) do
          c = FactoryGirl.create(:capture, :with_images, capture_owner: location)
          c.capture_images.first.update_attributes({remote: true})
        end
        it "returns the visit" do
          expect(Visit.remotely_uploaded).to eq([visit])
        end
      end
      context "capture is unsorted under visit and is remote" do
        before(:each) do
          c = FactoryGirl.create(:capture, :with_images, capture_owner: visit)
          c.capture_images.first.update_attributes({remote: true})
        end
        it "returns the visit" do
          expect(Visit.remotely_uploaded).to eq([visit])
        end
      end
    end
    describe "not_transcribed" do
      context "when visit is not transcribed" do
        let(:not_transcribed) { FactoryGirl.create(:visit, date: Date.today) }
        it "returns the visit" do
          expect(Visit.not_transcribed).to include(not_transcribed)
        end
      end
      context "when visit has been transcribed" do
        it "does not return the visit" do
          visit_with_fields = FactoryGirl.create(:visit, :with_all_fields)
          expect(Visit.not_transcribed).to_not include(visit_with_fields)
        end
      end
    end
    describe "without_scanned_field_notes" do
      context "when visit has no scanned field notes" do
        it "returns the station" do
          expect(Visit.without_scanned_field_notes).to include(visit)
        end
      end
      context "when visit has scanned field notes" do
        let!(:with_fns) { FactoryGirl.create(:visit, :with_field_notes) }
        it "does not return visit" do
          expect(Visit.without_scanned_field_notes).to_not include(with_fns)
        end
      end
    end
  end
end
