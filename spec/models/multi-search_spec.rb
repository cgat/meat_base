require 'spec_helper'

shared_examples_for "multi-search" do |searchable_columns|
  searchable_columns.each do |column|
    it "returns the searched for object when searching exactly on the value of column '#{column}' " do
      results = PgSearch.multisearch(search_object.send(column))
      expect(results.map(&:searchable)).to include(search_object)
      expect(results.map(&:searchable)).to_not include(other)
    end
    it "returns the searched for object when searching on partial elements of the column '#{column}' " do
      results = PgSearch.multisearch(search_object.send(column).to_s.split(" ").first)
      expect(results.map(&:searchable)).to include(search_object)
      expect(results.map(&:searchable)).to_not include(other)
    end
  end
  it "returns the searched for object when searching on a combination of the searchable column values" do
    results = PgSearch.multisearch(searchable_columns.map{|column| search_object.send(column).to_s.split(" ").first}.join(" "))
    expect(results.map(&:searchable)).to include(search_object)
    expect(results.map(&:searchable)).to_not include(other)
  end
end
shared_examples_for "multi-search with associations" do |searchable_associations|
  searchable_associations.each do |ass_obj, columns|
    columns.each do |column|
      it "returns the searched for object by searching an association's (#{ass_obj}) column '#{column}' " do
        results = PgSearch.multisearch(self.send(ass_obj).send(column))
        expect(results.map(&:searchable)).to include(search_object)
        expect(results.map(&:searchable)).to_not include(other)
      end
    end
  end
end
describe "Multi-search" do
  it "searches for word prefixes" do
    p = FactoryGirl.create(:project, name: "name")
    expect(PgSearch.multisearch("nam").first.searchable).to eq p
  end
  it "searches with stemming" do
    p = FactoryGirl.create(:project, name: "name")
    expect(PgSearch.multisearch("names").first.searchable).to eq p
  end
  describe Project do
    let(:search_object) { FactoryGirl.create(:project, name: "Project Name1", description: "Describing Project")}
    let(:other) { FactoryGirl.create(:project)}
    include_examples "multi-search", [:name, :description]
  end
  describe Surveyor do
    let(:search_object) { FactoryGirl.create(:surveyor, last_name: "special name", given_names: "patrick fitzgerald")}
    let(:other) { FactoryGirl.create(:surveyor)}
    include_examples "multi-search", [:last_name, :given_names]
  end
  describe Survey do
    let(:search_object) { FactoryGirl.create(:survey, name: "Survey Name1")}
    let(:other) { FactoryGirl.create(:survey)}
    include_examples "multi-search", [:name]
  end
  describe SurveySeason do
    let(:search_object) { FactoryGirl.create(:survey_season, geographic_coverage: "Alberta Rockies")}
    let(:other) { FactoryGirl.create(:survey_season)}
    include_examples "multi-search", [:geographic_coverage]
  end
  describe Station do
    let(:search_object) { FactoryGirl.create(:station, name: "Station 25.", nts_sheet: "sheet 123")}
    let(:other) { FactoryGirl.create(:station)}
    include_examples "multi-search", [:name, :nts_sheet]
  end
  describe Visit do
    let(:search_object) { FactoryGirl.create(:visit, visit_narrative: "Narration words visit.", weather_narrative: "cloudly sunny cloudly periods")}
    let(:other) { FactoryGirl.create(:visit)}
    include_examples "multi-search", [:visit_narrative, :weather_narrative, :date]
    describe "search through associations" do
      let(:location) { search_object.locations.create(location_identity: "A", location_narrative: "3 feet away carin")}
      let(:keyword) { search_object.keywords.create(keyword: "keyword333")}
      let(:participant) { search_object.hiking_party_participants.create(given_names: "Tommy", last_name: "Smith")}
      include_examples "multi-search with associations", {:location => [:location_narrative], :keyword => [:keyword], :participant => [:given_names, :last_name] }
      it "can handle nil values in assocated columns" do
        loc2 = search_object.locations.create(location_identity: "Z", location_narrative: nil)
        expect(PgSearch.multisearch("carin")).to_not include(loc2)
      end
    end
  end
  describe Capture do
    let(:search_object) { FactoryGirl.create(:capture, fn_photo_reference: "A00012342")}
    let(:other) { FactoryGirl.create(:capture)}
    include_examples "multi-search", [:fn_photo_reference]
  end
  describe HistoricCapture do
    let(:search_object) { FactoryGirl.create(:historic_capture, fn_photo_reference: "A00012342")}
    let(:other) { FactoryGirl.create(:historic_capture)}
    include_examples "multi-search", [:fn_photo_reference]
  end
  describe CaptureImage do
    let!(:search_object) { FactoryGirl.create(:repeat_image) }
    let(:other) { FactoryGirl.create(:repeat_image)}
    include_examples "multi-search", [:image_state]
    it "is searcahble by the original image name" do
      expect(PgSearch.multisearch(search_object.display_name).first.searchable).to eq search_object
    end
  end
  describe LocationImage do
    let(:search_object) { FactoryGirl.create(:location_image)}
    let(:other) { FactoryGirl.create(:location_image)}
    it "is searcahble by the original image name" do
      expect(PgSearch.multisearch(search_object.display_name).first.searchable).to eq search_object
    end
  end

end
