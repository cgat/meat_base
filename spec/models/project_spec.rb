require 'spec_helper'

describe Project do
  let(:project) { FactoryGirl.create(:project) }

  it "has a valid default FactoryGirl object" do
    expect(project).to be_valid
  end

  it { should allow_value("project name1").for(:name) }
  it { should allow_value("project description").for(:description) }

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should_not allow_value("a"*256).for(:name) }
  end

  describe 'associations' do
    it { should have_many(:stations).dependent(:destroy) }
    it { should have_many(:unsorted_captures).dependent(:destroy) }
    it { should have_many(:unsorted_hcaptures).dependent(:destroy) }
    it { should have_many(:scenics).dependent(:destroy) }
  end

end
