require 'spec_helper'

describe Camera do
  it { should allow_value("Hasselblad").for(:model) }
  it { should allow_value("2").for(:unit) }
  it { should allow_value("Digital").for(:format) }
end
