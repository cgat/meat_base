require 'spec_helper'


describe Image do
  let(:scenic_image) { FactoryGirl.create(:scenic_image) }
  let(:location_image) { FactoryGirl.create(:location_image)}
  #the default will be the scenic image. The use of scenic and location image will be used in the few cases that are specific to those image types
  let(:image) { scenic_image }
  it "has a valid default (scenic) FactoryGirl object" do
    expect(scenic_image).to be_valid
  end
  it "has a valid default (location image) FactoryGirl object" do
    expect(location_image).to be_valid
  end

  describe 'validations' do
    it { should validate_presence_of(:image) }
  end

  describe do
    subject {image}
    include_examples "camera info metadata"
  end
  describe do
    subject { FactoryGirl.create(:scenic_image, :with_embedded_gps) }
    include_examples "camera info metadata"
    include_examples "objects that can have coordinates in decimal format"
  end

  describe do
    subject { FactoryGirl.create(:image, :without_gps, :decimal_lat_long) }
    include_examples "bad input of coordinates configurations"
    include_examples "objects that can have coordinates in decimal format"
  end

  describe do
    subject {FactoryGirl.create(:image, :without_gps, :DMS_lat_long)}
    include_examples "objects that can have coordinates in DMS format"
  end

  describe do
    subject {FactoryGirl.create(:image, :without_gps, :MinDec_lat_long)}
    include_examples "objects that can have coordinates in MinDec format"
  end

  describe do
    subject {FactoryGirl.create(:image, :without_gps, :UTM)}
    include_examples "objects that can have coordinates in UTM format"
  end

  describe "uploading" do
    subject { FactoryGirl.create(:location_image) }
    it_behaves_like "an image that intially uploaded locally, but will be later be uploaded remotely"
    it_behaves_like "locally stored image"
    it_behaves_like "extraction of file related metadata from image"
    context "with remote storage as well" do
      subject { FactoryGirl.create(:location_image, remote: "1") }
      it_behaves_like "remotely stored image"
    end
  end

end
