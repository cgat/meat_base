require 'spec_helper'

describe MetadataFile do
  let(:metadata) { FactoryGirl.create(:metadata_file)}

  it "has a valid default FactoryGirl object" do
    expect(metadata).to be_valid
  end

  it { should allow_value(fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))).for(:metadata_file) }

  describe 'validations' do
    it { should_not allow_value(nil).for(:metadata_file) }
  end
end
