# == Schema Information
#
# Table name: surveyors
#
#  id          :integer          not null, primary key
#  last_name   :string(255)
#  given_names :string(255)
#  short_name  :string(255)
#  affiliation :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

describe Surveyor do
  let(:surveyor) { FactoryGirl.create(:surveyor) }

  it "has a valid default FactoryGirl object" do
    expect(surveyor).to be_valid
  end

  it { should allow_value("Jones").for(:last_name) }
  it { should allow_value("Tom").for(:given_names) }
  it { should allow_value("TJ").for(:short_name) }
  it { should allow_value("Dominion Survey").for(:affiliation) }

  describe 'validations' do
    it { should_not allow_value("a"*256).for(:last_name) }
    it "requires a last name or an affliation" do
      surveyor.last_name = ""
      surveyor.affiliation = ""
      expect(surveyor).to_not be_valid
    end
    specify "a last name is not required if there is an affiliation" do
      surveyor.last_name = ""
      surveyor.affiliation = "Dominion"
      expect(surveyor).to be_valid
    end
    specify "an affiliation is not required if there is a last name" do
      surveyor.affiliation = ""
      expect(surveyor).to be_valid
    end
    it { should validate_uniqueness_of(:last_name).scoped_to(:given_names) }
    it { should validate_uniqueness_of(:given_names).scoped_to(:last_name) }
    specify "affiliations not associated with last or given names are unique" do
      surveyor.last_name = ""
      surveyor.given_names = ""
      surveyor.affiliation = "Dominion"
      surveyor.save!
      surveyor2 = FactoryGirl.build(:surveyor, last_name: "", given_names: "", affiliation: surveyor.affiliation)
      expect(surveyor2).to_not be_valid
    end
    specify "an affiliation is not unique if the names for the surveyor are different" do
      surveyor2 = FactoryGirl.build(:surveyor, affiliation: surveyor.affiliation, short_name: "BDA")
      expect(surveyor2).to be_valid
    end
    it { should validate_uniqueness_of(:short_name) }
    it { should_not allow_value("ABCDE").for(:short_name) }
    it { should allow_value("").for(:short_name) }
    it { should_not allow_value("a"*256).for(:given_names) }
    it { should allow_value("").for(:given_names) }
  end

  describe "associations" do
    it { should have_many(:surveys).dependent(:destroy) }
  end

  describe "#full_name" do
    it "returns the given names and last name concatenated together" do
      surveyor.last_name = "Higgs"
      surveyor.given_names = "Eric"
      surveyor.save!
      expect(surveyor.full_name).to eq("Eric Higgs")
    end
    it "returns the affiliation if there is no name specified" do
      surveyor.last_name = ""
      surveyor.given_names = ""
      surveyor.affiliation = "Royal Engineers"
      expect(surveyor.full_name).to eq("Royal Engineers")
    end
    it "returns just the last name if there is no given names" do
      surveyor.last_name = "Higgs"
      surveyor.given_names = ""
      surveyor.save!
      expect(surveyor.full_name).to eq("Higgs")
    end
  end

end
