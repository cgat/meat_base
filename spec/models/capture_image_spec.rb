# == Schema Information
#
# Table name: capture_images
#
#  id               :integer          not null, primary key
#  capture_id       :integer
#  hash_key         :string(255)
#  file_path :text
#  file_size        :float
#  x_dim            :integer
#  y_dim            :integer
#  bit_depth        :integer
#  image_state      :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

require 'spec_helper'


#Problem. When trying to update the parents of a capture, the module specific callback functions are not being run (specifically :move_filesystem_obj).
#http://stackoverflow.com/questions/13636989/rspec-issuing-a-save-on-an-existing-but-modified-activerecord-does-not-run-be/13637077#comment18708193_13637077
#work around (in shared_filesystemable file) is to explicitly call the function before updating.
describe CaptureImage do

  it { should allow_value(fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))).for(:image) }
  it { should validate_inclusion_of(:image_state).in_array(IMAGE_STATES) }

  describe 'validations' do
    it { should_not allow_value("Raw").for(:image_state) }
    #it { should validate_presence_of(:image) }
  end

  describe "uploading" do
    after(:each) do
      subject.image.remove! if subject.image.present?
      subject.image_remote.remove! if subject.image_remote.present?
    end

    describe "general tests related to storing images remotely" do
      subject { FactoryGirl.create(:repeat_image) }
      it_behaves_like "an image that intially uploaded locally, but will be later be uploaded remotely"
    end

    context "color jpeg" do
      subject { FactoryGirl.create(:repeat_image)}
      it_behaves_like "locally stored image"
      it_behaves_like "extraction of file related metadata from image"
      context "with remote storage as well" do
        subject { FactoryGirl.create(:repeat_image, remote: "1")}
        it_behaves_like "remotely stored image"
      end
    end
  end

  describe '#resolution_similar?' do

    it "should return false when the receiver has no x_dim" do
      cap_img1 = FactoryGirl.create(:repeat_image)
      cap_img1.x_dim = nil
      cap_img2 = FactoryGirl.create(:repeat_image)
      cap_img1.resolution_similar?(cap_img2)
    end

    it "should return false when the receiver has no y_dim" do
      cap_img1 = FactoryGirl.create(:repeat_image)
      cap_img1.y_dim = nil
      cap_img2 = FactoryGirl.create(:repeat_image)
      cap_img1.resolution_similar?(cap_img2)
    end

    it "should return false when the argument has no x_dim" do
      cap_img1 = FactoryGirl.create(:repeat_image)
      cap_img1.x_dim = nil
      cap_img2 = FactoryGirl.create(:repeat_image)
      cap_img2.resolution_similar?(cap_img1)
    end

    it "should return false when the argument has no y_dim" do
      cap_img1 = FactoryGirl.create(:repeat_image)
      cap_img1.y_dim = nil
      cap_img2 = FactoryGirl.create(:repeat_image)
      cap_img2.resolution_similar?(cap_img1)
    end

  end

  describe 'scopes' do
    describe 'remotely_uploaded' do
      let!(:ci1) { FactoryGirl.create(:repeat_image) }
      let!(:ci2) { FactoryGirl.create(:repeat_image) }
      context 'where no capture images are remote' do
        it "return an empty array" do
          expect(CaptureImage.remotely_uploaded.size).to eq(0)
        end
      end
      context "where one capture image is remote" do
        it "returns an array with the remote capture" do
          ci1.reload.remote=true
          ci1.save!
          expect(CaptureImage.remotely_uploaded).to eq([ci1])
        end
      end
    end
  end

end
