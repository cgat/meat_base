require 'spec_helper'

describe Location do
  let(:location) { FactoryGirl.create(:location)}

  it "has a valid default FactoryGirl object" do
    expect(location).to be_valid
  end

  it { should allow_value("1").for(:location_identity) }
  it { should allow_value("narrative adsf").for(:location_narrative) }

  describe 'validations' do
    it { should validate_presence_of(:visit) }
    it { should validate_presence_of(:location_identity) }
    it { should validate_uniqueness_of(:location_identity).scoped_to(:visit_id) }
  end

  describe 'associations' do
    it { should belong_to(:visit) }
    it { should have_many(:location_photos).dependent(:destroy) }
    it { should have_many(:captures).dependent(:destroy) }
  end

  describe '#location_photos=' do
    it "can add multiple location photos from an array of param hashes" do
      photo1 = fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))
      photo2 = fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))
      location.location_photos = [photo1,photo2]
      location.save!
      expect(location.location_photos.size).to eq(2)
    end
    context "when the location is new" do
      it "can add multiple location photos from an array of param hashes" do
        location = FactoryGirl.build(:location)
        photo1 = fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))
        photo2 = fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))
        location.location_photos = [photo1,photo2]
        location.save!
        expect(location.location_photos.size).to eq(2)
      end
    end
  end

  describe do
    subject { FactoryGirl.create(:location, :decimal_lat_long) }
    include_examples "bad input of coordinates configurations"
    include_examples "objects that can have coordinates in decimal format"
  end

  describe do
    subject {FactoryGirl.create(:location, :DMS_lat_long)}
    include_examples "objects that can have coordinates in DMS format"
  end

  describe do
    subject {FactoryGirl.create(:location, :MinDec_lat_long)}
    include_examples "objects that can have coordinates in MinDec format"
  end

  describe do
    subject {FactoryGirl.create(:location, :UTM)}
    include_examples "objects that can have coordinates in UTM format"
  end

end
