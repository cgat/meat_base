# == Schema Information
#
# Table name: keywords
#
#  id         :integer          not null, primary key
#  keyword    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe Keyword do

  it { should allow_value("taco time").for(:keyword) }

  describe 'validations' do
    it { should validate_presence_of(:keyword) }
    it { should_not allow_value("a"*256).for(:keyword) }
  end

  describe 'associations' do
    it { should have_many(:keyword_visit_associations) }
    it { should have_many(:visits).through(:keyword_visit_associations) }
  end

end
