
require 'spec_helper'

describe HistoricVisit do
  let(:historic_visit) { FactoryGirl.create(:historic_visit)}

  it "has a valid default FactoryGirl object" do
    expect(historic_visit).to be_valid
  end

  it { should allow_value(Date.today).for(:date) }

  describe 'validations' do
    it { should allow_value("").for(:date) }
    specify "that the date can be in the year of its survey season ancestor" do
      if historic_visit.station.station_owner_type=='SurveySeason'
        season_year = historic_visit.station.station_owner.year
        historic_visit.date = Date.strptime(season_year.to_s,"%Y") #this will be jan 1st of the specified survey season year minus 1 year
        expect(historic_visit).to be_valid
      end
    end
    specify "the date can have any year if it's part of a project" do
      project_station = FactoryGirl.create(:historic_visit, date: Date.current, station: FactoryGirl.create(:station, :owned_by_project))
      project_station.date = Date.strptime("2099","%Y")
      expect(project_station).to be_valid
    end
    specify "that the date cannot be in a year different from its survey season ancestor" do
      if historic_visit.station.station_owner_type=='SurveySeason'
        season_year = historic_visit.station.station_owner.year
        historic_visit.date = Date.strptime(season_year.to_s,"%Y") - 1.year #this will be jan 1st of the specified survey season year minus 1 year
        expect(historic_visit).to_not be_valid
      end
    end
    specify "that another historic visit cannot have the same parent" do
      historic_visit2 = FactoryGirl.build(:historic_visit, station: historic_visit.station)
      expect(historic_visit2).to_not be_valid
    end
    it { should validate_presence_of(:station) }
  end

  describe 'associations' do
    it { should have_many(:historic_captures).dependent(:destroy) }
  end

end
