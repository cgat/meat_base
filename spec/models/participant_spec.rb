# == Schema Information
#
# Table name: participants
#
#  id          :integer          not null, primary key
#  last_name   :string(255)
#  given_names :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

describe Participant do
  let(:participant) { FactoryGirl.create(:participant)}

  it "has a valid default FactoryGirl object" do
    expect(participant).to be_valid
  end

  it { should allow_value("a"*255).for(:given_names) }
  it { should allow_value("a"*255).for(:last_name) }

  describe 'validations' do
    it { should_not allow_value("a"*256).for(:given_names) }
    it { should validate_presence_of(:given_names) }
    it { should_not allow_value("a"*256).for(:last_name) }
    it { should validate_presence_of(:last_name) }
    it { should validate_uniqueness_of(:given_names).scoped_to(:last_name) }
    it { should validate_uniqueness_of(:last_name).scoped_to(:given_names) }
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:visits_photographed) }
    it { should_not have_and_belong_to_many(:visits_photographed).dependent(:destroy) }
    it { should have_and_belong_to_many(:visits_authored) }
    it { should_not have_and_belong_to_many(:visits_authored).dependent(:destroy) }
    it { should have_many(:visits) }
    it { should_not have_many(:visits).dependent(:destroy) }

  end

  describe "#full_name" do
    it "returns a single string containing bothe first and last names" do
      expect(participant.full_name).to eq(participant.given_names+" "+participant.last_name)
    end
  end


end
