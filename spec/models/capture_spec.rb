require 'spec_helper'

describe Capture do
  let(:capture) { FactoryGirl.create(:capture)}

  it "has a valid default FactoryGirl object" do
    expect(capture).to be_valid
  end

  it { should allow_value("12345A").for(:fn_photo_reference) }
  it { should allow_value(1.4).for(:f_stop) }
  it { should allow_value("1/100").for(:shutter_speed) }
  it { should allow_value(100).for(:iso) }
  it { should allow_value(35).for(:focal_length) }
  it { should allow_value(DateTime.now).for(:capture_datetime) }
  it { should allow_value(180).for(:azimuth) }

  describe 'validations' do
    it { should_not allow_value(-1).for(:azimuth) }
    it { should_not allow_value(360).for(:azimuth) }
    it { should validate_uniqueness_of(:fn_photo_reference).scoped_to([:capture_owner_id, :capture_owner_type] )}
    it { should_not allow_value("a"*256).for(:comments) }
  end

  describe do
    subject {capture}
    include_examples "camera info metadata"
  end
  describe do
    #normally this would be: subject {FactoryGirl.create(:capture_with_nested_gps_image)},
    #but it seems rspec has trouble memoizing the subject fully when it is updated by its after(:create) child in the factory
    subject {t = FactoryGirl.create(:capture, :with_nested_gps_image); Capture.find(t.id)}
    include_examples "camera info metadata"
    include_examples "objects that can have coordinates in decimal format"
  end

  describe do
    subject { FactoryGirl.create(:capture, :decimal_lat_long) }
    include_examples "bad input of coordinates configurations"
    include_examples "objects that can have coordinates in decimal format"
  end

  describe do
    subject {FactoryGirl.create(:capture, :DMS_lat_long)}
    include_examples "objects that can have coordinates in DMS format"
  end

  describe do
    subject {FactoryGirl.create(:capture, :MinDec_lat_long)}
    include_examples "objects that can have coordinates in MinDec format"
  end

  describe do
    subject {FactoryGirl.create(:capture, :UTM)}
    include_examples "objects that can have coordinates in UTM format"
  end

  describe 'associations' do
    #this test may seem odd, but there are rare occassions where
    #a surveyor took the same photo twice, resulting in two
    #historic captures, and thus more than one comparison
    it { should have_many(:historic_captures) }
    it { should have_many(:comparison_indices).dependent(:destroy) }
    it { should have_many(:capture_images).dependent(:destroy) }
  end

  describe "#add_comparison" do
    it "can use the add_comparison method to create comparisons" do
      capture.add_comparison(FactoryGirl.create(:historic_capture))
      capture.add_comparison(FactoryGirl.create(:historic_capture))
      expect(capture.historic_captures.size).to eq(2)
    end

    it "does not make two comparisons when adding the same capture on the association twice" do
      hc = FactoryGirl.create(:historic_capture)
      capture.add_comparison(hc)
      capture.save!
      capture.add_comparison(hc)
      capture.save!
      expect(capture.historic_captures).to eq([hc])
    end
  end

  describe "#remove_comparison" do
    it "can use the remove_comparison method to create comparisons" do
      hc = FactoryGirl.create(:historic_capture)
      capture.add_comparison(FactoryGirl.create(:historic_capture))
      capture.add_comparison(hc)
      capture.remove_comparison(hc)
      expect(capture.historic_captures.size).to eq(1)
    end
  end

  describe "#visit" do
    context "when the parent is a location" do
      it "returns the capture's visit" do
        capture.capture_owner = FactoryGirl.create(:location)
        capture.save!
        expect(capture.visit).to eq(capture.capture_owner.visit)
      end
    end
    context "when the parent is a visit" do
      it "returns the capture's visit" do
        capture.capture_owner = FactoryGirl.create(:visit)
        capture.save!
        expect(capture.visit).to eq(capture.capture_owner)
      end
    end
    context "when unsorted" do
      it "returns nil" do
        capture.capture_owner = FactoryGirl.create(:station)
        capture.save!
        expect(capture.visit).to be_blank
      end
    end
  end

  describe "#date" do
    context "when it has a visit" do
      it "returns the visit's date" do
        expect(capture.date).to eq(capture.visit.date)
      end
    end
    context "when it does not have a visit" do
      it "returns nil" do
        capture.capture_owner = FactoryGirl.create(:station)
        capture.save!
        expect(capture.date).to be_blank
      end
    end
  end


  describe "scopes" do
    describe "missing_comparisons" do
      context "a capture without comparisons exists" do
        let!(:cap_without_comp) { FactoryGirl.create(:capture)}
        let!(:cap_with_comp) { FactoryGirl.create(:capture, :with_comparisons) }
        it "returns only the capture with comparisons" do
          Capture.missing_comparisons.to_a.should =~ [cap_without_comp]
        end
      end
      context "all captures have comparisons" do
        let!(:cap_with_comp1) { FactoryGirl.create(:capture, :with_comparisons) }
        let!(:cap_with_comp2) { FactoryGirl.create(:capture, :with_comparisons) }
        it "returns an empty array" do
          Capture.missing_comparisons.should be_empty
        end
      end
    end
    describe "with_capture_images" do
      context "captures exist and do not have capture images" do
        let!(:cap1) { FactoryGirl.create(:capture)}
        let!(:cap2) { FactoryGirl.create(:capture)}
        it "returns an empty array" do
          expect(Capture.with_capture_images).to be_empty
        end
      end
      context "captures exist and have capture images" do
        let!(:cap1) { FactoryGirl.create(:capture, :with_images)}
        let!(:cap2) { FactoryGirl.create(:capture, :with_images)}
        it "returns both captures" do
          (Capture.with_capture_images).should =~ [cap1,cap2]
        end
      end
    end
    describe "without_capture_images" do
      context "captures exist and do not have capture images" do
        let!(:cap1) { FactoryGirl.create(:capture)}
        let!(:cap2) { FactoryGirl.create(:capture)}
        it "returns both captures" do
          (Capture.without_capture_images).should =~ [cap1,cap2]
        end
      end
      context "captures exist and have capture images" do
        let!(:cap1) { FactoryGirl.create(:capture, :with_images)}
        let!(:cap2) { FactoryGirl.create(:capture, :with_images)}
        it "returns an empty array" do
          expect(Capture.without_capture_images).to be_empty
        end
      end
    end
    describe "remotely_uploaded" do
      context "captures exist and do not have capture images" do
        let!(:cap1) { FactoryGirl.create(:capture)}
        let!(:cap2) { FactoryGirl.create(:capture)}
        it "returns an empty array" do
          expect(Capture.remotely_uploaded.length).to eq(0)
        end
      end
      describe "captures with capture images" do
        let!(:cap1) { FactoryGirl.create(:capture, :with_images)}
        let!(:cap2) { FactoryGirl.create(:capture, :with_images)}
        context "but images are not uploaded remotely" do
          it "returns an empty array" do
            expect(Capture.remotely_uploaded.length).to eq(0)
          end
        end
        context "where only one image is remotely uploaded" do
          before(:each) do
            cap1.reload.capture_images.first.update_attributes({remote: true})
            cap2.reload.capture_images.first.update_attributes({remote: true})
          end
          it "returns both captures" do
            (Capture.remotely_uploaded).should =~ [cap1,cap2]
          end
        end
        context "where all images are uploaded" do
          before(:each) do
            cap1.reload.capture_images.each{|a| a.update_attributes({remote: true}) }
            cap2.reload.capture_images.each{|a| a.update_attributes({remote: true}) }
          end
          it "returns both captures" do
            (Capture.remotely_uploaded).should =~ [cap1,cap2]
          end
        end
      end
    end
    describe "mastered" do
      it "returns mastered captures" do
        cap_mastered = FactoryGirl.create(:capture, :mastered)
        (Capture.mastered).should =~ [cap_mastered]
      end
      it "does not return captures with comparisons but not mastered" do
        cap_unmastered = FactoryGirl.create(:capture, :with_comparison_with_images)
        (Capture.mastered).should be_empty
      end
    end
    describe "not_mastered" do
      it "returns captures that are compared but not mastered" do
        cap_compared = FactoryGirl.create(:capture, :with_comparison_with_images)
        (Capture.not_mastered).should =~ [cap_compared]
      end
      it "does not return captures that do not have comparisons" do
        cap_not_compared = FactoryGirl.create(:capture, :with_images)
        (Capture.not_mastered).should be_empty
      end
      it "does not return mastered captures" do
        cap_mastered = FactoryGirl.create(:capture, :mastered)
        (Capture.not_mastered).should be_empty
      end
    end
    describe 'published' do
      it 'returns only published captures' do
        cap_published = FactoryGirl.create(:capture, published: true)
        (Capture.published).should =~ [cap_published]
      end
      it 'does not return unpublished captures' do
        cap_unpublished = FactoryGirl.create(:capture, published: false)
        (Capture.published).should be_empty
      end
    end
    describe "has_comparisons_with_images" do
      it "returns only captures that have comparisons which have capture images associated with them"  do
        cap_compared_with_himage = FactoryGirl.create(:capture, :with_comparison_with_images)
        cap_compared_without_himage = FactoryGirl.create(:capture, :with_comparisons)
        (Capture.has_comparisons_with_images).should =~ [cap_compared_with_himage]
      end
      it "does not return captures that have comparisons which do not have capture images associated with them" do
        cap_compared_without_himage = FactoryGirl.create(:capture, :with_comparisons)
        (Capture.has_comparisons_with_images).should be_empty
      end
    end
  end

  describe "#preview_image" do
    let(:capture_with_images) { FactoryGirl.create(:capture, :with_images) }
    it "retieves the preview_image" do
      expect(capture_with_images.reload.preview_image).to be_present
    end
    context "capture images are missing dimensions" do
      before(:each) do
        capture_with_images.capture_images.each{|ci| ci.update_column(:x_dim, nil); ci.update_column(:y_dim, nil) }
      end
      it "retrieves the preview_image" do
        expect(capture_with_images.reload.preview_image).to be_present
      end
    end
    context "one of the capture images is missing dimensions" do
      before(:each) do
        capture_with_images.capture_images.first.update_column(:x_dim,nil)
        capture_with_images.capture_images.first.update_column(:y_dim,nil)
      end
      it "retrieves the preview_image" do
        expect(capture_with_images.reload.preview_image).to be_present
      end
    end
  end

  describe "#display_name" do
    context "when a fn_photo_reference exists" do
      it "will use the fn_photo_reference for the display_name" do
        capture.update_attributes(fn_photo_reference: "A2143")
        expect(capture.display_name).to eq (capture.fn_photo_reference)
      end
    end
    context "whe no fn_photo_reference" do
      it "will use the name of the master capture_image if it exists" do
        capture = FactoryGirl.create(:capture, :with_images)
        ci = capture.capture_images.last
        capture.update_column(:fn_photo_reference, nil)
        ci.update_column(:image_state, "MASTER")
        expect(capture.display_name).to eq (ci.display_name.sub(/\.[^\/]*$/,""))
      end
      it "will use the name of the capture_image with the biggest size if there is no master" do
        capture = FactoryGirl.create(:capture, :with_images)
        ci = capture.capture_images.last
        capture.update_column(:fn_photo_reference, nil)
        ci.update_column(:file_size, 10000000000)
        expect(capture.display_name).to eq (ci.display_name.sub(/\.[^\/]*$/,""))
      end
    end
  end

  describe "merging" do
    context "a capture with attributes to a capture without" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture, :with_all_fields) }
        let(:capture_argument) { FactoryGirl.create(:capture) }
      end
    end
    context "a capture without attributes to a capture with" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture) }
        let(:capture_argument) { FactoryGirl.create(:capture, :with_all_fields) }
      end
    end
    context "a capture with capture images to a capture without" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture, :with_images, :with_all_fields) }
        let(:capture_argument) { FactoryGirl.create(:capture) }
      end
    end
    context "a capture without capture images to a capture with" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture) }
        let(:capture_argument) { FactoryGirl.create(:capture, :with_images, :with_all_fields) }
      end
    end
    context "both captures have capture_images" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture, :with_all_fields, :with_images) }
        let(:capture_argument) { FactoryGirl.create(:capture, :with_images) }
      end
    end
    context "a capture with comparisons to a capture without" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture) }
        let(:capture_argument) { FactoryGirl.create(:capture, :with_comparisons)  }
      end
    end
    context "a capture without comparisons to a capture with" do
      include_examples "merging mergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture, :with_comparisons) }
        let(:capture_argument) { FactoryGirl.create(:capture) }
      end
    end
    context "unmergeable captures" do
      include_examples "mergeing unmergeable captures" do
        let(:capture_receiver) { FactoryGirl.create(:capture, :with_all_fields) }
        let(:capture_argument) { FactoryGirl.create(:capture, :with_all_fields) }
      end
    end
  end



end


