# == Schema Information
#
# Table name: surveys
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  historical_map_sheet :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

require 'spec_helper'

describe Survey do

  let(:survey) { FactoryGirl.create(:survey)}

  it "has a default FactoryGirl object" do
    survey.should be_valid
  end

  it { should allow_value("a"*255).for(:name) }
  it { should allow_value("a"*255).for(:historical_map_sheet) }
  it { should allow_value("").for(:historical_map_sheet)}

  describe "validates" do
    it { should_not allow_value("a"*256).for(:name) }
    it { should_not allow_value("a"*256).for(:historical_map_sheet) }
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name).scoped_to(:surveyor_id) }
    it { should validate_presence_of(:surveyor) }
  end

  describe "associations" do
    it { should have_many(:survey_seasons).dependent(:destroy) }
    it { should have_many(:unsorted_captures).class_name("Capture").dependent(:destroy) }
    it { should have_many(:unsorted_hcaptures).class_name("HistoricCapture").dependent(:destroy) }
    it { should have_many(:scenics).class_name("ScenicImage").dependent(:destroy) }
  end

end
