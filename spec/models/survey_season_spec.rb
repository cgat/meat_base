# == Schema Information
#
# Table name: survey_seasons
#
#  id                  :integer          not null, primary key
#  survey_id           :integer
#  year                :date
#  geographic_coverage :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

require 'spec_helper'

describe SurveySeason do

  let(:survey_season) { FactoryGirl.create(:survey_season)}

  it "has a valid default FactoryGirl object" do
    expect(survey_season).to be_valid
  end

  it { should allow_value("1899").for(:year) }
  it { should allow_value("a"*255).for(:geographic_coverage) }

  describe "validates" do
    it { should validate_presence_of(:year) }
    it { should_not allow_value("123").for(:year) }
    it { should_not allow_value("12345").for(:year) }
    it { should_not allow_value("a"*256).for(:geographic_coverage) }
    it { should validate_presence_of(:survey) }
    it { should validate_uniqueness_of(:year).scoped_to(:survey_id) }
  end

  describe 'associations' do
    it { should have_many(:stations).dependent(:destroy)}
    it { should have_many(:unsorted_captures).class_name("Capture").dependent(:destroy) }
    it { should have_many(:unsorted_hcaptures).class_name("HistoricCapture").dependent(:destroy) }
    it { should have_many(:scenics).class_name("ScenicImage").dependent(:destroy) }
  end

end
