# == Schema Information
#
# Table name: lens
#
#  id           :integer          not null, primary key
#  brand        :string(255)
#  focal_length :string(255)
#  max_aperture :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

require 'spec_helper'

describe Lens do
  before { @lens = Lens.new}
  subject { @lens}
  it { should respond_to(:brand)}
  it { should respond_to(:focal_length)}
  it { should respond_to(:max_aperture)}

  it { should be_valid}
end
