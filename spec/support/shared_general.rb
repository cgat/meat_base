#SHARED EXAMPLES REQUIRE SUBJECT#

shared_examples "camera info metadata" do
  it { should allow_value(1.4).for(:f_stop)}
  it { should_not allow_value("string").for(:f_stop) }
  it { should allow_value(100).for(:iso) }
  it { should_not allow_value("string").for(:iso)}
  it { should allow_value("1/100").for(:shutter_speed) }
  it { should allow_value(35).for(:focal_length) }
  it { should_not allow_value(-1).for(:focal_length) }
  it { should_not allow_value("string").for(:focal_length) }
end

shared_examples "extraction of file related metadata from image" do
  it { subject.should be_valid }
  it { subject.file_size.should_not be_blank }
  it { subject.x_dim.should_not be_blank }
  it { subject.y_dim.should_not be_blank }
  it { subject.bit_depth.should_not be_blank }
end

shared_examples "locally stored image" do
  it "has file in the file structure" do
    File.exists?(subject.reload.image.file.path).should == true
    expect(subject.image.file.filename).to match(/#{subject.image_secure_token}/)
    expect(subject.image.medium.url).to match(/medium_.*#{subject.image_secure_token}/)
    expect(subject.image.thumb.url).to match(/thumb_.*#{subject.image_secure_token}/)
  end
  it "can recreate all the versions", :skip do
    version_timestamps=subject.reload.image.versions.values.map{|v| File.new(v.file.path).mtime}
    subject.process_image_upload=true #required for carrierwave-backgrounder when recreating versions
    subject.image.recreate_versions!
    expect(subject.reload.image.versions.values.map{|v| File.new(v.file.path).mtime}).to_not eq(version_timestamps)
  end
end

shared_examples "remotely stored image" do
  it "has a url for the remote image when uploaded with remote paramter" do
    subject.reload.image_remote.url.should match /.*\/test-bucket.*#{subject.image_secure_token}/
    subject.image_remote.medium.url.should match /.*\/test-bucket.*medium_.*#{subject.image_secure_token}/
    subject.image_remote.thumb.url.should match /.*\/test-bucket.*thumb_.*#{subject.image_secure_token}/
  end
end

shared_examples "an image that intially uploaded locally, but will be later be uploaded remotely" do
  it "has a url for when remote parameter set to 1 and saved" do
    subject.reload
    subject.remote = "1"
    subject.save!
    subject.reload.image_remote.url.should match /.*\/test-bucket.*/
  end
  it "after being saved remotely, can be deleted remotely" do
    subject.reload
    subject.remote = "1"
    subject.save!
    subject.remote = "0"
    subject.save!
    subject.reload.image_remote.url.should be_blank
  end
end

shared_examples "merging mergeable captures" do
    it "should merge successfully" do
      cap_attr_receiver = capture_receiver.attributes.reject{|k,v| ['created_at','updated_at','id','capture_owner_type','capture_owner_id','condition'].include?(k) }
      cap_attr_argument = capture_argument.attributes.reject{|k,v| ['created_at','updated_at','id','capture_owner_type','capture_owner_id','condition'].include?(k) }
      cap_attributes = cap_attr_receiver.merge(cap_attr_argument) {|key, v_rec, v_arg| value = v_arg.nil? || v_arg=="" ? v_rec : v_arg }
      cap_images_receiver = capture_argument.capture_images
      cap_images_argument = capture_receiver.capture_images
      comparisons_receiver = capture_argument.comparisons
      comparisons_argument = capture_receiver.comparisons
      capture_receiver.merge(capture_argument)
      capture_receiver.reload
      cap_attributes.each do |name,value|
        if value.class==ActiveSupport::TimeWithZone
          [name,capture_receiver.send(name).to_i].should =~([name,value.to_i])
        else
          [name,capture_receiver.send(name)].should =~([name,value])
        end
      end
      (cap_images_receiver+cap_images_argument).each do |ci|
        expect(capture_receiver.capture_images).to include(ci)
      end
      (comparisons_receiver+comparisons_argument).each do |comp|
        expect(capture_receiver.comparisons).to include(comp)
      end
    end
  end

  shared_examples "mergeing unmergeable captures" do
    it "should fail to merge captures" do
      expect(capture_receiver.merge(capture_argument)).to be_falsey
    end
  end

#END=SHARED EXAMPLES REQUIRE SUBJECT=END#
