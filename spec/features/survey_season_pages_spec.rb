require 'spec_helper'

describe "SurveySeason Pages" do
  it "shows survey season details" do
    survey_season = FactoryGirl.create(:survey_season, :with_all_fields)
    visit survey_season_path(survey_season)
    page.should have_content(survey_season.year)
    page.should have_content(survey_season.geographic_coverage)
  end
end
