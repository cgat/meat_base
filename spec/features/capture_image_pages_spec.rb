require 'spec_helper'
include ActionView::Helpers::NumberHelper

describe "CaptureImage Pages" do
  it "shows capture image details" do
    capture_image = FactoryGirl.create(:repeat_image, :with_all_fields)
    visit capture_image_path(capture_image)
    page.should have_xpath("//img[@alt='#{capture_image.display_name}']")
    page.should have_content(capture_image.display_name)
    page.should have_content(capture_image.x_dim)
    page.should have_content(capture_image.y_dim)
    page.should have_content(capture_image.image_state)
    page.should have_content(number_to_human_size(capture_image.file_size))
    page.should have_content("#{capture_image.bit_depth} bit")
    page.should have_content(capture_image.remote)
    page.should have_content(capture_image.comments)
  end
end
