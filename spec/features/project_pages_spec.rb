require 'spec_helper'

describe "Project Pages" do
  it "shows project details" do
    project = FactoryGirl.create(:project, :with_all_fields)
    visit project_path(project)
    page.should have_content(project.name)
    page.should have_content(project.description)
  end
end
