require 'spec_helper'

describe "Location Pages" do
  it "shows location details" do
    location = FactoryGirl.create(:location, :with_all_fields)
    visit location_path(location)
    page.should have_content(location.location_identity)
    page.should have_content(location.location_narrative)
  end
  it "shows association objects" do
    location = FactoryGirl.create(:location, :with_captures, :with_location_photos)
    visit location_path(location)
    page.should have_content("Captures")
    page.should have_content(location.captures.first.display_name)
    #not viewable outside meat, move to meat eventually
    # page.should have_content("Location Photos")
    # page.should have_content(location.location_photos.first.display_name)
  end
end
