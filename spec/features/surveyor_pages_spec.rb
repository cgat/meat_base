require 'spec_helper'

describe "Surveyor Pages" do
  it "shows surveyor details" do
    surveyor = FactoryGirl.create(:surveyor, :with_all_fields)
    visit surveyor_path(surveyor)
    page.should have_content(surveyor.given_names)
    page.should have_content(surveyor.last_name)
    page.should have_content(surveyor.short_name)
    page.should have_content(surveyor.affiliation)
  end
end
