require 'spec_helper'

describe "HistoricVisit Pages" do
  it "shows historic visit details" do
    historic_visit = FactoryGirl.build(:historic_visit, :with_all_fields, date: Date.today)
    historic_visit.save(validate: false) #saving without validations because date should be within the survey season year
    visit historic_visit_path(historic_visit)
    page.should have_content(historic_visit.date.to_s(:long))
    page.should have_content(historic_visit.comments)
  end
  it "shows association objects" do
    historic_visit = FactoryGirl.create(:historic_visit, :with_historic_captures)
    visit historic_visit_path(historic_visit)
    page.should have_content("Comparisons")
    page.should have_content(historic_visit.historic_captures.first.display_name)
  end
end
