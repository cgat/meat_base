# encoding: UTF-8
require 'spec_helper'
include ActionView::Helpers::NumberHelper

feature "Capture Pages" do
  it "shows capture details" do
    capture = FactoryGirl.create(:capture, :with_all_fields)
    visit capture_path(capture)
    page.should have_content("#{capture.azimuth}°")
    page.should have_content(capture.capture_datetime.to_s(:long))
    page.should have_content("#{capture.focal_length}mm")
    page.should have_content(capture.iso)
    page.should have_content("f/#{capture.f_stop}")
    page.should have_content(capture.shutter_speed)
    #not viewable in non admin mode, put in meat app
    #page.should have_content(capture.fn_photo_reference)
    #page.should have_content(capture.comments)
    #page.should have_content(capture.alternate)
  end
  it "shows assocation objects" do
    capture = FactoryGirl.create(:capture, :with_images, :with_camera, :with_lens, :with_comparisons)
    visit capture_path(capture)
    page.should have_content(capture.camera.display_name)
    page.should have_content(capture.lens.display_name)
    #not viewable in non admin mode, put in meat app
    # page.should have_content("Capture Images")
    # cap_img = capture.capture_images.first
    # page.should have_content(cap_img.display_name)
    # page.should have_content(cap_img.x_dim)
    # page.should have_content(cap_img.y_dim)
    # page.should have_content(cap_img.image_state)
    # page.should have_content(number_to_human_size(cap_img.file_size))
    # page.should have_content(cap_img.remote)
    page.should have_content("Comparisons")
    page.should have_content(capture.historic_captures.first.display_name)
  end

  describe "sequential navigation" do

    let(:capture_sequence) do
        sequence=[]
        #note that captures should be sorted by their display_name (which in this case is fn_photo_reference)
        #so sequence should be last in the sequence even though it was created first.
        sequence<<FactoryGirl.create(:capture, fn_photo_reference: "C")
        sequence<<FactoryGirl.create(:capture, fn_photo_reference: "B", capture_owner: sequence[0].capture_owner)
        sequence<<FactoryGirl.create(:capture, fn_photo_reference: "A", capture_owner: sequence[0].capture_owner)
    end

    context "capture is in mid sequence" do
        scenario "next is clicked" do
            visit capture_path(capture_sequence[1])
            click_link("next_link")
            within(:css,"#preview_image .display_name") do
                page.should have_text("C")
            end
        end
        scenario "previous is clicked" do
            visit capture_path(capture_sequence[1])
            click_link("prev_link")
            within(:css,"#preview_image .display_name") do
                page.should have_text("A")
            end
        end
    end

    scenario "capture is at the start of the sequence" do
        visit capture_path(capture_sequence[2])
        page.should_not have_css('#prev_link')
        page.should have_css('#next_link')
    end

    scenario "capture is at the end of the sequence" do
        visit capture_path(capture_sequence[0])
        page.should_not have_css('#next_link')
        page.should have_css('#prev_link')
    end

end

end
