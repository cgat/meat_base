require 'spec_helper'

feature "HistoricCapture Pages" do
  it "shows historic capture preview image" do
    historic_capture = FactoryGirl.create(:historic_capture, :with_all_fields)
    visit historic_capture_path(historic_capture)
    page.should have_css('#preview_image img')
    #not viewable in non-admin app. Put this is meat for testing
    #page.should have_content(historic_capture.fn_photo_reference)
    #page.should have_content(historic_capture.comments)
    #page.should have_content(historic_capture.digitization_datetime.to_s(:long))
    #page.should have_content(historic_capture.digitization_location)
    #page.should have_content("f/#{historic_capture.f_stop}")
    #page.should have_content("#{historic_capture.focal_length}mm")
    #page.should have_content(historic_capture.shutter_speed)
    #page.should have_content(historic_capture.lac_box)
    #page.should have_content(historic_capture.lac_catalogue)
    #page.should have_content(historic_capture.lac_collection)
    #page.should have_content(historic_capture.lac_ecopy)
    #page.should have_content(historic_capture.lac_wo)
  end
  it "shows associations objects" do
    historic_capture = FactoryGirl.create(:historic_capture, :with_images, :with_comparisons, :with_lens, :with_camera)
    visit historic_capture_path(historic_capture)
    #not viewable in non-admin app. Put this is meat for testing
    #page.should have_content(historic_capture.camera.display_name)
    #page.should have_content(historic_capture.lens.display_name)
    # page.should have_content("Capture Images")
    # page.should have_content(historic_capture.capture_images.first.display_name)
    page.should have_content("Comparisons")
    page.should have_content(historic_capture.captures.first.display_name)
  end

  describe "sequential navigation" do

    let(:historic_capture_sequence) do
        sequence=[]
        #note that historic_captures should be sorted by their display_name (which in this case is fn_photo_reference)
        #so sequence should be last in the sequence even though it was created first.
        sequence<<FactoryGirl.create(:historic_capture, fn_photo_reference: "C")
        sequence<<FactoryGirl.create(:historic_capture, fn_photo_reference: "B", capture_owner: sequence[0].capture_owner)
        sequence<<FactoryGirl.create(:historic_capture, fn_photo_reference: "A", capture_owner: sequence[0].capture_owner)
    end

    context "historic_capture is in mid sequence" do
        scenario "next is clicked" do
            visit historic_capture_path(historic_capture_sequence[1])
            click_link("next_link")
            within(:css,"#preview_image .display_name") do
                page.should have_text("C")
            end
        end
        scenario "previous is clicked" do
            visit historic_capture_path(historic_capture_sequence[1])
            click_link("prev_link")
            within(:css,"#preview_image .display_name") do
                page.should have_text("A")
            end
        end
    end

    scenario "historic_capture is at the start of the sequence" do
        visit historic_capture_path(historic_capture_sequence[2])
        page.should_not have_css('#prev_link')
        page.should have_css('#next_link')
    end

    scenario "historic_capture is at the end of the sequence" do
        visit historic_capture_path(historic_capture_sequence[0])
        page.should_not have_css('#next_link')
        page.should have_css('#prev_link')
    end

  end
end
