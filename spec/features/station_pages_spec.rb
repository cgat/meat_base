require 'spec_helper'

describe "Station Pages" do
  it "shows station details" do
    station = FactoryGirl.create(:station, :with_all_fields)
    visit station_path(station)
    page.should have_content(station.name)
  end
  it "shows association objects" do
    station = FactoryGirl.create(:station)
    visit1 = FactoryGirl.create(:visit, station: station)
    unsorted_cap = FactoryGirl.create(:capture, capture_owner: station)
    historic_visit = FactoryGirl.create(:historic_visit, station: station)
    hcap = FactoryGirl.create(:historic_capture, capture_owner: historic_visit)
    visit station_path(station)
    page.should have_content(visit1.date.to_s(:long))
    page.should have_content(hcap.display_name)
    #move to meat app
    # page.should have_content("Unsorted Captures")
    # page.should have_content(unsorted_cap.display_name)
  end
end
