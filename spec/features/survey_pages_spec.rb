require 'spec_helper'

describe "Survey Pages" do
  it "shows survey details" do
    survey = FactoryGirl.create(:survey, :with_all_fields)
    visit survey_path(survey)
    page.should have_content(survey.name)
    page.should have_content(survey.historical_map_sheet)
  end
end
