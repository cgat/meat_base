require 'spec_helper'
include ActionView::Helpers::NumberHelper

describe "LocationImage Pages" do
  it "shows locaiton image details" do
    location_image = FactoryGirl.create(:location_image, :with_all_fields)
    visit location_image_path(location_image)
    page.should have_content(location_image.display_name)
    page.should have_content(number_to_human_size(location_image.file_size))
    page.should have_content(location_image.x_dim)
    page.should have_content(location_image.y_dim)
    page.should have_content("#{location_image.bit_depth} bit")
    page.should have_content("#{location_image.focal_length}mm")
    page.should have_content("f/#{location_image.f_stop}")
    page.should have_content(location_image.shutter_speed)
    page.should have_content(location_image.iso)
    page.should have_content(location_image.capture_datetime.to_s(:long))
  end
end
