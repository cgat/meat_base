require 'spec_helper'

describe "Visit Pages" do
  it "shows visit details" do
    visit1 = FactoryGirl.create(:visit, :with_all_fields)
    visit visit_path(visit1)
    page.should have_content(visit1.date.to_s(:long))
    page.should have_content(visit1.start_time.to_s(:time))
    page.should have_content(visit1.finish_time.to_s(:time))
    #move commented tests to meat app
    #page.should have_content(visit1.pilot)
    #page.should have_content(visit1.rw_call_sign)
    page.should have_content(visit1.visit_narrative)
    #page.should have_content(visit1.illustration)
    page.should have_content(visit1.weather_narrative)
    page.should have_content(visit1.weather_temp)
    page.should have_content(visit1.weather_ws)
    page.should have_content(visit1.weather_gs)
    page.should have_content(visit1.weather_pressure)
    page.should have_content(visit1.weather_rh)
    page.should have_content(visit1.weather_wb)
  end
  it "shows association objects" do
    visit1 = FactoryGirl.create(:visit, :with_locations, :with_unsorted_captures, :with_photographer, :with_fn_author, :with_hiking_party)
    visit visit_path(visit1)
    page.should have_content("Location: #{visit1.locations.first.location_identity}")
    page.should have_content(visit1.locations.first.captures.first.display_name)
    #move this test to meat app
    # page.should have_content("Missing Comparisons")
    # within(:css, "#visit_missing_comparisons_tab_#{visit1.id}") do
    #   page.should have_content(visit1.locations.first.captures.first.display_name)
    # end
    #page.should have_content(visit1.locations.first.location_photos.first.display_name)
    page.should have_content(visit1.photographers.first.full_name)
    page.should have_content(visit1.fn_authors.first.full_name)
    page.should have_content(visit1.hiking_party_participants.first.full_name)
    page.should have_content("Captures Without Location")
    page.should have_content(visit1.unsorted_captures.first.display_name)
    unsorted_cap = visit1.unsorted_captures.first
    unsorted_cap.historic_captures << FactoryGirl.create(:historic_capture)
    unsorted_cap.save!
    visit visit_path(visit1)
    page.should have_content("Comparisons")
    within(:css, "#visit_comparisons_tab_#{visit1.id}") do
      page.should have_content(unsorted_cap.display_name)
      page.should have_content(unsorted_cap.historic_captures.first.display_name)
    end
  end
end
