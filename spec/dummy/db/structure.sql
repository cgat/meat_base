--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cameras; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE cameras (
    id integer NOT NULL,
    model character varying(255),
    unit character varying(255),
    format character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    make character varying(255)
);


--
-- Name: cameras_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cameras_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cameras_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cameras_id_seq OWNED BY cameras.id;


--
-- Name: capture_images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE capture_images (
    id integer NOT NULL,
    captureable_id integer,
    hash_key character varying(255),
    image text,
    file_size double precision,
    x_dim integer,
    y_dim integer,
    bit_depth integer,
    image_state character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image_remote character varying(255),
    captureable_type character varying(255),
    image_secure_token character varying(255),
    legacy_path text,
    comments character varying(255),
    fs_path text,
    image_tmp character varying(255),
    image_remote_processing boolean
);


--
-- Name: capture_images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE capture_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: capture_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE capture_images_id_seq OWNED BY capture_images.id;


--
-- Name: captures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE captures (
    id integer NOT NULL,
    capture_owner_id integer,
    fn_photo_reference character varying(255),
    f_stop double precision,
    shutter_speed character varying(255),
    iso integer,
    focal_length integer,
    camera_id integer,
    lens_id integer,
    capture_datetime timestamp without time zone,
    lat double precision,
    long double precision,
    elevation double precision,
    azimuth integer,
    comments character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    alternate boolean,
    capture_owner_type character varying(255),
    published boolean
);


--
-- Name: captures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE captures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: captures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE captures_id_seq OWNED BY captures.id;


--
-- Name: comparison_indices; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE comparison_indices (
    id integer NOT NULL,
    capture_id integer,
    historic_capture_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: comparison_indices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE comparison_indices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comparison_indices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE comparison_indices_id_seq OWNED BY comparison_indices.id;


--
-- Name: field_notes; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE field_notes (
    id integer NOT NULL,
    field_note_file character varying(255),
    visit_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    legacy_path text,
    fs_path text
);


--
-- Name: field_notes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE field_notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: field_notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE field_notes_id_seq OWNED BY field_notes.id;


--
-- Name: fn_authors_visits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fn_authors_visits (
    participant_id integer NOT NULL,
    visit_id integer NOT NULL
);


--
-- Name: hiking_parties; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE hiking_parties (
    id integer NOT NULL,
    participant_id integer,
    visit_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: hiking_parties_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hiking_parties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hiking_parties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE hiking_parties_id_seq OWNED BY hiking_parties.id;


--
-- Name: historic_captures; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE historic_captures (
    id integer NOT NULL,
    plate_id character varying(255),
    fn_photo_reference character varying(255),
    f_stop double precision,
    shutter_speed character varying(255),
    focal_length integer,
    camera_id integer,
    lens_id integer,
    capture_datetime timestamp without time zone,
    digitization_location character varying(255),
    digitization_datetime timestamp without time zone,
    lac_ecopy character varying(255),
    lac_wo character varying(255),
    lac_collection character varying(255),
    lac_box character varying(255),
    lac_catalogue character varying(255),
    condition character varying(255),
    comments character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    capture_owner_id integer,
    capture_owner_type character varying(255),
    published boolean
);


--
-- Name: historic_captures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE historic_captures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: historic_captures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE historic_captures_id_seq OWNED BY historic_captures.id;


--
-- Name: historic_visits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE historic_visits (
    id integer NOT NULL,
    station_id integer,
    date date,
    comments text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fs_path text,
    published boolean
);


--
-- Name: historic_visits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE historic_visits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: historic_visits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE historic_visits_id_seq OWNED BY historic_visits.id;


--
-- Name: images; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE images (
    id integer NOT NULL,
    image character varying(255),
    hash_key character varying(255),
    file_size double precision,
    x_dim integer,
    y_dim integer,
    bit_depth integer,
    image_remote character varying(255),
    image_owner_id integer,
    image_owner_type character varying(255),
    image_secure_token character varying(255),
    legacy_path text,
    comments character varying(255),
    lat double precision,
    long double precision,
    elevation double precision,
    f_stop double precision,
    shutter_speed character varying(255),
    iso integer,
    focal_length integer,
    capture_datetime timestamp without time zone,
    camera_id integer,
    lens_id integer,
    type character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fs_path text,
    image_tmp character varying(255),
    image_remote_processing boolean
);


--
-- Name: images_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE images_id_seq OWNED BY images.id;


--
-- Name: keyword_visit_associations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE keyword_visit_associations (
    id integer NOT NULL,
    keyword_id integer,
    visit_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: keyword_visit_associations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE keyword_visit_associations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keyword_visit_associations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE keyword_visit_associations_id_seq OWNED BY keyword_visit_associations.id;


--
-- Name: keywords; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE keywords (
    id integer NOT NULL,
    keyword character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: keywords_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE keywords_id_seq OWNED BY keywords.id;


--
-- Name: lens; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE lens (
    id integer NOT NULL,
    brand character varying(255),
    focal_length character varying(255),
    max_aperture double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: lens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE lens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE lens_id_seq OWNED BY lens.id;


--
-- Name: location_photos; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE location_photos (
    id integer NOT NULL,
    location_id integer,
    hash_key character varying(255),
    image character varying(255),
    file_size double precision,
    x_dim integer,
    y_dim integer,
    bit_depth integer,
    camera_id integer,
    capture_datetime timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    image_remote character varying(255)
);


--
-- Name: location_photos_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE location_photos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: location_photos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE location_photos_id_seq OWNED BY location_photos.id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE locations (
    id integer NOT NULL,
    visit_id integer,
    location_narrative text,
    location_identity character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    lat double precision,
    long double precision,
    legacy_photos_start integer,
    legacy_photos_end integer,
    elevation double precision,
    published boolean
);


--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE locations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE locations_id_seq OWNED BY locations.id;


--
-- Name: metadata_files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE metadata_files (
    id integer NOT NULL,
    metadata_file character varying(255),
    legacy_path text,
    metadata_owner_id integer,
    metadata_owner_type character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fs_path text
);


--
-- Name: metadata_files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE metadata_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: metadata_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE metadata_files_id_seq OWNED BY metadata_files.id;


--
-- Name: participants; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE participants (
    id integer NOT NULL,
    last_name character varying(255),
    given_names character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: participants_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE participants_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE participants_id_seq OWNED BY participants.id;


--
-- Name: pg_search_documents; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE pg_search_documents (
    id integer NOT NULL,
    content text,
    searchable_id integer,
    searchable_type character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: pg_search_documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE pg_search_documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pg_search_documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE pg_search_documents_id_seq OWNED BY pg_search_documents.id;


--
-- Name: photographers_visits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE photographers_visits (
    participant_id integer NOT NULL,
    visit_id integer NOT NULL
);


--
-- Name: preferences; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE preferences (
    id integer NOT NULL,
    mlp_library_local_root character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    update_criteria_captures character varying(255),
    update_criteria_historic_captures character varying(255)
);


--
-- Name: preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: preferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE preferences_id_seq OWNED BY preferences.id;


--
-- Name: projects; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE projects (
    id integer NOT NULL,
    name character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    description text,
    legacy_path text,
    fs_path text,
    published boolean
);


--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE projects_id_seq OWNED BY projects.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: stations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stations (
    id integer NOT NULL,
    name character varying(255),
    lat double precision,
    long double precision,
    elevation double precision,
    nts_sheet character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fs_path text,
    station_owner_id integer,
    station_owner_type character varying(255),
    published boolean
);


--
-- Name: stations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE stations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE stations_id_seq OWNED BY stations.id;


--
-- Name: survey_seasons; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE survey_seasons (
    id integer NOT NULL,
    survey_id integer,
    year integer,
    geographic_coverage character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    legacy_path text,
    fs_path text,
    published boolean
);


--
-- Name: survey_seasons_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE survey_seasons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: survey_seasons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE survey_seasons_id_seq OWNED BY survey_seasons.id;


--
-- Name: surveyors; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE surveyors (
    id integer NOT NULL,
    last_name character varying(255),
    given_names character varying(255),
    short_name character varying(255),
    affiliation character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fs_path text,
    published boolean
);


--
-- Name: surveyors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE surveyors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: surveyors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE surveyors_id_seq OWNED BY surveyors.id;


--
-- Name: surveys; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE surveys (
    id integer NOT NULL,
    name character varying(255),
    historical_map_sheet character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    legacy_path text,
    surveyor_id integer,
    fs_path text,
    published boolean
);


--
-- Name: surveys_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE surveys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: surveys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE surveys_id_seq OWNED BY surveys.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    admin boolean DEFAULT false
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: visits; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE visits (
    id integer NOT NULL,
    station_id integer,
    date date,
    start_time time without time zone,
    finish_time time without time zone,
    pilot character varying(255),
    rw_call_sign character varying(255),
    visit_narrative text,
    illustration boolean,
    weather_narrative text,
    weather_temp double precision,
    weather_ws double precision,
    weather_gs double precision,
    weather_pressure double precision,
    weather_rh double precision,
    weather_wb double precision,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    fs_path text,
    fn_physical_location character varying(255),
    fn_transcription_comment text,
    published boolean
);


--
-- Name: visits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE visits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE visits_id_seq OWNED BY visits.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cameras ALTER COLUMN id SET DEFAULT nextval('cameras_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY capture_images ALTER COLUMN id SET DEFAULT nextval('capture_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY captures ALTER COLUMN id SET DEFAULT nextval('captures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY comparison_indices ALTER COLUMN id SET DEFAULT nextval('comparison_indices_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY field_notes ALTER COLUMN id SET DEFAULT nextval('field_notes_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY hiking_parties ALTER COLUMN id SET DEFAULT nextval('hiking_parties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY historic_captures ALTER COLUMN id SET DEFAULT nextval('historic_captures_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY historic_visits ALTER COLUMN id SET DEFAULT nextval('historic_visits_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY images ALTER COLUMN id SET DEFAULT nextval('images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY keyword_visit_associations ALTER COLUMN id SET DEFAULT nextval('keyword_visit_associations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY keywords ALTER COLUMN id SET DEFAULT nextval('keywords_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY lens ALTER COLUMN id SET DEFAULT nextval('lens_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY location_photos ALTER COLUMN id SET DEFAULT nextval('location_photos_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY locations ALTER COLUMN id SET DEFAULT nextval('locations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY metadata_files ALTER COLUMN id SET DEFAULT nextval('metadata_files_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY participants ALTER COLUMN id SET DEFAULT nextval('participants_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY pg_search_documents ALTER COLUMN id SET DEFAULT nextval('pg_search_documents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY preferences ALTER COLUMN id SET DEFAULT nextval('preferences_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY projects ALTER COLUMN id SET DEFAULT nextval('projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stations ALTER COLUMN id SET DEFAULT nextval('stations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY survey_seasons ALTER COLUMN id SET DEFAULT nextval('survey_seasons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY surveyors ALTER COLUMN id SET DEFAULT nextval('surveyors_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY surveys ALTER COLUMN id SET DEFAULT nextval('surveys_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY visits ALTER COLUMN id SET DEFAULT nextval('visits_id_seq'::regclass);


--
-- Name: cameras_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY cameras
    ADD CONSTRAINT cameras_pkey PRIMARY KEY (id);


--
-- Name: capture_images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY capture_images
    ADD CONSTRAINT capture_images_pkey PRIMARY KEY (id);


--
-- Name: captures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY captures
    ADD CONSTRAINT captures_pkey PRIMARY KEY (id);


--
-- Name: comparison_indices_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY comparison_indices
    ADD CONSTRAINT comparison_indices_pkey PRIMARY KEY (id);


--
-- Name: field_notes_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY field_notes
    ADD CONSTRAINT field_notes_pkey PRIMARY KEY (id);


--
-- Name: hiking_parties_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY hiking_parties
    ADD CONSTRAINT hiking_parties_pkey PRIMARY KEY (id);


--
-- Name: historic_captures_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY historic_captures
    ADD CONSTRAINT historic_captures_pkey PRIMARY KEY (id);


--
-- Name: historic_visits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY historic_visits
    ADD CONSTRAINT historic_visits_pkey PRIMARY KEY (id);


--
-- Name: images_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY images
    ADD CONSTRAINT images_pkey PRIMARY KEY (id);


--
-- Name: keyword_visit_associations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY keyword_visit_associations
    ADD CONSTRAINT keyword_visit_associations_pkey PRIMARY KEY (id);


--
-- Name: keywords_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY keywords
    ADD CONSTRAINT keywords_pkey PRIMARY KEY (id);


--
-- Name: lens_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY lens
    ADD CONSTRAINT lens_pkey PRIMARY KEY (id);


--
-- Name: location_photos_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY location_photos
    ADD CONSTRAINT location_photos_pkey PRIMARY KEY (id);


--
-- Name: locations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: metadata_files_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY metadata_files
    ADD CONSTRAINT metadata_files_pkey PRIMARY KEY (id);


--
-- Name: participants_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY participants
    ADD CONSTRAINT participants_pkey PRIMARY KEY (id);


--
-- Name: pg_search_documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY pg_search_documents
    ADD CONSTRAINT pg_search_documents_pkey PRIMARY KEY (id);


--
-- Name: preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY preferences
    ADD CONSTRAINT preferences_pkey PRIMARY KEY (id);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- Name: stations_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stations
    ADD CONSTRAINT stations_pkey PRIMARY KEY (id);


--
-- Name: survey_seasons_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY survey_seasons
    ADD CONSTRAINT survey_seasons_pkey PRIMARY KEY (id);


--
-- Name: surveyors_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY surveyors
    ADD CONSTRAINT surveyors_pkey PRIMARY KEY (id);


--
-- Name: surveys_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY surveys
    ADD CONSTRAINT surveys_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: visits_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY visits
    ADD CONSTRAINT visits_pkey PRIMARY KEY (id);


--
-- Name: index_capture_images_on_captureable_id_and_captureable_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_capture_images_on_captureable_id_and_captureable_type ON capture_images USING btree (captureable_id, captureable_type);


--
-- Name: index_capture_images_on_legacy_path; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_capture_images_on_legacy_path ON capture_images USING btree (legacy_path);


--
-- Name: index_captures_on_capture_owner_id_and_capture_owner_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_captures_on_capture_owner_id_and_capture_owner_type ON captures USING btree (capture_owner_id, capture_owner_type);


--
-- Name: index_field_notes_on_field_note_file_and_visit_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_field_notes_on_field_note_file_and_visit_id ON field_notes USING btree (field_note_file, visit_id);


--
-- Name: index_field_notes_on_legacy_path; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_field_notes_on_legacy_path ON field_notes USING btree (legacy_path);


--
-- Name: index_field_notes_on_visit_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_field_notes_on_visit_id ON field_notes USING btree (visit_id);


--
-- Name: index_fn_authors_visits_on_participant_id_and_visit_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_fn_authors_visits_on_participant_id_and_visit_id ON fn_authors_visits USING btree (participant_id, visit_id);


--
-- Name: index_images_on_image_owner_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_images_on_image_owner_id ON images USING btree (image_owner_id);


--
-- Name: index_images_on_image_owner_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_images_on_image_owner_type ON images USING btree (image_owner_type);


--
-- Name: index_photographers_visits_on_participant_id_and_visit_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_photographers_visits_on_participant_id_and_visit_id ON photographers_visits USING btree (participant_id, visit_id);


--
-- Name: index_stations_on_station_owner_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stations_on_station_owner_id ON stations USING btree (station_owner_id);


--
-- Name: index_stations_on_station_owner_type; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_stations_on_station_owner_type ON stations USING btree (station_owner_type);


--
-- Name: index_survey_seasons_on_legacy_path; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_survey_seasons_on_legacy_path ON survey_seasons USING btree (legacy_path);


--
-- Name: index_surveys_on_legacy_path; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_surveys_on_legacy_path ON surveys USING btree (legacy_path);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20120813052123');

INSERT INTO schema_migrations (version) VALUES ('20120813061239');

INSERT INTO schema_migrations (version) VALUES ('20120813064246');

INSERT INTO schema_migrations (version) VALUES ('20120813192049');

INSERT INTO schema_migrations (version) VALUES ('20120813193039');

INSERT INTO schema_migrations (version) VALUES ('20120813193318');

INSERT INTO schema_migrations (version) VALUES ('20120813193419');

INSERT INTO schema_migrations (version) VALUES ('20120813193553');

INSERT INTO schema_migrations (version) VALUES ('20120813193856');

INSERT INTO schema_migrations (version) VALUES ('20120813202746');

INSERT INTO schema_migrations (version) VALUES ('20120813203307');

INSERT INTO schema_migrations (version) VALUES ('20120813203428');

INSERT INTO schema_migrations (version) VALUES ('20120813203656');

INSERT INTO schema_migrations (version) VALUES ('20120813203811');

INSERT INTO schema_migrations (version) VALUES ('20120813203852');

INSERT INTO schema_migrations (version) VALUES ('20120813203941');

INSERT INTO schema_migrations (version) VALUES ('20120813204638');

INSERT INTO schema_migrations (version) VALUES ('20120813204817');

INSERT INTO schema_migrations (version) VALUES ('20120813204933');

INSERT INTO schema_migrations (version) VALUES ('20120813205421');

INSERT INTO schema_migrations (version) VALUES ('20120813213616');

INSERT INTO schema_migrations (version) VALUES ('20120814170508');

INSERT INTO schema_migrations (version) VALUES ('20120814182148');

INSERT INTO schema_migrations (version) VALUES ('20120814210330');

INSERT INTO schema_migrations (version) VALUES ('20120816175229');

INSERT INTO schema_migrations (version) VALUES ('20120816175817');

INSERT INTO schema_migrations (version) VALUES ('20120816183211');

INSERT INTO schema_migrations (version) VALUES ('20120816203640');

INSERT INTO schema_migrations (version) VALUES ('20120817203931');

INSERT INTO schema_migrations (version) VALUES ('20120817223600');

INSERT INTO schema_migrations (version) VALUES ('20120829200759');

INSERT INTO schema_migrations (version) VALUES ('20120830010202');

INSERT INTO schema_migrations (version) VALUES ('20120831022537');

INSERT INTO schema_migrations (version) VALUES ('20120831025733');

INSERT INTO schema_migrations (version) VALUES ('20120904175210');

INSERT INTO schema_migrations (version) VALUES ('20120904204403');

INSERT INTO schema_migrations (version) VALUES ('20120922154932');

INSERT INTO schema_migrations (version) VALUES ('20120928095337');

INSERT INTO schema_migrations (version) VALUES ('20121008191038');

INSERT INTO schema_migrations (version) VALUES ('20121008202146');

INSERT INTO schema_migrations (version) VALUES ('20121016095018');

INSERT INTO schema_migrations (version) VALUES ('20121021190655');

INSERT INTO schema_migrations (version) VALUES ('20121022073755');

INSERT INTO schema_migrations (version) VALUES ('20121022074040');

INSERT INTO schema_migrations (version) VALUES ('20121029090228');

INSERT INTO schema_migrations (version) VALUES ('20121029090247');

INSERT INTO schema_migrations (version) VALUES ('20121029141541');

INSERT INTO schema_migrations (version) VALUES ('20121029175415');

INSERT INTO schema_migrations (version) VALUES ('20121030114218');

INSERT INTO schema_migrations (version) VALUES ('20121103181056');

INSERT INTO schema_migrations (version) VALUES ('20121106102325');

INSERT INTO schema_migrations (version) VALUES ('20121116001304');

INSERT INTO schema_migrations (version) VALUES ('20121116024455');

INSERT INTO schema_migrations (version) VALUES ('20121116025204');

INSERT INTO schema_migrations (version) VALUES ('20121118215425');

INSERT INTO schema_migrations (version) VALUES ('20121118235913');

INSERT INTO schema_migrations (version) VALUES ('20121120031140');

INSERT INTO schema_migrations (version) VALUES ('20121122010149');

INSERT INTO schema_migrations (version) VALUES ('20121127030218');

INSERT INTO schema_migrations (version) VALUES ('20121127030248');

INSERT INTO schema_migrations (version) VALUES ('20121205072124');

INSERT INTO schema_migrations (version) VALUES ('20121213053818');

INSERT INTO schema_migrations (version) VALUES ('20121214023106');

INSERT INTO schema_migrations (version) VALUES ('20121230011523');

INSERT INTO schema_migrations (version) VALUES ('20121230212831');

INSERT INTO schema_migrations (version) VALUES ('20130103065628');

INSERT INTO schema_migrations (version) VALUES ('20130114084120');

INSERT INTO schema_migrations (version) VALUES ('20130227231011');

INSERT INTO schema_migrations (version) VALUES ('20130312224002');

INSERT INTO schema_migrations (version) VALUES ('20130516045507');

INSERT INTO schema_migrations (version) VALUES ('20130531041742');

INSERT INTO schema_migrations (version) VALUES ('20130531042238');

INSERT INTO schema_migrations (version) VALUES ('20130620021135');

INSERT INTO schema_migrations (version) VALUES ('20130627101516');

INSERT INTO schema_migrations (version) VALUES ('20130807022406');

INSERT INTO schema_migrations (version) VALUES ('20130808231243');

INSERT INTO schema_migrations (version) VALUES ('20130815001320');

INSERT INTO schema_migrations (version) VALUES ('20131016031754');

INSERT INTO schema_migrations (version) VALUES ('20131017020503');

INSERT INTO schema_migrations (version) VALUES ('20131018020640');

INSERT INTO schema_migrations (version) VALUES ('20131031072453');

INSERT INTO schema_migrations (version) VALUES ('20131101023507');

INSERT INTO schema_migrations (version) VALUES ('20131102015303');

INSERT INTO schema_migrations (version) VALUES ('20131102021651');