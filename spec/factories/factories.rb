
include ActionDispatch::TestProcess #include so that we can use the fixture_file_upload with carrierwave uploads
FactoryGirl.define do
  #PHOTOS_PATH = File.expand_path("../photos", Rails.root)

  #####GPS related traits#####
  trait :decimal_lat_long do
    lat 48.595139
    long -123.881836
    elevation 2000
    lat_d nil
    lat_m nil
    lat_s nil
    lat_dir nil
    long_d nil
    long_m nil
    long_s nil
    long_dir nil
  end
  trait :DMS_lat_long do
    long_d 114
    long_m 43
    long_s 7.518
    long_dir 1
    lat_d 51
    lat_m 8
    lat_s 33.23
    lat_dir 1
    elevation 2000
  end
  trait :MinDec_lat_long do
    long_d 114
    long_m 43.34
    long_s nil
    long_dir 1
    lat_d 50
    lat_m 8.45
    lat_s nil
    lat_dir 1
    elevation 2000
  end
  trait :UTM do
    zone '11U'
    easting 650000
    northing 5530000
    elevation 2000
  end

  factory :project do
    published false
    sequence(:name) { |n| "Project#{n}" }
    trait :with_all_fields do
      description "This is a Repeat Project"
    end
  end

  factory :surveyor do
    published false
    sequence(:last_name)  { |n| "LastName #{n}" }
    sequence(:given_names) { |n| "FirstName#{n}"}
    trait :with_all_fields do
      sequence(:short_name) { |n| n.to_s }
      affiliation "Government"
    end
  end


  factory :survey do
    published false
    sequence(:name) { |n| "SurveyName #{n}" }
    surveyor
    trait :with_all_fields do
      historical_map_sheet "map123"
    end
    trait :with_survey_seasons do
      after(:create) do |this_survey|
        FactoryGirl.create_list(:survey_season,5,survey: this_survey)
      end
    end
  end


  factory :survey_season do
    published false
    sequence(:year) { |n| Date.current.year-100-n }
    survey
    trait :with_all_fields do
      sequence(:geographic_coverage) { |n| "SomePlace#{n}" }
    end
    trait :with_stations do
      after(:create) do |this_survey_season|
        FactoryGirl.create(:station, station_owner: this_survey_season)
      end
    end
  end

  factory :station do
    published false
    sequence(:name) { |n| "Stn. #{n}"}
    association :station_owner, factory: :survey_season
    trait :with_all_fields do
      nts_sheet 'Sheet12345'
    end
    trait :owned_by_project do
      association :station_owner, factory: :project
    end
    trait :with_metadata do
      transient do
        metadata_file_count 5
      end
      after(:create) do |this_station, evaluator|
        FactoryGirl.create_list(:metadata_file, evaluator.metadata_file_count, metadata_owner: this_station)
      end
    end
  end

  factory :visit do
    published false
    sequence(:date) { |n| Date.current-n }
    station
    trait :with_all_fields do
      start_time Time.zone.now
      finish_time Time.zone.now + 800
      pilot 'Tom'
      rw_call_sign 'Golf Alpha Tango'
      visit_narrative 'a'*256 + 'b'*10
      illustration false
      weather_narrative 'a'*256 + 'b'*10
      weather_temp 25.1
      weather_ws 20.3
      weather_gs 30.2
      weather_pressure 2.3
      weather_rh 10.2
      weather_wb 20.43
    end
    trait :with_photographer do
      after(:create) do |this_visit|
        photographer = FactoryGirl.create(:photographer)
        this_visit.photographers << photographer
        this_visit.save!
      end
    end
    trait :with_fn_author do
      after(:create) do |this_visit|
        fn_author = FactoryGirl.create(:fn_author)
        this_visit.fn_authors << fn_author
        this_visit.save!
      end
    end
    trait :with_hiking_party do
      after(:create) do |this_visit|
        participant = FactoryGirl.create(:participant)
        FactoryGirl.create(:hiking_party, participant: participant, visit: this_visit)
        this_visit.save!
      end
    end
    trait :with_keywords do
      after(:create) do |this_visit|
        keyword = FactoryGirl.create(:keyword)
        FactoryGirl.create(:keyword_visit_association, keyword: keyword, visit: this_visit)
      end
    end
    trait :with_field_notes do
      after(:create) do |this_visit|
        field_note = FactoryGirl.create(:field_note, visit_id: this_visit.id)
        field_note_transcribed_pdf = FactoryGirl.create(:field_note_transcribed_pdf, visit_id: this_visit.id)
      end
    end
    trait :with_metadata do
      transient do
        metadata_file_count 5
      end
      after(:create) do |this_visit, evaluator|
        FactoryGirl.create_list(:metadata_file, evaluator.metadata_file_count, metadata_owner: this_visit)
      end
    end
    trait :with_unsorted_captures do
      after(:create) do |this_visit|
        cap = FactoryGirl.create(:capture, capture_owner: this_visit)
      end
    end
    trait :with_locations do
      after(:create) do |this_visit|
        location = FactoryGirl.create(:location, :with_captures, :with_location_photos, visit: this_visit)
      end
    end

  end

  factory :historic_visit do
    published false
    station
    trait :with_all_fields do
      date nil
      sequence(:comments) { |n| "This is a comment#{n}"+("a"*256) }
    end
    trait :with_historic_captures do
      after(:create) do |this_hvisit|
        FactoryGirl.create(:historic_capture, capture_owner: this_hvisit)
      end
    end
  end

  factory :historic_capture do
    published false
    association :capture_owner, factory: :historic_visit

    trait :with_all_fields do
      sequence(:plate_id) { |n| "12345#{n}" }
      sequence(:fn_photo_reference) {|n|"BRI123#{n}"}
      f_stop 16
      shutter_speed "1/100"
      focal_length 35
      digitization_location "Library Archives Canada, Ottawa"
      digitization_datetime DateTime.civil_from_format(:utc, 2011,04,15,2,12,2)
      lac_ecopy 'e123456789'
      lac_wo 'WO-12345'
      lac_collection 'Collection100'
      lac_box '123A'
      lac_catalogue 'catalogue 2'
      comments "This is a comment about a historic capture"
    end
    trait :with_camera do
      camera
    end
    trait :with_lens do
      lens
    end
    trait :with_images do
      transient do
        image_count 5
      end
      after(:create) do |hcapture, evaluator|
        FactoryGirl.create_list(:historic_image, evaluator.image_count, captureable: hcapture, do_not_save_metadata: true)
      end
    end
    trait :owned_by_survey_season do
      association :capture_owner, factory: :survey_season
    end
    trait :owned_by_survey do
      association :capture_owner, factory: :survey
    end
    trait :mastered do
      after(:create) do |this_hcapture|
        cap=FactoryGirl.create(:capture)
        cap_image = FactoryGirl.create(:historic_image, captureable: cap)
        cap_image = FactoryGirl.create(:historic_image, captureable: this_hcapture)
        this_hcapture.comparison_indices.create(capture_id: cap.id)
      end
    end
    trait :with_comparison_with_images do
      after(:create) do |this_hcapture|
        cap=FactoryGirl.create(:capture, :with_images)
        this_hcapture.comparison_indices.create(capture_id: cap.id)
      end
    end
    trait :with_comparisons do
      transient do
        comparison_count 2
      end
      after(:create) do |this_hcapture, evaluator|
        evaluator.comparison_count.times do |_|
          cap=FactoryGirl.create(:capture)
          this_hcapture.comparison_indices.create(capture_id: cap.id)
        end
      end
    end

  end

  factory :location do
    published false
    visit
    sequence(:location_identity) { |n| "A#{n}"}
    trait :with_all_fields do
      location_narrative 'Narrative at location'
    end
    trait :with_location_photos do
      after(:create) do |location1|
        FactoryGirl.create_list(:location_image, 5, image_owner: location1)
      end
    end
    trait :with_captures do
      after(:create) do |location1|
        FactoryGirl.create(:capture, capture_owner: location1)
      end
    end
  end

  factory :comparison_index do
    historic_capture
    capture
  end


  factory :capture do
    published false
    association :capture_owner, factory: :location
    trait :with_all_fields do
      sequence(:fn_photo_reference) { |n| "A12345#{n}"}
      f_stop 16
      shutter_speed "1/100"
      iso 50
      focal_length 35
      capture_datetime DateTime.now-400
      azimuth 21
      alternate false
      comments "This is a comment on a capture"
    end
    trait :with_camera do
      camera
    end
    trait :with_lens do
      lens
    end
    trait :owned_by_visit do
      association :capture_owner, factory: :visit
    end
    trait :owned_by_station do
      association :capture_owner, factory: :station
    end
    trait :owned_by_survey_season do
      association :capture_owner, factory: :survey_season
    end
    trait :owned_by_survey do
      association :capture_owner, factory: :survey
    end
    trait :with_nested_gps_image do
      after(:create) do |this_capture|
        FactoryGirl.create(:repeat_image_with_embedded_gps, captureable: this_capture)
      end
    end
    trait :with_images do
      transient do
        image_count 5
      end
      after(:create) do |this_capture, evaluator|
        FactoryGirl.create_list(:repeat_image, evaluator.image_count, captureable: this_capture, do_not_save_metadata: true)
      end
    end
    trait :mastered do
      after(:create) do |this_capture|
        hcap=FactoryGirl.create(:historic_capture)
        cap_image = FactoryGirl.create(:repeat_image, captureable: hcap)
        cap_image = FactoryGirl.create(:repeat_image, captureable: this_capture)
        this_capture.comparison_indices.create(historic_capture_id: hcap.id)
      end
    end
    trait :with_comparison_with_images do
      after(:create) do |this_capture|
        hcap=FactoryGirl.create(:historic_capture, :with_images)
        this_capture.comparison_indices.create(historic_capture_id: hcap.id)
      end
    end
    trait :with_comparisons do
      transient do
        comparison_count 2
      end
      after(:create) do |this_capture, evaluator|
        evaluator.comparison_count.times do |_|
          hcap=FactoryGirl.create(:historic_capture)
          this_capture.comparison_indices.create(historic_capture_id: hcap.id)
        end
      end
    end
  end

  factory :capture_image do
    image { fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))}
    remote "0"
    image_state 'RAW'

    trait :with_all_fields do
      hash_key 'ABC123959'
      comments 'This is a comment'
    end
    trait :with_embedded_gps do
      image { fixture_file_upload(File.join(PHOTOS_PATH,'gps_small.tif'))}
    end
    trait :without_gps do
      image { fixture_file_upload(File.join(PHOTOS_PATH, 'bw_historic.jpg'))}
    end
    trait :no_image do
      image nil
    end
    trait :with_meta_values_set do
      file_size 12355
      x_dim 3000
      y_dim 2222
      bit_depth 8
    end

    factory :repeat_image do
      association :captureable, factory: :capture
      factory :repeat_image_no_image, traits: [:no_image]
      factory :repeat_image_with_embedded_gps, traits: [:with_embedded_gps]
      factory :repeat_image_without_gps, traits: [:without_gps]
    end
    factory :historic_image do
      image { fixture_file_upload(File.join(PHOTOS_PATH,'bw_historic.jpg'))}
      association :captureable, factory: :historic_capture
      factory :historic_image_no_image, traits: [:no_image]
    end


  end


  factory :image do
    image { fixture_file_upload(File.join(PHOTOS_PATH,'color_repeat.jpg'))}
    remote "0"
    trait :location do
      type "LocationImage"
    end
    trait :scenic do
      type "ScenicImage"
    end
    trait :owned_by_location do
      association :image_owner, factory: :location
    end
    trait :owned_by_survey do
      association :image_owner, factory: :survey
    end
    trait :owned_by_survey_season do
      association :image_owner, factory: :survey_season
    end
    trait :with_embedded_gps do
      image { fixture_file_upload(File.join(PHOTOS_PATH,'gps_small.tif'))}
    end
    trait :without_gps do
      image { fixture_file_upload(File.join(PHOTOS_PATH, 'bw_historic.jpg'))}
    end
    trait :with_all_fields do
      file_size 12355
      x_dim 3000
      y_dim 2222
      bit_depth 8
      f_stop 16
      shutter_speed "1/100"
      iso 50
      focal_length 35
      capture_datetime DateTime.now - 400
      hash_key 'ABC123959'
      comments 'This is a comment'
    end
    trait :with_camera do
      camera
    end
    trait :with_lens do
      lens
    end


    factory :location_image, traits: [:location, :owned_by_location], parent: :image, class: 'LocationImage'
    factory :scenic_image, traits: [:scenic, :owned_by_survey_season], parent: :image, class: 'ScenicImage'
    factory :scenic_image_with_gps, traits: [:scenic, :with_embedded_gps, :owned_by_survey], parent: :image, class: 'ScenicImage'
  end

  factory :metadata_file do
    metadata_file { fixture_file_upload(File.join(PHOTOS_PATH,'metadata.xls'))}
    association :metadata_owner, factory: :station
  end

  factory :field_note do
    field_note_file { fixture_file_upload(File.join(PHOTOS_PATH,'scanned_field_note.jpg'))}
    factory :field_note_transcribed_pdf do
      field_note_file { fixture_file_upload(File.join(PHOTOS_PATH,'transcribed_field_note.pdf'))}
    end
    visit
  end


  factory :camera do
    model "Hasselblad H3DII"
    sequence(:unit) {|n| "#{n}"}
    format "Digital"
  end

  factory :lens do
    brand "Hasselblad"
    focal_length '35mm'
    max_aperture 2.8
  end

  factory :participant, aliases: [:photographer, :fn_author] do
    sequence(:last_name) { |n| "Tom#{n}"}
    sequence(:given_names) { |n| "Arnold#{n}"}
  end

  factory :keyword do
    sequence(:keyword) { |n| "firestorm #{n}"}
  end

  factory :keyword_visit_association do
    keyword
    visit
  end

  factory :hiking_party do
    participant
    visit
  end

end

