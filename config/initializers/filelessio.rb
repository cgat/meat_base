class FilelessIO < StringIO
    def initialize(path,opt={})
      path = path.to_s
      opt_default = {dummy: false}
      opt = opt_default.merge(opt)
      dummy_option = opt[:dummy]
      if dummy_option
        path = Rails.root.join('spec','photos','dummy_file.jpeg').to_s
        img_binary = File.open(path){ |i| i.read }
      else
        img_binary = File.open(path){ |i| i.read }
      end
      img_encoded = Base64.encode64(img_binary)
      super(Base64.decode64(img_encoded))
      self.original_filename = path.split('/').last
    end

    attr_accessor :original_filename
end
