fog_credentials = YAML.load(ERB.new(File.read(Rails.root.join('config/fog_credentials.yml'))).result)["default"]
fog_credentials[:provider] = 'AWS'
fog_dir = if Rails.env == 'production'
      'mlplibrary'
    elsif Rails.env == 'development'
      'dev-mlplibrary'
    else Rails.env == 'test'
      'test-bucket'
    end
CarrierWave.configure do |config|
  config.fog_credentials = fog_credentials
  config.fog_directory  = fog_dir                    # required
  config.fog_public     = true
end
