
ActiveRecord::Base.class_eval do
    def self.validates_num_decimals(*attr_names)
        options = attr_names.extract_options!
        validates_each(attr_names, options) do |record, attribute, value|
          record.errors[attribute] << "Error: Maximum number of decimal places is " + options[:max_decimals_places].to_s unless NumberHelper.num_of_decimals(value) <= options[:max_decimals_places]
        end
    end
end
