Rails.application.routes.draw do
  resources :surveyors, only: :show
  resources :surveys, only: :show
  resources :projects, only: :show
  resources :survey_seasons, only: :show
  resources :stations, only: :show
  resources :visits, only: :show
  resources :historic_visits, only: :show
  resources :locations, only: :show
  resources :captures, only: :show do
    member do
      get 'comparisons'
    end
  end
  resources :historic_captures, only: :show do
    member do
      get 'comparisons'
    end
  end
  resources :capture_images, only: :show
  resources :location_images, only: :show
  get 'search', to: 'application#search', as: "search"
  get 'map', to: 'application#map', as: 'map'
end
