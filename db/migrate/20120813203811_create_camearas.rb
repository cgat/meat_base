class CreateCamearas < ActiveRecord::Migration
  def change
    create_table :camearas do |t|
      t.string :model
      t.string :unit
      t.string :format

      t.timestamps
    end
  end
end
