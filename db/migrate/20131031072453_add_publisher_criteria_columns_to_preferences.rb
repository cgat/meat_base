class AddPublisherCriteriaColumnsToPreferences < ActiveRecord::Migration
  def change
    add_column :preferences, :update_criteria_captures, :string
    add_column :preferences, :update_criteria_historic_captures, :string
  end
end
