class CreateSurveyors < ActiveRecord::Migration
  def change
    create_table :surveyors do |t|
      t.string :last_name
      t.string :given_names
      t.string :short_name
      t.string :affiliation

      t.timestamps
    end
  end
end
