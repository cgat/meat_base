class FixRenameColumnError < ActiveRecord::Migration
  def change
    rename_column :historic_capture_images, :string, :image
  end

end
