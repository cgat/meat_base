class ChangeColumnsToHistoricCaptureImage < ActiveRecord::Migration
  def change
    add_column :historic_capture_images, :image_remote, :string
    rename_column :historic_capture_images, :file_name, :string
    remove_column :historic_capture_images, :file_parent_path
  end
end
