class CreateSurveySeasons < ActiveRecord::Migration
  def change
    create_table :survey_seasons do |t|
      t.integer :survey_id
      t.date :year
      t.string :geographic_coverage

      t.timestamps
    end
  end
end
