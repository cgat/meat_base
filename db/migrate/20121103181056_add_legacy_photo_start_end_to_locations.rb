class AddLegacyPhotoStartEndToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :legacy_photos_start, :integer
    add_column :locations, :legacy_photos_end, :integer
  end
end
