class AddPublishedColumnToRemainingTables < ActiveRecord::Migration
  def up
    [:historic_captures, :visits, :historic_visits, :stations, :survey_seasons, :surveys, :surveyors, :projects].each do |table|
      add_column table, :published, :boolean
    end
  end
  def down
    [:historic_captures, :visits, :historic_visits, :stations, :survey_seasons, :surveys, :surveyors, :projects].each do |table|
      remove_column table, :published
    end
  end
end
