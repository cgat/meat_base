class CreateLocationPhotos < ActiveRecord::Migration
  def change
    create_table :location_photos do |t|
      t.integer :location_id
      t.string :hash_key
      t.text :file_parent_path
      t.string :file_name
      t.float :file_size
      t.integer :x_dim
      t.integer :y_dim
      t.integer :bit_depth
      t.integer :camera_id
      t.datetime :capture_datetime

      t.timestamps
    end
  end
end
