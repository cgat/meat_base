class ChangeLegacyNameToLegacyPathInTables < ActiveRecord::Migration
  def up
    rename_column :surveys, :legacy_name, :legacy_path
    rename_column :survey_seasons, :legacy_name, :legacy_path
    add_index :surveys, :legacy_path
    add_index :survey_seasons, :legacy_path
  end

  def down
    rename_column :surveys, :legacy_path, :legacy_name
    rename_column :survey_seasons, :legacy_path, :legacy_name
  end
end
