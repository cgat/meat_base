# This migration comes from meat_base_engine (originally 20131018014753)
class AddFileVisitIdUniqueIndexToFieldNotes < ActiveRecord::Migration
  def change
    add_index :field_notes, [:field_note_file, :visit_id], unique: true
  end
end
