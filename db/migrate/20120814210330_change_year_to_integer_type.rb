class ChangeYearToIntegerType < ActiveRecord::Migration
  # def change
  #     change_column :survey_seasons, :year, :integer
  #   end
  def up 
    execute <<-SQL
    ALTER TABLE survey_seasons ALTER year TYPE integer USING year::int
    SQL
  end
  
  def down 
    execute <<-SQL
    ALTER TABLE survey_seasons ALTER year TYPE character varying(255) USING  year::string
    SQL
  end
end
