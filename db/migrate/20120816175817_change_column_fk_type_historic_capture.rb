class ChangeColumnFkTypeHistoricCapture < ActiveRecord::Migration
  # def change
  #   change_column :historic_captures, :capture_id, :integer
  #   change_column :historic_captures, :location_id, :integer
  # end
  
  def up
    execute <<-SQL
    ALTER TABLE historic_captures ALTER capture_id TYPE integer USING capture_id::int, ALTER location_id TYPE integer USING location_id::int
    SQL
  end
  
  def down
    execute <<-SQL
    ALTER TABLE historic_captures ALTER capture_id TYPE character varying(255) USING capture_id::string, ALTER location_id TYPE character varying(255) USING location_id::string
    SQL
  end
end
