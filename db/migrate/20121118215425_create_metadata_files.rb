class CreateMetadataFiles < ActiveRecord::Migration
  def change
    create_table :metadata_files do |t|
      t.string :metadata_file
      t.string :legacy_path
      t.integer :metadata_owner_id
      t.string :metadata_owner_type

      t.timestamps
    end
  end
end
