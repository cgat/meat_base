class AddLegacyPathToCaptureImages < ActiveRecord::Migration
  def change
    add_column :capture_images, :legacy_path, :string
  end
end
