class AddLegacyPathColumnToFieldNotes < ActiveRecord::Migration
  def change
    add_column :field_notes, :legacy_path, :string
    add_index :field_notes, :legacy_path
  end
end
