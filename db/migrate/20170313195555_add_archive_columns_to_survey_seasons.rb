class AddArchiveColumnsToSurveySeasons < ActiveRecord::Migration
  def change
    add_column :survey_seasons, :record_id, :integer
    add_index :survey_seasons, :record_id
    add_column :survey_seasons, :jurisdiction, :text
    add_column :survey_seasons, :affiliation, :text
    add_column :survey_seasons, :archive, :text
    add_column :survey_seasons, :collection, :text
    add_column :survey_seasons, :location, :text
    add_column :survey_seasons, :sources, :text
    add_column :survey_seasons, :notes, :text
  end
end
