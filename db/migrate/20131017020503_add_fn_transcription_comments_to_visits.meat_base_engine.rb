# This migration comes from meat_base_engine (originally 20131017015051)
class AddFnTranscriptionCommentsToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :fn_transcription_comment, :text
  end
end
