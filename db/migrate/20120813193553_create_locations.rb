class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :visit_id
      t.text :location_narrative
      t.string :location_identity

      t.timestamps
    end
  end
end
