class ChangeColumnNameHistoricCapturesCamerea < ActiveRecord::Migration
  def change
    rename_column :historic_captures, :camerea_id, :camera_id
  end

end
