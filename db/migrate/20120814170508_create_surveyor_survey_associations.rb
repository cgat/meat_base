class CreateSurveyorSurveyAssociations < ActiveRecord::Migration
  def change
    create_table :surveyor_survey_associations do |t|
      t.integer :survey_project_id
      t.integer :surveyor_id

      t.timestamps
    end
  end
end
