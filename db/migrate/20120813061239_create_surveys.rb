class CreateSurveys < ActiveRecord::Migration
  def change
    create_table :surveys do |t|
      t.string :name
      t.string :historical_map_sheet

      t.timestamps
    end
  end
end
