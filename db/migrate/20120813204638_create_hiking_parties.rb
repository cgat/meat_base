class CreateHikingParties < ActiveRecord::Migration
  def change
    create_table :hiking_parties do |t|
      t.integer :participant_id
      t.integer :visit_id

      t.timestamps
    end
  end
end
