class AddAlternateToCaptures < ActiveRecord::Migration
  def change
    add_column :captures, :alternate, :boolean
  end
end
