class MakeStationPolymorphic < ActiveRecord::Migration
  def up
    remove_column :stations, :survey_season_id
    add_column :stations, :station_owner_id, :integer
    add_column :stations, :station_owner_type, :string
    add_index :stations, :station_owner_id
    add_index :stations, :station_owner_type
  end

  def down
    add_column :stations, :station_id, :integer
    remove_index :stations, :station_owner_id
    remove_index :stations, :station_owner_type
    remove_column :stations, :station_owner_id
    remove_column :stations, :station_owner_type
  end
end
