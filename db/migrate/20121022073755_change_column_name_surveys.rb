class ChangeColumnNameSurveys < ActiveRecord::Migration
  def change
    rename_column :surveys, :old_filesystem_name, :legacy_name
  end
end
