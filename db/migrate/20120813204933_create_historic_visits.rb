class CreateHistoricVisits < ActiveRecord::Migration
  def change
    create_table :historic_visits do |t|
      t.integer :station_id
      t.date :date
      t.text :comments

      t.timestamps
    end
  end
end
