class ChangeFsPathLegacyPathToText < ActiveRecord::Migration
  def up
    change_column :capture_images, :legacy_path, :text
    change_column :capture_images, :fs_path, :text
    change_column :images, :legacy_path, :text
    change_column :images, :fs_path, :text
    change_column :field_notes, :legacy_path, :text
    change_column :field_notes, :fs_path, :text
    change_column :metadata_files, :legacy_path, :text
    change_column :metadata_files, :fs_path, :text
    
    change_column :surveys, :legacy_path, :text
    change_column :surveys, :fs_path, :text
    change_column :survey_seasons, :legacy_path, :text
    change_column :survey_seasons, :fs_path, :text
    
    change_column :surveyors, :fs_path, :text
    change_column :visits, :fs_path, :text
    change_column :stations, :fs_path, :text
    change_column :historic_visits, :fs_path, :text
    
    change_column :projects, :legacy_path, :text
    change_column :projects, :fs_path, :text
  end

  def down
    change_column :capture_images, :legacy_path, :string
    change_column :capture_images, :fs_path, :string
    change_column :images, :legacy_path, :string
    change_column :images, :fs_path, :string
    change_column :field_notes, :legacy_path, :string
    change_column :field_notes, :fs_path, :string
    change_column :metadata_files, :legacy_path, :string
    change_column :metadata_files, :fs_path, :string
    
    change_column :surveys, :legacy_path, :string
    change_column :surveys, :fs_path, :string
    change_column :survey_seasons, :legacy_path, :string
    change_column :survey_seasons, :fs_path, :string
    
    change_column :surveyors, :fs_path, :string
    change_column :visits, :fs_path, :string
    change_column :stations, :fs_path, :string
    change_column :historic_visits, :fs_path, :string
    
    change_column :projects, :legacy_path, :string
    change_column :projects, :fs_path, :string
  end
end
