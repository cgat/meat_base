class AddPublishedColumnToCaptures < ActiveRecord::Migration
  def change
    add_column :captures, :published, :boolean
  end
end
