class RemoveUnusedColumnsHistoricCaptures < ActiveRecord::Migration
  def up
    remove_column :historic_captures, :survey_id
    remove_column :historic_captures, :plate_id_user_generated
  end

  def down
    add_column :historic_captures, :survey_id, :integer
    add_column :historic_captures, :plate_id_user_generated, :boolean
  end
end
