class CreateFieldNotes < ActiveRecord::Migration
  def change
    create_table :field_notes do |t|
      t.string :field_note_file
      t.references :visit

      t.timestamps
    end
    add_index :field_notes, :visit_id
  end
end
