class RemoveLocationColumnFromHistoricCaptures < ActiveRecord::Migration
  def change
  	remove_column :historic_captures, :location_id
  end
end
