class CreateLens < ActiveRecord::Migration
  def change
    create_table :lens do |t|
      t.string :brand
      t.string :focal_length
      t.float :max_aperture

      t.timestamps
    end
  end
end
