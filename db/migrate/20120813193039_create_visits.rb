class CreateVisits < ActiveRecord::Migration
  def change
    create_table :visits do |t|
      t.integer :station_id
      t.date :date
      t.time :start_time
      t.time :finish_time
      t.string :pilot
      t.string :rw_call_sign
      t.integer :photographer_participant_id
      t.integer :fn_author_participant_id
      t.text :visit_narrative
      t.boolean :illustration
      t.text :weather_narrative
      t.float :weather_temp
      t.float :weather_ws
      t.float :weather_gs
      t.float :weather_pressure
      t.float :weather_rh
      t.float :weather_wb

      t.timestamps
    end
  end
end
