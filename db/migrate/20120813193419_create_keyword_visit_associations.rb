class CreateKeywordVisitAssociations < ActiveRecord::Migration
  def change
    create_table :keyword_visit_associations do |t|
      t.integer :keyword_id
      t.integer :visit_id

      t.timestamps
    end
  end
end
