class ChangeColumnNamesToMatchAssociationsTablesComparisonIndex < ActiveRecord::Migration
  def change
  	rename_column :comparison_indices, :baseline_id, :historic_capture_id
  	rename_column :comparison_indices, :repeat_id, :capture_id
  end
end
