class CreateHistoricCaptures < ActiveRecord::Migration
  def change
    create_table :historic_captures do |t|
      t.string :plate_id
      t.boolean :plate_id_user_generated
      t.string :capture_id
      t.string :location_id
      t.string :fn_photo_reference
      t.float :f_stop
      t.string :shutter_speed
      t.integer :focal_length
      t.integer :camerea_id
      t.integer :lens_id
      t.datetime :capture_datetime
      t.string :digitization_location
      t.datetime :digitization_datetime
      t.string :lac_ecopy
      t.string :lac_wo
      t.string :lac_collection
      t.string :lac_box
      t.string :lac_catalouge
      t.string :condition
      t.integer :survey_id
      t.string :comments

      t.timestamps
    end
  end
end
