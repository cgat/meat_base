class ChangeCaptureImagesToPolymorphic < ActiveRecord::Migration
  class CaptureImage < ActiveRecord::Base
  end
  
  def up 
  	change_table :capture_images do |t|
  		t.rename :capture_id, :captureable_id
  		t.string :captureable_type
  	end
  	CaptureImage.reset_column_information
  	CaptureImage.all.each {|c| c.update_attribute(:captureable_type, 'Capture') }
  	add_index :capture_images, [:captureable_id, :captureable_type]
  end

  def down
    remove_index :capture_images, [:captureable_id, :captureable_type]
    change_table :capture_images do |t|
  		t.rename :captureable_id, :capture_id
  		t.remove :captureable_type
  	end
  end
end
