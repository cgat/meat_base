class AddUnknownColumnsToSurveySurveyorSeason < ActiveRecord::Migration
  def change
    add_column :surveyors, :unknown, :boolean, default: false
    add_column :surveys, :unknown, :boolean, default: false
    add_column :survey_seasons, :unknown, :boolean, default: false
  end
end
