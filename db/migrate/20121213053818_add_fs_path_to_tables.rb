class AddFsPathToTables < ActiveRecord::Migration
  def change
    add_column :surveyors, :fs_path, :string
    add_column :surveys, :fs_path, :string
    add_column :survey_seasons, :fs_path, :string
    add_column :stations, :fs_path, :string
    add_column :visits, :fs_path, :string
    add_column :historic_visits, :fs_path, :string
    add_column :locations, :fs_path, :string
    add_column :captures, :fs_path, :string
    add_column :historic_captures, :fs_path, :string
    add_column :capture_images, :fs_path, :string
    add_column :images, :fs_path, :string
    add_column :metadata_files, :fs_path, :string    
    add_column :field_notes, :fs_path, :string    
  end
end
