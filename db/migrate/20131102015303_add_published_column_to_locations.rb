class AddPublishedColumnToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :published, :boolean
  end
end
