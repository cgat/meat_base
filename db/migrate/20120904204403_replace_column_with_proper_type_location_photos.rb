class ReplaceColumnWithProperTypeLocationPhotos < ActiveRecord::Migration
  def change
    remove_column :location_photos, :image_remote
    add_column :location_photos, :image_remote, :string
  end

end
