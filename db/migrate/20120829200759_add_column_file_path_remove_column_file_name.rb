class AddColumnFilePathRemoveColumnFileName < ActiveRecord::Migration
  def change
    rename_column :capture_images, :file_parent_path, :file_path
    remove_column :capture_images, :file_name
  end

end
