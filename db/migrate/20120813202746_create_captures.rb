class CreateCaptures < ActiveRecord::Migration
  def change
    create_table :captures do |t|
      t.integer :location_id
      t.string :fn_photo_reference
      t.float :f_stop
      t.string :shutter_speed
      t.integer :iso
      t.integer :focal_length
      t.integer :camera_id
      t.integer :lens_id
      t.datetime :capture_datetime
      t.float :lat
      t.float :long
      t.float :elevation
      t.integer :azimuth
      t.string :comments

      t.timestamps
    end
  end
end
