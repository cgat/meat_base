class CreateStations < ActiveRecord::Migration
  def change
    create_table :stations do |t|
      t.integer :survey_season_id
      t.string :name
      t.float :lat
      t.float :long
      t.float :elevation
      t.string :nts_sheet

      t.timestamps
    end
  end
end
