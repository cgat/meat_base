class DropSurveyorSurveyAssocationMake1ToMany < ActiveRecord::Migration

  def up
    add_column :surveys, :surveyor_id, :integer
    Survey.all.each do |survey|
      survey.update_attributes!(surveyor_id: survey.surveyors.first.try(:id))
    end
    
    drop_table :surveyor_survey_associations
  end

  def down
    create_table :surveyor_survey_associations do |t|
      t.integer :survey_project_id
      t.integer :surveyor_id
      t.timestamps
    end
    Surveyor.all.each do |surveyor|
      surveyor.surveys do |survey|
        surveyor_survey_associations.create(survey_id: survey.id, surveyor_id: surveyor.id)
      end
    end
    remove_column :surveys, :surveyor_id
  end
end
