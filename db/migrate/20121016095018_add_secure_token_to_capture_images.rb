class AddSecureTokenToCaptureImages < ActiveRecord::Migration
  def change
  	add_column :capture_images, :image_secure_token, :string
  end
end
