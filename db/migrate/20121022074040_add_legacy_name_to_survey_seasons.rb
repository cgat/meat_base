class AddLegacyNameToSurveySeasons < ActiveRecord::Migration
  def change
    add_column :survey_seasons, :legacy_name, :string
  end
end
