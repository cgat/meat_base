class DropTableHistoricCaptureImages < ActiveRecord::Migration
  def up
    drop_table :historic_capture_images
  end

  def down
    create_table "historic_capture_images", :force => true do |t|
      t.integer  "historic_capture_id"
      t.string   "hash_key"
      t.string   "image"
      t.float    "file_size"
      t.integer  "x_dim"
      t.integer  "y_dim"
      t.integer  "bit_depth"
      t.string   "image_state"
      t.datetime "created_at",          :null => false
      t.datetime "updated_at",          :null => false
      t.string   "image_remote"
    end
  end
end
