class CreateComparisonIndices < ActiveRecord::Migration
  def change
    create_table :comparison_indices do |t|
      t.integer :repeat_id
      t.integer :baseline_id

      t.timestamps
    end
  end
end
