class ConvertVisitPhotographersAuthorsToHabtm < ActiveRecord::Migration
  def up
    remove_column :visits, :photographer_participant_id
    remove_column :visits, :fn_author_participant_id
    create_table :photographers_visits, id: false do |t|
      t.integer :participant_id, null: false
      t.integer :visit_id, null: false
    end
    create_table :fn_authors_visits, id: false do |t|
      t.integer :participant_id, null: false
      t.integer :visit_id, null: false
    end
    add_index :photographers_visits, [:participant_id, :visit_id], unique: true
    add_index :fn_authors_visits, [:participant_id, :visit_id], unique: true
  end

  def down
    add_column :visits, :photographer_participant_id, :integer
    add_column :visits, :fn_author_participant_id, :integer
    drop_table :photographers_visits
    drop_table :fn_authors_visits
  end
end
