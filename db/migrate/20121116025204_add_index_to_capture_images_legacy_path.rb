class AddIndexToCaptureImagesLegacyPath < ActiveRecord::Migration
  def change
    add_index :capture_images, :legacy_path
  end
end
