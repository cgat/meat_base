class CreateGlassPlateListings < ActiveRecord::Migration
  def change
    create_table :glass_plate_listings do |t|
      t.integer :survey_season_id
      t.text :container
      t.text :plates
      t.text :notes

      t.timestamps null: false
    end
    add_index :glass_plate_listings, :survey_season_id
  end
end
