class RenameColumnsLocationPhotos < ActiveRecord::Migration
  def change
    rename_column :location_photos, :file_name, :image
    rename_column :location_photos, :file_parent_path, :image_remote
  end

end
