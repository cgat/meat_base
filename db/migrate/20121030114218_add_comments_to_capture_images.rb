class AddCommentsToCaptureImages < ActiveRecord::Migration
  def change
    add_column :capture_images, :comments, :string
  end
end
