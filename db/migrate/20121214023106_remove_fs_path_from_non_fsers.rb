class RemoveFsPathFromNonFsers < ActiveRecord::Migration
  def up
    remove_column :captures, :fs_path
    remove_column :historic_captures, :fs_path
    remove_column :locations, :fs_path
  end

  def down
    add_column :captures, :fs_path, :string
    add_column :historic_captures, :fs_path, :string
    add_column :locations, :fs_path, :string
  end
end
