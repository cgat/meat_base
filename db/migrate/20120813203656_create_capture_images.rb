class CreateCaptureImages < ActiveRecord::Migration
  def change
    create_table :capture_images do |t|
      t.integer :capture_id
      t.string :hash_key
      t.text :file_parent_path
      t.string :file_name
      t.float :file_size
      t.integer :x_dim
      t.integer :y_dim
      t.integer :bit_depth
      t.string :image_state

      t.timestamps
    end
  end
end
