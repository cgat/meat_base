class AddColumnImageAndImageRemoteCaptureImages < ActiveRecord::Migration
  def change
    rename_column :capture_images, :file_path, :image
    add_column :capture_images, :image_remote, :string
  end
end
