class AddOldFilesystemNameToSurveys < ActiveRecord::Migration
  def change
    add_column :surveys, :old_filesystem_name, :string
  end
end
