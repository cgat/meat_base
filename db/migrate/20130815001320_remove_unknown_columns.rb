class RemoveUnknownColumns < ActiveRecord::Migration
  def up
    remove_column :surveyors, :unknown
    remove_column :surveys, :unknown
    remove_column :survey_seasons, :unknown
  end

  def down
    add_column :surveyors, :unknown, :boolean
    add_column :surveys, :unknown, :boolean
    add_column :survey_seasons, :unknown, :boolean
  end
end
