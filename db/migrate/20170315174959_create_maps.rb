class CreateMaps < ActiveRecord::Migration
  def change
    create_table :maps do |t|
      t.text :nts_map
      t.text :historic_map
      t.text :links
      t.integer :survey_season_id

      t.timestamps null: false
    end
    add_index :maps, :survey_season_id
  end
end
