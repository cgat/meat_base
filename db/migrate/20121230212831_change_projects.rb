class ChangeProjects < ActiveRecord::Migration
  def up
    remove_column :projects, :affiliation
    add_column :projects, :description, :text
    add_column :projects, :legacy_path, :string
    add_column :projects, :fs_path, :string
  end

  def down
    add_column :projects, :affiliation, :string
    remove_column :projects, :description
    remove_column :projects, :legacy_path
    remove_column :projects, :fs_path
  end
end
