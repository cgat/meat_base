class ChangeCaptureAndHistoricToPolymorphic < ActiveRecord::Migration
  class HistoricCapture < ActiveRecord::Base
  end
  class Capture < ActiveRecord::Base
  end
  
  def up 
  	change_table :captures do |t|
  		t.rename :location_id, :capture_owner_id
  		t.string :capture_owner_type
  	end
  	Capture.reset_column_information
  	Capture.all.each {|c| c.update_attribute(:capture_owner_type, 'Location') }
  	add_index :captures, [:capture_owner_id, :capture_owner_type]
  	
  	change_table :historic_captures do |t|
  		t.rename :historic_visit_id, :hcapture_owner_id
  		t.string :hcapture_owner_type
  	end
  	HistoricCapture.reset_column_information
  	HistoricCapture.all.each {|c| c.update_attribute(:hcapture_owner_type, 'HistoricVisit') }
  	#add_index :historic_captures, [:hcapture_owner_id, :hcapture_owner_type]
  end

  def down
    remove_index :captures, [:capture_owner_id, :capture_owner_type]
    change_table :captures do |t|
  		t.rename :capture_owner_id, :location_id
  		t.remove :capture_owner_type
  	end
  	
  	#remove_index :historic_captures, [:hcapture_owner_id, :hcapture_owner_type]
    change_table :historic_captures do |t|
  		t.rename :hcapture_owner_id, :historic_visit_id
  		t.remove :hcapture_owner_type
  	end
  end
end
