# This migration comes from meat_base_engine (originally 20131016023856)
class AddFnPhyicalLocationToVisits < ActiveRecord::Migration
  def change
    add_column :visits, :fn_physical_location, :string
  end
end
