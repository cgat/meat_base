class RenameHcaptureOwner < ActiveRecord::Migration
  def change
    rename_column :historic_captures, :hcapture_owner_id, :capture_owner_id
    rename_column :historic_captures, :hcapture_owner_type, :capture_owner_type
  end
end
