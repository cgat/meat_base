class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :image
      t.string :hash_key
      t.float :file_size
      t.integer :x_dim
      t.integer :y_dim
      t.integer :bit_depth
      t.string :image_remote
      t.integer :image_owner_id
      t.string :image_owner_type
      t.string :image_secure_token
      t.string :legacy_path
      t.string :comments
      t.float :lat
      t.float :long
      t.float :elevation
      t.float :f_stop
      t.string :shutter_speed
      t.integer :iso
      t.integer :focal_length
      t.datetime :capture_datetime
      t.integer :camera_id
      t.integer :lens_id
      t.string :type
      
      t.timestamps
    end
    add_index :images, :image_owner_id
    add_index :images, :image_owner_type
  end
end
