class CreatePreferences < ActiveRecord::Migration
  def change
    create_table :preferences do |t|
      t.string :mlp_library_local_root

      t.timestamps
    end
  end
end
