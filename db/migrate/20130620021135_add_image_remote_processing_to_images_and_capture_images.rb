class AddImageRemoteProcessingToImagesAndCaptureImages < ActiveRecord::Migration
  def change
    add_column :capture_images, :image_remote_processing, :boolean
    add_column :images, :image_remote_processing, :boolean
  end
end
