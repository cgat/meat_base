class RenameColumnSurveyorSurveyAssociations < ActiveRecord::Migration
  def change
    rename_column :surveyor_survey_associations, :survey_project_id, :survey_id
  end

end
