class ChangeVisitIdOnLocations < ActiveRecord::Migration
  def up
    execute <<-SQL
    ALTER TABLE locations ALTER visit_id TYPE integer USING visit_id::int
    SQL
  end
  
  def down 
    execute <<-SQL
    ALTER TABLE locations ALTER visit_id TYPE character varying(255) USING visit_id::string
    SQL
  end

  
end
