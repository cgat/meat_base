# encoding: utf-8
require 'uuidtools'

class ImageRemoteUploader < CarrierWave::Uploader::Base
  #include ::CarrierWave::Backgrounder::Delay

  # Choose what kind of storage to use for this uploader:
  storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    return nil #store at root
  end

  version :medium do
    #see meat app decorator image_remote_uploader_decorator for processing
  end
  version :thumb, from_version: :medium do
    #see meat app decorator image_remote_uploader_decorator for processing
  end

  def extension_white_list
    %w(jpg jpeg png tif tiff fff psd 3FR NEF)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
   "#{secure_token}.jpeg" if original_filename.present?
  end

  protected
  def secure_token
    model.image_secure_token ||= UUIDTools::UUID.timestamp_create().to_s
  end

  def not_jpeg?(new_file)
    !(new_file.content_type=='image/jpeg' || new_file.content_type=='image/jpg')
  end

end
