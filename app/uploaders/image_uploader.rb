# encoding: utf-8
require 'uuidtools'
require 'carrierwave/processing/mime_types'

class ImageUploader < CarrierWave::Uploader::Base
  #include CarrierWave::RMagick
  #include ModuleHelper::Helper
  # Include the Sprockets helpers for Rails 3.1+ asset pipeline compatibility:
  #include Sprockets::Rails::Helper

  include NumberHelper

  # Choose what kind of storage to use for this uploader:
  storage :file


  def default_url
    ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  end

  def processing_url
    ActionController::Base.helpers.asset_path("processing_throbber2.gif")
  end


  def move_to_store
    true
  end


  def move_to_cache
    true
  end

  #this is overridden in meat
  def store_dir
    Rails.env.test? ? "uploads/test/" : "uploads/"
  end

  version :medium do
    def store_dir
      if Rails.env.test?
        "uploads/test/versions"
      else
        "uploads/versions"
      end
    end

    def full_filename (for_file = model.image.file)
      "medium_#{secure_token}.jpeg"
    end

    def url
      if model.image.present? && File.exists?(self.file.path)
        super
      elsif model.image_tmp.present?
        self.processing_url
      else
        self.default_url
      end
    end

    def move_to_cache
      false
    end
    #see meat app image_uploader_decorator for processing
  end

  version :thumb, from_version: :medium do
    def store_dir
      if Rails.env.test?
        "uploads/test/versions"
      else
        "uploads/versions"
      end
    end

    def full_filename (for_file = model.image.file)
      "thumb_#{secure_token}.jpeg"
    end

    def url
      if model.image.present? && File.exists?(self.file.path)
        super
      elsif model.image_tmp.present?
        self.processing_url
      else
        self.default_url
      end
    end

    def move_to_cache
      false
    end
    #see meat app image_uploader_decorator for processing
  end

  def url(options={})
    if model.image.present? && File.exists?(self.file.path)
      super(options)
    elsif model.image_tmp.present?
      self.processing_url
    else
      self.default_url
    end
  end

  # Add a white list of extensions which are allowed to be uploaded.
  # For images you might use something like this:
  # def extension_white_list
  #   %w(jpg jpeg gif png)
  # end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    #when recreating versions, the image is restored, which causes this method to be called on the
    #previously renamed image (the one that already has the secure token). This just checks to see
    #if the secure_token is already there, and if it is, skips the file name adjustments step.
    if file.filename=~/#{secure_token}/
      super
    elsif  original_filename.present?
      "#{file.basename}_#{secure_token}.#{file.extension}"
    end
  end

  protected

  def secure_token
    model.image_secure_token ||= UUIDTools::UUID.timestamp_create().to_s
  end



end
