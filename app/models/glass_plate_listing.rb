class GlassPlateListing < ActiveRecord::Base
  attr_accessible :survey_season_id, :container, :plates, :notes
  belongs_to :survey_season
end
