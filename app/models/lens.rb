# == Schema Information
#
# Table name: lens
#
#  id           :integer          not null, primary key
#  brand        :string(255)
#  focal_length :string(255)
#  max_aperture :float
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Lens < ActiveRecord::Base
  attr_accessible :brand, :focal_length, :max_aperture
  
  has_many :captures
  has_many :historic_captures
  
  
  def display_name
    brand = self.brand.nil? ? "" : "#{self.brand} "
    focal_length = self.focal_length.nil? ? "" : "#{self.focal_length} "
    max_aperture = self.max_aperture.nil? ? "" : "#{self.max_aperture.to_s}"
    return brand+focal_length+max_aperture
  end
end
