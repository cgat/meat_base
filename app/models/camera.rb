class Camera < ActiveRecord::Base
  include NumberHelper
  
  attr_accessible :format, :make, :model, :unit
  
  has_many :captures
  has_many :historic_captures
  
  def display_name
    model_name = self.model.nil? ? "" : "#{self.model} "
    unit = self.unit.nil? ? "" : "#{self.unit.to_s} "
    format = self.format.nil? ? "" : "(#{self.format})"
    return model_name+unit+format
  end
  
end
