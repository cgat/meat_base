# == Schema Information
#
# Table name: historic_captures
#
#  id                      :integer          not null, primary key
#  plate_id                :string(255)
#  capture_owner_id       :integer
#  capture_owner_type     :string(255)
#  fn_photo_reference      :string(255)
#  f_stop                  :float
#  shutter_speed           :string(255)
#  focal_length            :integer
#  camerea_id              :integer
#  lens_id                 :integer
#  capture_datetime        :datetime
#  digitization_location   :string(255)
#  digitization_datetime   :datetime
#  lac_ecopy               :string(255)
#  lac_wo                  :string(255)
#  lac_collection          :string(255)
#  lac_box                 :string(255)
#  lac_catalouge           :string(255)
#  condition               :string(255)
#  comments                :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#

class HistoricCapture < ActiveRecord::Base
  include NumberHelper
  include BaseCapture
  include PgSearch

  multisearchable :against => [:fn_photo_reference]

  attr_accessible :camera_id, :capture_datetime, :capture_owner_id, :capture_owner_type, :comments, :condition, :digitization_datetime, :digitization_location, :f_stop, :fn_photo_reference, :focal_length, :lac_box, :lac_catalogue, :lac_collection, :lac_ecopy, :lac_wo, :lens_id, :plate_id, :shutter_speed
  attr_accessible :capture_images_attributes, :captures_attributes, :capture_ids

  belongs_to :camera
  belongs_to :lens
  belongs_to :capture_owner, polymorphic: true
  alias :parent :capture_owner
  has_many :comparison_indices, dependent: :destroy
  has_many :capture_images, -> { order(:image_state, :image) }, as: :captureable, dependent: :destroy
  has_many :captures, through: :comparison_indices
  alias :comparisons :captures

  accepts_nested_attributes_for :captures

  validates_uniqueness_of :fn_photo_reference, scope: [:capture_owner_id, :capture_owner_type], allow_blank: true
  validates_length_of :digitization_location, :lac_ecopy, :lac_wo, :lac_collection, :lac_box, :lac_catalogue, :comments, :fn_photo_reference, maximum: 255

  scope :has_comparisons, lambda { joins(:captures).uniq}
  scope :missing_comparisons, lambda {
    where.not({id: HistoricCapture.has_comparisons})
  }
  scope :without_capture_images, lambda {
    where.not({id: HistoricCapture.with_capture_images})
  }
  scope :has_comparisons_with_images, lambda { joins(captures: :capture_images).uniq}

  scope :mastered, lambda {
    HistoricCapture.joins(captures: :capture_images).joins(:capture_images).where( \
      %Q{
        (ABS(capture_images.x_dim-capture_images_historic_captures.x_dim)/GREATEST(capture_images.x_dim,capture_images_historic_captures.x_dim)::float*50
          +ABS(capture_images.y_dim-capture_images_historic_captures.y_dim)/GREATEST(capture_images.y_dim, capture_images_historic_captures.y_dim)::float*50)<?
        }, \
      CaptureImage::SIMILARITY_THRES).uniq
  }
  scope :not_mastered,  lambda {
    HistoricCapture.where.not(id: HistoricCapture.mastered ).where(id: HistoricCapture.has_comparisons)
  }

  def add_comparison(capture)
    unless self.captures.include?(capture)
      comparison_indices.create(capture_id: capture.id)
    else
      capture
    end
  end

  def remove_comparison(capture)
    comparison_indices.find_by_capture_id(capture.id).destroy
  end

  def comparable?(capture)
    comparison_indices.find_by_capture_id(capture.id)
  end

  def historic_visit
    self.capture_owner if self.capture_owner_type=="HistoricVisit"
  end

  def station
    case self.capture_owner_type
    when "HistoricVisit"
      self.capture_owner.station
    when "Station"
      self.capture_owner
    else
      nil
    end
  end

  def survey_season
    station.present? && station.station_owner_type=='SurveySeason' ? station.station_owner : nil
  end

  def year
    survey_season.present? ? survey_season.year : nil
  end

end
