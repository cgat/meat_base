class Map < ActiveRecord::Base
  attr_accessible :survey_season_id, :nts_map, :historic_map, :links
  belongs_to :survey_season
end
