class ScenicImage < Image
  validates :image_owner_type, inclusion: { in: ['Survey', 'SurveySeason', 'Station', 'Project']}
end
