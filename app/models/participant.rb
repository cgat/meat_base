# == Schema Information
#
# Table name: participants
#
#  id          :integer          not null, primary key
#  last_name   :string(255)
#  given_names :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Participant < ActiveRecord::Base
  attr_accessible :given_names, :last_name

  has_many :hiking_parties
  has_many :visits, through: :hiking_parties
  has_and_belongs_to_many :visits_photographed, class_name: 'Visit', join_table: :photographers_visits
  has_and_belongs_to_many :visits_authored, class_name: 'Visit', join_table: :fn_authors_visits

  validates_uniqueness_of :last_name, scope: :given_names
  validates_uniqueness_of :given_names, scope: :last_name
  validates :last_name, presence: true, length: {maximum: 255}
  validates :given_names, presence: true, length: {maximum: 255}

  def display_name
    full_name
  end

  def full_name
    self.given_names+" "+self.last_name
  end
end
