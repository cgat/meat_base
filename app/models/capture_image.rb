# == Schema Information
#
# Table name: capture_images
#
#  id               :integer          not null, primary key
#  captureable_id       :integer
#  hash_key         :string(255)
#  image :text
#  file_size        :float
#  x_dim            :integer
#  y_dim            :integer
#  bit_depth        :integer
#  image_state      :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class CaptureImage < ActiveRecord::Base
  include NumberHelper
  include ImageHelper
  include PgSearch

  multisearchable :against => [:image_state, :display_name]

  attr_accessible :captureable_id, :captureable_type, :bit_depth, :image, :image_remote, :file_size, :hash_key, :image_state, :x_dim, :y_dim, :remote, :image_secure_token, :comments, :legacy_path
  attr_accessible :image_cache, :do_not_save_metadata
  attr_accessor :do_not_save_metadata


  before_update :save_metadata, if: '!self.do_not_save_metadata'
  before_create :save_metadata, if: '!self.do_not_save_metadata'

  belongs_to :captureable, polymorphic: true
  alias :parent :captureable
  delegate :station, to: :captureable, allow_nil: true

  #see the resolution_similar? method for usage
  SIMILARITY_THRES = 0.3

  mount_uploader :image, ImageUploader

  mount_uploader :image_remote, ImageRemoteUploader


  validates :image_state, inclusion: { in: IMAGE_STATES }
  #validates :image, presence: true, if: 'self.new_record?'
  validates :captureable_type, inclusion: { in: ['Capture','HistoricCapture']}

  scope :remotely_uploaded, lambda { where.not(image_remote: [nil, '']) }
  #published aliases remotely_uploaded in the content of a capture image
  scope :published, lambda { remotely_uploaded }


  scope :not_processing, lambda { where(image_tmp: [nil, '']) }
  def file_present?
    if image.present?
      File.exists?(self.image.file.path)
    elsif image_tmp.present?
      true
    else
      false
    end
  end

  #we add a dynamic search column (display_name) in multisearchable
  #so we need to override the default pg_search rebuild with this
  #naive approach (slow)
  def self.rebuild_pg_search_documents
    find_each { |record| record.update_pg_search_document }
  end

  #used in comparisons when listing image files for capture/hcapture
  def image_listing_name
    return "#{self.image_state} - (#{self.x_dim}x#{self.y_dim}) - #{self.display_name}"
  end

  #read_attribute is used here to get the actual value
  #stored in the db row, rather than the mounted uploader object
  def display_name
    image_full_name = read_attribute("image")
    if image_full_name.present?
      image_full_name.sub("_"+self.image_secure_token, "")
    elsif image_tmp.present? && image_tmp=~/.*\/([^\/]+)$/
      $1
    else
      "UNKNOWN"
    end
  end

  def remote
    image_remote.present?
  end

  def remote=(remote_bool)
    remote_state = (remote_bool==true || remote_bool==1 || remote_bool=="1")
    #if the remote state changes, change image_remote (either add file or remove file)
    if (remote_state && !remote) || (!remote_state && remote)
      attribute_will_change!(:image_remote)
      config_for_remote_upload(remote_state)
    end
  end

  def save_metadata
    if File.exists?(self.image.file.path) || File.exists?(self.fs_path)
      begin
        path = File.exists?(self.image.file.path) ? self.image.file.path : self.fs_path
        @exif = MiniExiftool.new(path)
      rescue ArgumentError => e
        #with some files it miniexiftool throws an exception when there in invalid UTF-8 characters in the metadata
        if e.message == "invalid byte sequence in UTF-8"
          return true #silently fail.
        else
          raise e
        end
      end
    else
      return true #silently fail when file is missing
    end
    self.x_dim = @exif.file_type == "RAF" ?  @exif.raw_image_width : @exif.imagewidth
    self.y_dim = @exif.file_type == "RAF" ?  @exif.raw_image_height : @exif.imageheight
    self.bit_depth = @exif.bitspersample
    self.file_size = self.image.file.size
    if self.captureable_type=='Capture'
      capture = Capture.find_by_id(self.captureable_id)
      if !capture.nil? && (capture.capture_images.empty? || !capture.metadata_set?)
        update_hash = {}
        #exif can produce results as strings or as numerics. For us, it benefits to use both. By default, the miniexiftool will output string, so do this, then change to numerics
        if @exif.datetimeoriginal
          update_hash[:capture_datetime] = @exif.datetimeoriginal
        end
        update_hash[:shutter_speed] = (@exif.shutterspeed ||= @exif.exposuretime)
        update_hash[:shutter_speed] = update_hash[:shutter_speed].to_s #store as a string instead of a rational
        update_hash[:camera_make] = @exif.make
        update_hash[:camera_model] = @exif.model
        @exif.numerical = true
        @exif.reload
        update_hash[:iso] = @exif.iso
        update_hash[:f_stop] = @exif.aperture ||= @exif.fnumber
        update_hash[:focal_length] = @exif.focallength
        update_hash[:lat] = @exif.gpslatitude
        update_hash[:long] = @exif.gpslongitude
        update_hash[:elevation] = @exif.gpsaltitude
        capture.update_attributes(update_hash)
        true #return, even if the capture update fails, allowing  the capture image to be saved regardless of whether the capture update is successfully.
      end
    end
  end

  def aligned?(cap_img)
    if (x_dim==cap_img.x_dim && y_dim==cap_img.y_dim) || resolution_similar?(cap_img)
      return true
    else
      return false
    end
  end

  #Checks to see if the resolutions are similar
  def resolution_similar?(cap_img)
    return false if x_dim.blank? || y_dim.blank? || cap_img.blank? || cap_img.x_dim.blank? || cap_img.y_dim.blank?
    return SIMILARITY_THRES>(x_dim-cap_img.x_dim).abs/[x_dim,cap_img.x_dim].max.to_f*50 + (y_dim-cap_img.y_dim).abs/[y_dim,cap_img.y_dim].max.to_f*50
  end



end
