# == Schema Information
#
# Table name: keyword_visit_associations
#
#  id         :integer          not null, primary key
#  keyword_id :integer
#  visit_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class KeywordVisitAssociation < ActiveRecord::Base
  attr_accessible :keyword_id, :visit_id

  after_save 'visit.update_pg_search_document'

  belongs_to :visit
  belongs_to :keyword
end
