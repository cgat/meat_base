# == Schema Information
#
# Table name: locations
#
#  id                 :integer          not null, primary key
#  visit_id           :string(255)
#  location_narrative :text
#  location_identity  :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Location < ActiveRecord::Base
  include NumberHelper
  include Coordinateable

  after_save do |test|
    if visit.present?
      visit.reload
      visit.update_pg_search_document
    end
  end

  attr_accessible :location_identity, :location_narrative, :visit_id, :captures_attributes, :location_photos, :location_photos_attributes, :location_images_attributes, :legacy_photos_start, :legacy_photos_end
  #not the best place for this variable, but with it we can select multiple photos in the locations form
  #and still be able to specify whether we want the underlying location photos to be uploaded to remote
  #storage. With location photos, it will be an all or none option.
  attr_accessible :remote_location_photos
  attr_accessor :remote_location_photos

  belongs_to :visit, inverse_of: :locations
  alias :parent :visit
  delegate :station, to: :visit, allow_nil: true
  has_many :captures, as: :capture_owner, dependent: :destroy
  has_many :location_photos, -> { order :image }, as: :image_owner, class_name: 'LocationImage', dependent: :destroy


  accepts_nested_attributes_for :captures, :allow_destroy => true, reject_if: lambda {|a| a[:fn_photo_reference].blank? && a[:azimuth].blank? && a[:comments].blank? &&\
                                                                                          (a[:capture_images_attributes].nil? || \
                                                                                          a[:capture_images_attributes].any?{|k,v| v[:image].blank? && v[:image_cache].blank? && v[:image_state].blank?})}
  accepts_nested_attributes_for :location_photos, :allow_destroy => true

  validates :location_identity, presence: true
  validates :visit, presence: true
  validates_uniqueness_of :location_identity, scope: [:visit_id]

  scope :where_parent, lambda { |obj|
    where({visit_id: obj.id})
  }

  scope :published, lambda { where(published: true) }

  def capture?(capture)
    captures.find_by_id(capture.id)
  end

  def location_photo?(location_photo)
    location_photos.find_by_id(location_photo.id)
  end


  def location_photos=(attrs)
    attrs.each { |attr| self.location_photos.build(image: attr, remote: self.remote_location_photos )}
  end

  def display_name
    "Loc#{self.location_identity}"
  end

end
