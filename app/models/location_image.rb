class LocationImage < Image
  validates :image_owner_type, inclusion: { in: ['Location', 'Visit','Station'] }
end
