class MetadataFile < ActiveRecord::Base
  include NumberHelper

  attr_accessible :legacy_path, :metadata_file, :metadata_owner_id

  belongs_to :metadata_owner, polymorphic: true
  alias :parent :metadata_owner
  mount_uploader :metadata_file, FileUploader


  validates :legacy_path, length: {maximum: 255}, allow_blank: true
  validates :metadata_file, presence: true, if: 'self.new_record?'
  #metadata_owner is technically required, but this validation
  #will cause problems when used with nested attributes on
  #polymorhpic objects such as this.
  #validates :metadata_owner, presence: true
  def file_present?
    File.exists?(self.metadata_file.file.path)
  end

  def display_name
    self.metadata_file.file.filename
  end

end
