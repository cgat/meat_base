class Image < ActiveRecord::Base
  include NumberHelper
  include ImageHelper
  include Coordinateable
  include PgSearch

  multisearchable :against => [:display_name]

  attr_accessible :bit_depth, :camera_id, :capture_datetime, :comments, :f_stop, :file_size, :focal_length, :hash_key, :image, :image_owner_id, :image_owner_type, :image_remote, :image_secure_token, :iso, :legacy_path, :lens_id, :shutter_speed, :x_dim, :y_dim, :type
  attr_accessible :image_cache, :remote

  before_create :set_metadata

  belongs_to :image_owner, polymorphic: true
  alias :parent :image_owner
  belongs_to :camera
  belongs_to :lens

  scope :remotely_uploaded, lambda { where.not(image_remote: [nil, '']) }

  mount_uploader :image, ImageUploader
  mount_uploader :image_remote, ImageRemoteUploader


  validates :image, presence: true, if: 'self.new_record?'
  #While conceptually we don't want an image without a parent, image_owner is polymorphic
  #and thus cannot contain inverse_of... which means the below validation won't work for
  #nested attributes/forms
  #validates :image_owner, presence: true
  validates_numericality_of :f_stop, :iso, :focal_length, :file_size, :x_dim, :y_dim, allow_blank: true
  validates :focal_length, numericality: {greater_than: 0}, allow_blank: true
  validate :allowable_formats_for_remote, if: 'remote'


  #read_attribute is used here to get the actual value
  #stored in the db row, rather than the mounted uploader object
  def display_name
    image_full_name = read_attribute("image")
    if image_full_name.present?
      image_full_name.sub("_"+self.image_secure_token, "")
    elsif image_tmp.present? && image_tmp=~/.*\/([^\/]+)$/
      $1
    else
      "UNKNOWN"
    end
  end

  #we add a dynamic search column (display_name) in multisearchable
  #so we need to override the default pg_search rebuild with this
  #naive approach (slow)
  def self.rebuild_pg_search_documents
    find_each { |record| record.update_pg_search_document }
  end

  def file_present?
    if image.present?
      File.exists?(self.image.file.path)
    elsif image_tmp.present?
      true
    else
      false
    end
  end

  def remote
    image_remote.present?
  end

  def remote=(remote_bool)
    #attribute_will_change!(:remote)
    remote_state = (remote_bool==true || remote_bool==1 || remote_bool=="1")
    #if the remote state changes, change image_remote (either add file or remove file)
    if (remote_state && !remote) || (!remote_state && remote)
      attribute_will_change!(:remote)
      config_for_remote_upload(remote_state)
    end
  end

  def allowable_formats_for_remote
    if image.present? && image.file.filename=~/\.(3FR)$/i
      errors.add(:remote, "Remote copying of 3FR images is not currently supported.")
    end
  end

  def set_metadata
    if File.exists?(self.image.file.path) || File.exists?(self.fs_path)
      begin
        path = File.exists?(self.image.file.path) ? self.image.file.path : self.fs_path
        @exif = MiniExiftool.new(self.image.file.path)
      rescue ArgumentError => e
        #with some files it miniexiftool throws an exception when there in invalid UTF-8 characters in the metadata
        if e.message == "invalid byte sequence in UTF-8"
          return true #silently fail.
        else
          raise e
        end
      end
    else
      return true #silently fail when file is missing
    end
    self.x_dim = @exif.imagewidth if @exif.imagewidth.present?
    self.y_dim = @exif.imageheight if @exif.imageheight.present?
    self.bit_depth = @exif.bitspersample if @exif.bitspersample.present?
    self.file_size = self.image.file.size
    #exif can produce results as strings or as numerics. For us, it benefits to use both. By default, the miniexiftool will output string, so do this, then change to numerics
    if @exif.datetimeoriginal
      self.capture_datetime = @exif.datetimeoriginal
    end
    self.shutter_speed = (@exif.shutterspeed ||= @exif.exposuretime) if (@exif.shutterspeed ||= @exif.exposuretime).present?
    camera_make = @exif.make if @exif.make.present?
    camera_model = @exif.model if @exif.model.present?
    set_camera_id(camera_make,camera_model) if @exif.camera_model
    @exif.numerical = true
    @exif.reload
    self.iso = @exif.iso if @exif.iso.present?
    self.f_stop = @exif.aperture ||= @exif.fnumber if (@exif.aperture ||= @exif.fnumber).present?
    self.focal_length = @exif.focallength if @exif.focallength.present?
    self.lat = @exif.gpslatitude if @exif.gpslatitude.present?
    self.long = @exif.gpslongitude if @exif.gpslongitude.present?
    self.elevation = @exif.gpsaltitude if @exif.gpsaltitude.present?
  end

  def set_camera_id(camera_make,camera_model)
    if camera_make || camera_model
      camera = Camera.find_by_model(camera_model)
      #if camera doesn't exist, create it
      if camera.nil?
        camera = Camera.create(make: camera_make, model: camera_model)
      end
      self.camera_id = camera.id
    end
  end

  def station
    case self.image_owner_type
    when 'Location'
      self.image_owner.visit.station
    when 'Visit'
      self.image_owner.station
    when 'Station'
      self.image_owner
    else
      nil
    end
  end

end



