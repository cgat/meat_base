# == Schema Information
#
# Table name: historic_visits
#
#  id         :integer          not null, primary key
#  station_id :integer
#  date       :date
#  comments   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class HistoricVisit < ActiveRecord::Base

  attr_accessible :comments, :date, :station_id
  attr_accessible :historic_captures_attributes
  attr_accessible :survey_season_id, :survey_id, :surveyor_id
  attr_accessor  :survey_season_id, :survey_id, :surveyor_id


  belongs_to :station, inverse_of: :historic_visit
  alias :parent :station
  has_many :historic_captures, as: :capture_owner, dependent: :destroy
  accepts_nested_attributes_for :historic_captures, :allow_destroy => true,  reject_if: lambda {|a| a[:fn_photo_reference].blank? &&
                                                                                          (a[:capture_images_attributes].nil? || \
                                                                                          a[:capture_images_attributes].any?{|k,v| v[:image].blank? && v[:image_cache].blank? && v[:image_state].blank?})}

  validates :station, presence: true
  validate :date_in_survey_season?

  scope :where_parent, lambda { |obj|
    where({station_id: obj.id})
  }

  scope :published, lambda { where(published: true) }

  def date_in_survey_season?
    if self.date.present? && survey_season.present? && survey_season.year.present? && self.date.year != survey_season.year
      errors.add(:date, "The date must be in the same year as its survey season")
    end
  end

  def historic_capture?(historic_capture)
    historic_captures.find_by_id(historic_capture.id)
  end

  def survey_season
    if !self.station.nil? && self.station.station_owner_type=="SurveySeason"
      self.station.station_owner
    else
      nil
    end
  end

  def survey
    if survey_season
      survey_season.survey
    else
      nil
    end
  end

  def surveyor
    if survey
      survey.surveyor
    else
      nil
    end
  end

  def display_name
    "Historic"
  end


end
