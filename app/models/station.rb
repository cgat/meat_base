
class Station < ActiveRecord::Base
  include NumberHelper
  include Coordinateable
  include HasMetadata
  include PgSearch

  multisearchable :against => [:name, :nts_sheet]

  attr_accessible :name, :nts_sheet, :station_owner_id, :station_owner_type
  #By having these availalbes as accessor variables, I can make more conherent forms (because I allow user to change the parents and grandparents)
  attr_accessible :survey_id, :surveyor_id
  attr_accessor :survey_id, :surveyor_id

  has_many :visits, inverse_of: :station, dependent: :destroy
  has_many :locations, through: :visits
  has_many :located_captures, through: :locations, source: :captures, dependent: :destroy
  has_many :unsorted_in_visit_captures, through: :visits, source: :unsorted_captures, dependent: :destroy
  has_many :unsorted_captures, as: :capture_owner, class_name: "Capture", dependent: :destroy
  has_many :unsorted_location_photos, -> { order :image }, as: :image_owner, class_name: "LocationImage", dependent: :destroy
  has_many :visits_location_photos, -> { order :image }, through: :visits, source: :unsorted_location_photos, dependent: :destroy
  has_many :sorted_location_photos, -> { order :image }, through: :visits, source: :sorted_location_photos, dependent: :destroy
  belongs_to :station_owner, polymorphic: true
  alias :parent :station_owner
  has_one :historic_visit, inverse_of: :station , dependent: :destroy
  has_many :historic_captures, through: :historic_visit

  validates :name, presence: true, length: {maximum: 255}
  validates :station_owner_type, inclusion: {in: ['Project','SurveySeason']}
  validates :station_owner, presence: true
  validates_uniqueness_of :name, scope: [:station_owner_id, :station_owner_type]

  scope :where_parent, lambda { |obj|
    where({station_owner_id: obj.id, station_owner_type: obj.class.name})
  }

  scope :published, lambda { where(published: true) }
  #merge_captures is a helper scope. There are several station scopes that are based on the context of
  #the captures that are stored within the station. Captures can be stored in location, visits, or directly
  #underneath the station. This scope accepts a Capture scope (such as Capture.all, Capture.missing_comparisons)
  #which is merged with the joins captures (based on location).
  scope :merge_captures, lambda { |cap_merge_scope|
    scoped_caps_ids = cap_merge_scope.pluck(:id)
    where("stations.id IN (?) OR stations.id IN (?) OR stations.id IN (?)",
      Station.select(:id).distinct.joins(:unsorted_captures).where(captures: { id: scoped_caps_ids}),
      Station.select(:id).distinct.joins(:located_captures).where(captures: { id: scoped_caps_ids}),
      Station.select(:id).distinct.joins(:unsorted_in_visit_captures).where(captures: { id: scoped_caps_ids}),
      )
  }
  scope :remotely_uploaded, lambda {
    stations_remote_through_ucaps = Station.select(:id).joins(unsorted_captures: :capture_images).merge(CaptureImage.remotely_uploaded).uniq
    stations_remote_through_vucaps = Station.select(:id).joins(unsorted_in_visit_captures: :capture_images).merge(CaptureImage.remotely_uploaded).uniq
    stations_remote_through_lcaps = Station.select(:id).joins(located_captures: :capture_images).merge(CaptureImage.remotely_uploaded).uniq
    stations_remote_thorugh_hcaps = Station.select(:id).joins(historic_captures: :capture_images).merge(CaptureImage.remotely_uploaded).uniq
    where("stations.id IN (?) OR stations.id IN (?) OR stations.id IN (?) OR stations.id IN (?)",
      stations_remote_through_ucaps,
      stations_remote_through_vucaps,
      stations_remote_through_lcaps,
      stations_remote_thorugh_hcaps)
  }
  scope :with_hcaptures, lambda { Station.joins(:historic_captures).uniq}
  scope :without_hcaptures, lambda { Station.where.not(id: Station.with_hcaptures)}
  scope :with_captures, lambda { Station.merge_captures(Capture.all) }
  scope :without_captures, lambda { Station.where.not(id: Station.with_captures) }
  scope :grouped_state, lambda {
    where(lat: nil, long: nil)
    .where(id: Station.without_captures)
  }
  scope :located_state, lambda {
    where.not(lat: nil, long: nil)
    .where(id: Station.without_captures)
  }
  scope :repeated_state, lambda {
    Station.where(id: Station.with_captures).where.not(id: Station.merge_captures(Capture.mastered))
  }
  #A little explanation about the mastered_states below. Merging Station with Capture.mastered will yield all stations that have
  #at least one capture that is mastered. Merging Station with Capture.not_mastered will yield all stations that have at least
  #one capture that is not mastered. For partially_mastered_state, we want all stations that have at least one mastered capture,
  #but not all mastered. For mastered_state, we want station where all captures are mastered.
  scope :partially_mastered_state, lambda {
    where("stations.id IN (?) AND stations.id IN (?)",
      Station.select(:id).merge_captures(Capture.mastered),
      Station.select(:id).merge_captures(Capture.not_mastered))
  }
  scope :mastered_state, lambda {
    where("stations.id IN (?) AND stations.id NOT IN (?)",
      Station.select(:id).merge_captures(Capture.mastered),
      Station.select(:id).merge_captures(Capture.not_mastered))
  }

  scope :not_repeated, lambda {
    where("stations.id IN (?) AND stations.id IN (?)",
      Station.select(:id).with_hcaptures,
      Station.select(:id).without_captures)
  }
  scope :has_hcaps_with_missing_comparisons, lambda {
    where("stations.id IN (?) AND stations.id IN (?)",
      Station.select(:id).with_captures,
      Station.select(:id).distinct.joins(:historic_captures).merge(HistoricCapture.missing_comparisons))
  }
  scope :has_caps_with_missing_comparisons, lambda {
    Station.merge_captures(Capture.missing_comparisons)
  }
  scope :has_caps_without_capture_images, lambda {
    Station.merge_captures(Capture.without_capture_images)
  }
  scope :has_hcaps_without_capture_images, lambda {
    Station.joins(:historic_captures).merge(HistoricCapture.without_capture_images).uniq
  }
  scope :has_visits_with_not_transcribed_but_with_fns, lambda {
    Station.joins(visits: :field_notes).merge(Visit.not_transcribed).uniq
  }
  scope :has_visits_no_scanned_fns, lambda {
    Station.joins(:visits).merge(Visit.without_scanned_field_notes).uniq
  }
  scope :has_captures_without_azimuths, lambda {
    Station.merge_captures(Capture.without_azimuth)
  }

  scope :with_issues, lambda {
    where((['stations.id IN (?)'] * Station.issue_filters.size).join(' OR '), *Station.issue_filters.map{|f| Station.send(f).select(:id).uniq})
  }

  def self.issue_filters
    [ :has_hcaps_with_missing_comparisons, \
      :has_caps_with_missing_comparisons, \
      :has_caps_without_capture_images, \
      :has_hcaps_without_capture_images, \
      :has_visits_with_not_transcribed_but_with_fns, \
      :has_visits_no_scanned_fns, \
      :has_captures_without_azimuths
    ]
  end

  def self.process_filters
    [ :grouped_state, \
      :located_state, \
      :repeated_state, \
      :partially_mastered_state, \
      :mastered_state]
  end

  def self.other_filters
    []
  end

  def self.filters
    [:remotely_uploaded, \
      :has_hcaps_with_missing_comparisons, \
      :has_caps_with_missing_comparisons, \
      :has_caps_without_capture_images, \
      :has_hcaps_without_capture_images, \
      :has_visits_with_not_transcribed_but_with_fns]
  end

  def display_name
    self.name
  end

  def captures
    lcaps = located_captures.select("captures.id")
    ucaps = unsorted_captures.select("captures.id")
    vcaps = unsorted_in_visit_captures.select("captures.id")
    Capture.where("captures.id IN (?) OR captures.id IN (?) OR captures.id IN (?)", lcaps, ucaps, vcaps)
  end

  def location_photos
    sloc_photos = sorted_location_photos.select("images.id")
    uloc_photos = unsorted_location_photos.select("images.id")
    vloc_photos = visits_location_photos.select("images.id")
    LocationImage.where("images.id IN (?) OR images.id IN (?) OR images.id IN (?)",
      sloc_photos, uloc_photos, vloc_photos)
  end

  def test_locations
    locations
  end

  def latitude
    lat or
    (unsorted_captures+located_captures+unsorted_in_visit_captures).find{|a|a.lat.present?}.try(:lat) or
    locations.find{|a|a.lat.present?}.try(:lat) or
    nil
  end

  def longitude
    long or
    (unsorted_captures+located_captures+unsorted_in_visit_captures).find{|a|a.long.present?}.try(:long) or
    locations.find{|a|a.long.present?}.try(:long) or
    nil
  end

  ####
  #This function will upload one capture image for each
  #capture and historic capture understand this station
  #(with the exception of unsorted captures) in addition
  #to location photos. See the capture/historic capture
  #preview_capture_image method for information
  #on which image is chosen.
  def make_remote_children
    captures.each do |c|
      ci = c.preview_capture_image
      if ci.present?
        ci.update_attributes({remote: true})
      end
    end
    historic_captures.each do |hc|
      ci = hc.preview_capture_image
      if ci.present?
        ci.update_attributes({remote: true})
      end
    end
    location_photos.each do |lp|
      lp.update_attributes({ remote: true })
    end
  end

  def make_unremote_children
    images = captures.map{|c| c.capture_images }.flatten+historic_captures.map{|c| c.capture_images}.flatten+location_photos.flatten
    images.each{|i| i.update_attributes(remote: false) }
  end


  def legacy_path
    if self.station_owner_type=='SurveySeason' && self.station_owner.legacy_path.present?
      return File.join(self.station_owner.legacy_path,"Stations",self.name)
    elsif self.station_owner_type=='Project'
      self.captures.each do |cap|
        if (ci=cap.capture_images.first) && ci.legacy_path.present?
          return ci.legacy_path[/.*Stations\/[^\/]+/]
        end
      end
      #if it hasn't already returned, try historic
      if self.historic_visit.present? && self.historic_visit.historic_captures.present?
        self.historic_visit.historic_captures.each do |hcap|
          if (ci=hcap.capture_images.first) && ci.legacy_path.present?
            return ci.legacy_path[/.*Stations\/[^\/]+/]
          end
        end
      end
    end
    return "" #safer to return blank string in case this is pasted to Dir.exists?
  end

  def survey
    if !self.station_owner.nil? && self.station_owner_type=='SurveySeason'
      self.station_owner.survey
    else
      nil
    end
  end

  def surveyor
    s = self.survey
    if s.present?
      s.surveyor
    else
      nil
    end
  end


  #### some methods to help me assess whether the station is suspected of having an import problem ####
  #upon import, captures should have more than one image (typically)
  def has_one_image_captures?
    self.captures.any?{|a|a.capture_images.count==1}
  end
  def has_one_image_hcaptures?
    self.historic_captures.any?{|a|a.capture_images.count==1}
  end
  def has_zero_image_captures?
    self.captures.any?{|a|a.capture_images.count==0}
  end
  def has_zero_image_hcaptures?
    self.historic_captures.any?{|a|a.capture_images.count==0}
  end
  #returns the number of captures without comparisons, or nil if none
  def num_of_captures_without_comparisons
    without_comparisons = self.captures.select{|c| c.historic_captures.empty?}
    without_comparisons.count==0? nil: without_comparisons.count
  end
  def num_of_hcaptures_without_comparisons
    without_comparisons = self.historic_captures.select{|hc| hc.captures.empty?}
    without_comparisons.count==0? nil: without_comparisons.count
  end
  def suspected_transcribed_fn_import_failure?
    self.visits.any?{|a|!a.from_transcribed_field_notes}
  end


end
