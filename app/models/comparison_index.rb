# == Schema Information
#
# Table name: comparison_indices
#
#  id          :integer          not null, primary key
#  capture_id   :integer
#  historic_capture_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ComparisonIndex < ActiveRecord::Base
  
  attr_accessible :historic_capture_id, :capture_id
  
  belongs_to :capture, class_name: 'Capture'
  belongs_to :historic_capture, class_name: 'HistoricCapture', foreign_key: 'historic_capture_id'

  validates_uniqueness_of :capture_id, scope: [:historic_capture_id]
end
