# == Schema Information
#
# Table name: visits
#
#  id                          :integer          not null, primary key
#  station_id                  :integer
#  date                        :date
#  start_time                  :time
#  finish_time                 :time
#  pilot                       :string(255)
#  rw_call_sign                :string(255)
#  photographer_participant_id :integer
#  fn_author_participant_id    :integer
#  visit_narrative             :text
#  illustration                :boolean
#  weather_narrative           :text
#  weather_temp                :float
#  weather_ws                  :float
#  weather_gs                  :float
#  weather_pressure            :float
#  weather_rh                  :float
#  weather_wb                  :float
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#

class Visit < ActiveRecord::Base
  include NumberHelper
  include HasMetadata
  include PgSearch

  multisearchable :against => [:visit_narrative, :weather_narrative, :date, :searchable_keywords, :searchable_participants, :searchable_location_narratives]

  attr_accessible :date, :finish_time, :illustration, :pilot, :rw_call_sign, :start_time, :station_id, :visit_narrative, :weather_gs, :weather_narrative, :weather_pressure, :weather_rh, :weather_temp, :weather_wb, :weather_ws, :fn_physical_location, :fn_transcription_comment
  attr_accessible :locations_attributes, :hiking_party_participants_attributes, :hiking_party_participant_ids, :fn_author_ids, :photographer_ids, :historic_capture_ids, :keywords, :keyword_ids, :field_notes, :field_notes_attributes
  #By having these availalbes as accessor variables, I can make more conherent forms (because I allow user to change the parents and grandparents)
  attr_accessible :survey_id, :survey_season_id, :surveyor_id
  attr_accessor :survey_id, :survey_season_id, :surveyor_id

  belongs_to :station, inverse_of: :visits
  alias :parent :station
  has_and_belongs_to_many :photographers, -> { order :last_name }, join_table: :photographers_visits, class_name: 'Participant'
  has_and_belongs_to_many :fn_authors, -> { order :last_name }, join_table: :fn_authors_visits, class_name: 'Participant'
  has_many :locations, -> { order :location_identity }, inverse_of: :visit, dependent: :destroy
  has_many :sorted_location_photos, -> { order :image }, through: :locations, source: :location_photos
  has_many :located_captures, through: :locations, source: :captures
  has_many :keyword_visit_associations, dependent: :destroy
  has_many :keywords, -> { order :keyword }, through: :keyword_visit_associations
  has_many :hiking_parties, dependent: :destroy
  has_many :hiking_party_participants, -> { order :last_name }, through: :hiking_parties, source: :participant
  has_many :field_notes, -> { order :field_note_file }, inverse_of: :visit, dependent: :destroy
  has_many :unsorted_captures, as: :capture_owner, class_name: "Capture", dependent: :destroy
  has_many :unsorted_location_photos, -> { order :image },  as: :image_owner, class_name: "LocationImage", dependent: :destroy

  accepts_nested_attributes_for :locations, :allow_destroy => true
  accepts_nested_attributes_for :hiking_party_participants
  accepts_nested_attributes_for :keywords
  accepts_nested_attributes_for :field_notes, :allow_destroy => true
  accepts_nested_attributes_for :photographers
  accepts_nested_attributes_for :fn_authors

  validates_uniqueness_of :date, scope: [:station_id]
  validates :pilot, length: { maximum: 20 }
  validates :rw_call_sign, length: { maximum: 30 }
  validates :weather_temp, numericality: {greater_than_or_equal_to: -50, less_than_or_equal_to: 50 }, allow_blank: true
  validates :weather_ws, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  validates :weather_gs, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  validates :weather_wb, numericality: {greater_than_or_equal_to: -50 }, allow_blank: true
  validates :weather_pressure, numericality: {greater_than_or_equal_to: 0 }, allow_blank: true
  validates :weather_rh, numericality: {greater_than_or_equal_to: 0, less_than_or_equal_to: 100}, allow_blank: true
  validates :date, presence: true
  validates :station, presence: true
  validate :starts_before_finish?
  validate :not_future?

  scope :where_parent, lambda { |obj|
    where({station_id: obj.id})
  }

  scope :published, lambda { where(published: true) }

  #complex scopes written as class methods
  scope :remotely_uploaded, lambda {
    visits_remote_through_ucaps = Visit.select(:id).distinct.joins(unsorted_captures: :capture_images).merge(CaptureImage.remotely_uploaded)
    visits_remote_through_lcaps = Visit.select(:id).distinct.joins(located_captures: :capture_images).merge(CaptureImage.remotely_uploaded)
    Visit.where("visits.id IN (?) OR visits.id IN (?)", visits_remote_through_ucaps, visits_remote_through_lcaps)
  }
  scope :transcribed, lambda {
    query = "(NOT(visits.visit_narrative IS NULL OR visits.visit_narrative = '')) OR" \
    "(NOT(visits.weather_narrative IS NULL OR visits.weather_narrative = ''))"
    where(query)
  }
  scope :not_transcribed, lambda { where.not(id: Visit.transcribed) }
  scope :with_scanned_field_notes, lambda{ joins(:field_notes).distinct }
  scope :without_scanned_field_notes, lambda { where.not(id: Visit.with_scanned_field_notes) }

  #we add a dynamic search method (all searchable_* methods) in multisearchable
  #so we need to override the default pg_search rebuild with this
  #naive approach (slow)
  def self.rebuild_pg_search_documents
    find_each { |record| record.update_pg_search_document }
  end

  def display_name
    self.date.strftime("%Y-%m-%d")
  end

  #Returns all of the captures related to this visit, directly (unsorted_captures) or indirectly (located_captures)
  def captures
    lcaps = located_captures.select("captures.id")
    ucaps = unsorted_captures.select("captures.id")
    Capture.where("captures.id IN (?) OR captures.id IN (?)", lcaps, ucaps)
  end

  def capture_images
    captures = self.located_captures.includes(:capture_images)+self.unsorted_captures.includes(:capture_images) # fetch the captures with eager loading
    return captures.map{|c|c.capture_images}.flatten.compact
  end

  def location_photos
    sloc_photos = sorted_location_photos.select("images.id")
    uloc_photos = unsorted_location_photos.select("images.id")
    LocationImage.where("images.id IN (?) OR images.id IN (?)", sloc_photos, uloc_photos)
  end


  def keyword?(keyword)
    keyword_visit_associations.find_by_keyword_id(keyword.id)
  end

  def add_keyword(keyword)
    keyword_visit_associations.create(keyword_id: keyword.id)
  end

  def remove_keyword(keyword)
    keyword_visit_associations.find_by_keyword_id(keyword.id).destroy
  end

  def hiking_party_participant?(participant)
    hiking_parties.find_by_participant_id(participant.id)
  end

  def add_party_participant(participant)
    hiking_parties.create(participant_id: participant.id)
  end

  def remove_party_participant(participant)
    hiking_parties.find_by_participant_id(participant.id).destroy
  end

  def location?(location)
    locations.find_by_id(location.id)
  end

  def field_notes=(attrs)
    attrs.each { |attr| self.field_notes.build(field_note_file: attr )}
  end


  def survey_season
    if !self.station.nil?
      self.station.survey_season
    else
      nil
    end
  end

  def survey
    if survey_season
      survey_season.survey
    else
      nil
    end
  end

  def surveyor
    if survey
      survey.surveyor
    else
      nil
    end
  end

  def starts_before_finish?
    if !(self.start_time.nil?) && !(self.finish_time.nil?) && !(self.start_time<self.finish_time)
      errors.add(:start_time, "start time must occur before finish time")
    end
  end

  def not_future?
    if !(self.date.nil?) && ! (Date.today >= self.date)
      errors.add(:date, "the date cannot occur in the future")
    end
  end

private
  def searchable_keywords
    #we need to reload the keywords in case multiple keywords have been added
    keywords.reload.inject(""){ |memo,v| memo+="#{v.keyword or ""} " }
  end

  def searchable_participants
    #we need to reload the participants in case muliple people have been added
    hiking_party_participants.reload.inject(""){ |memo,v| memo+="#{v.full_name or ""} " }
  end

  def searchable_location_narratives
    #we need to reload location in case multiple locations have been added
    locations.reload.inject(""){ |memo,v| memo+="#{v.location_narrative or ""} " }
  end

end
