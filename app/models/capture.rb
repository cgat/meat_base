# == Schema Information
#
# Table name: captures
#
#  id                 :integer          not null, primary key
#  capture_owner_id        :integer
#  capture_owner_type     :string(255)
#  fn_photo_reference :string(255)
#  f_stop             :float
#  shutter_speed      :string(255)
#  iso                :integer
#  focal_length       :integer
#  camera_id          :integer
#  lens_id            :integer
#  capture_datetime   :datetime
#  lat                :float
#  long               :float
#  elevation          :float
#  azimuth            :integer
#  comments           :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Capture < ActiveRecord::Base
  include NumberHelper
  include BaseCapture
  include Coordinateable
  include PgSearch

  multisearchable :against => [:fn_photo_reference]

  attr_accessible :azimuth, :camera_id, :capture_datetime, :comments, :f_stop, :fn_photo_reference, :focal_length, :iso, :lens_id, :capture_owner_id, :capture_owner_type, :shutter_speed,  :capture_images_attributes, :alternate
  attr_accessible :historic_captures_attributes, :historic_capture_ids
  attr_accessible :camera_make, :camera_model
  attr_accessor :camera_make, :camera_model

  before_save :set_camera_id

  belongs_to :camera
  belongs_to :lens
  belongs_to :capture_owner, polymorphic: true
  alias :parent :capture_owner
  has_many :comparison_indices, dependent: :destroy
  has_many :historic_captures, through: :comparison_indices
  alias :comparisons :historic_captures

  accepts_nested_attributes_for :historic_captures

  validates_uniqueness_of :fn_photo_reference, scope: [:capture_owner_id, :capture_owner_type], allow_blank: true
  validates :azimuth, numericality: {greater_than_or_equal_to: 0, less_than: 360}, allow_blank: true
  validates :iso, numericality: true,  allow_blank: true #inclusion: { in: ISOS }. Exclude this for now, as it may be difficult to predict what will be returned from the file metadata
  validates :f_stop, numericality: true,  allow_blank: true
  #validates :shutter_speed, inclusion: { in: SHUTTER_SPEEDS}, allow_blank: true
  validates :focal_length, numericality: {greater_than: 0 }, allow_blank: true

  #Scopes
  scope :has_comparisons, lambda { joins(:historic_captures).uniq }
  scope :has_comparisons_with_images, lambda { joins(historic_captures: :capture_images).uniq }
  scope :missing_comparisons, lambda {
    where.not({id: Capture.has_comparisons})
  }
  scope :without_capture_images, lambda {
    where.not({id: Capture.with_capture_images})
  }
  scope :without_azimuth, lambda { where(azimuth: nil) }

  #mastered scope will return all captures that have a capture image which closely matches
  #the dimensions of a capture image that belows to the historic capture comparison.
  #We don't match on dimensions being exactly the same since there is a subset of mastered
  #images that are off by a few pixels (an error in the mastering script at the time)
  scope :mastered, lambda {
    Capture.joins(historic_captures: :capture_images).joins(:capture_images).where( \
      %Q{
        (ABS(capture_images.x_dim-capture_images_captures.x_dim)/GREATEST(capture_images.x_dim,capture_images_captures.x_dim)::float*50
          +ABS(capture_images.y_dim-capture_images_captures.y_dim)/GREATEST(capture_images.y_dim, capture_images_captures.y_dim)::float*50)<?
        }, \
      CaptureImage::SIMILARITY_THRES).uniq
  }

  scope :not_mastered,  lambda {
    Capture.where.not(id: Capture.mastered ).where(id: Capture.has_comparisons)
  }

  def add_comparison(historic_capture)
    unless self.historic_captures.include?(historic_capture)
      comparison_indices.create(historic_capture_id: historic_capture.id)
    else
      historic_capture
    end
  end

  def remove_comparison(historic_capture)
    comparison_indices.find_by_historic_capture_id(historic_capture.id).destroy
  end


  def metadata_set?
    !focal_length.nil?
  end

  def set_camera_id
    if self.camera_make || self.camera_model
      camera = Camera.find_by_model(self.camera_model)
      #if camera doesn't exist, create it
      if camera.nil?
        camera = Camera.create(make: self.camera_make, model: self.camera_model)
      end
      self.camera_id = camera.id
    end
  end

  def location
    self.capture_owner if self.capture_owner_type =="Location"
  end

  def visit
    case self.capture_owner_type
    when "Location"
      self.capture_owner.visit
    when "Visit"
      self.capture_owner
    else
      nil
    end
  end

  def date
    visit.present? ? visit.date : nil
  end


end
