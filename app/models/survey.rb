# == Schema Information
#
# Table name: surveys
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  historical_map_sheet :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Survey < ActiveRecord::Base
  include NumberHelper
  include HasUnmanagedMetadata
  include PgSearch

  multisearchable :against => [:name]

  attr_accessible :historical_map_sheet, :name, :legacy_path, :surveyor_id

  belongs_to :surveyor, inverse_of: :surveys
  alias :parent :surveyor
  has_many :survey_seasons, inverse_of: :survey, dependent: :destroy
  has_many :stations, through: :survey_seasons
  has_many :visits, through: :stations
  has_many :unsorted_captures, as: :capture_owner, dependent: :destroy, class_name: "Capture"
  has_many :unsorted_hcaptures, as: :capture_owner, dependent: :destroy, class_name: "HistoricCapture"
  has_many :scenics, as: :image_owner, dependent: :destroy, class_name: 'ScenicImage'

  validates_uniqueness_of :legacy_path, allow_blank: true
  validates :name, presence: true, length: {maximum: 255}
  validates :historical_map_sheet, length: {maximum: 255}
  validates_uniqueness_of :name, scope: [:surveyor_id]
  validates :surveyor, presence: true

  scope :where_parent, lambda { |obj|
    where({surveyor_id: obj.id})
  }

  scope :published, lambda { where(published: true) }

  def display_name
      self.name
  end

end
