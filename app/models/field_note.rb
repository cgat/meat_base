class FieldNote < ActiveRecord::Base
  include NumberHelper

  attr_accessible :field_note_file, :visit_id, :legacy_path

  after_destroy :clean_up

  belongs_to :visit, inverse_of: :field_notes
  alias :parent :visit
  validates :field_note_file, presence: true, if: 'self.new_record?'
  validates :legacy_path, length: { maximum: 255 }
  validates :visit, presence: true

  mount_uploader :field_note_file, FileUploader


  def file_present?
    File.exists?(self.field_note_file.file.path)
  end

  def display_name
    self.field_note_file.file.filename
  end

end
