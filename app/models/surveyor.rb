# == Schema Information
#
# Table name: surveyors
#
#  id          :integer          not null, primary key
#  last_name   :string(255)
#  given_names :string(255)
#  short_name  :string(255)
#  affiliation :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Surveyor < ActiveRecord::Base
  include PgSearch

  multisearchable :against => [:last_name, :given_names]

  attr_accessible :affiliation, :given_names, :last_name, :short_name

  has_many :surveys, inverse_of: :surveyor, dependent: :destroy
  has_many :survey_seasons, through: :surveys
  has_many :stations, through: :survey_seasons

  validates_uniqueness_of :last_name, scope: :given_names, allow_blank: true
  validates_uniqueness_of :given_names, scope: :last_name, allow_blank: true
  validates_uniqueness_of :short_name, allow_blank: true
  validates_uniqueness_of :affiliation, scope: [:last_name, :given_names], allow_blank: true
  validates_length_of :last_name, :given_names, :affiliation, maximum: 255
  validates :last_name, presence: true, if: 'self.affiliation.blank?'
  validates :affiliation, presence: true, if: 'self.last_name.blank?'
  validates :short_name, length: {maximum: 4}

  scope :published, lambda { where(published: true) }

  def display_name
    self.full_name
  end

  def full_name
    if self.given_names.present? && self.last_name.present?
      return self.given_names+" "+self.last_name
    elsif self.last_name.present?
      return self.last_name
    elsif self.affiliation.present?
      return self.affiliation
    else
      nil
    end
  end

  def parent
    nil
  end


end
