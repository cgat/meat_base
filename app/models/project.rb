
class Project < ActiveRecord::Base
  include HasUnmanagedMetadata
  include PgSearch

  multisearchable :against => [:name, :description]

  attr_accessible :name, :description, :legacy_path, container_folder: "Organized_by_Project"

  has_many :stations, as: :station_owner, dependent: :destroy
  has_many :unsorted_captures, as: :capture_owner, dependent: :destroy, class_name: "Capture"
  has_many :unsorted_hcaptures, as: :capture_owner, dependent: :destroy, class_name: "HistoricCapture"
  has_many :scenics, as: :image_owner, class_name: 'ScenicImage', dependent: :destroy

  validates :name, presence: true, length: { maximum: 255 }
  validates :legacy_path, length: { maximum: 255 }

  scope :published, lambda { where(published: true) }

  def display_name
    self.name
  end

  def parent
    nil
  end

end
