# == Schema Information
#
# Table name: keywords
#
#  id         :integer          not null, primary key
#  keyword    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Keyword < ActiveRecord::Base
  attr_accessible :keyword


  has_many :keyword_visit_associations
  has_many :visits, through: :keyword_visit_associations

  validates_uniqueness_of :keyword
  validates :keyword, presence: true, length: { maximum: 255}

  def display_name
    keyword
  end


end
