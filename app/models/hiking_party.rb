# == Schema Information
#
# Table name: hiking_parties
#
#  id             :integer          not null, primary key
#  participant_id :integer
#  visit_id       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class HikingParty < ActiveRecord::Base
  attr_accessible :participant_id, :visit_id

  after_save 'visit.update_pg_search_document'

  belongs_to :visit
  belongs_to :participant
end
