# == Schema Information
#
# Table name: survey_seasons
#
#  id                  :integer          not null, primary key
#  survey_id           :integer
#  year                :date
#  geographic_coverage :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class SurveySeason < ActiveRecord::Base
  include NumberHelper
  include HasUnmanagedMetadata
  include PgSearch

  multisearchable :against => [:geographic_coverage, :year, :jurisdiction,
    :affiliation, :archive, :collection, :location, :sources, :notes,
    :searchable_maps, :searchable_glass_plate_listings]

  attr_accessible :geographic_coverage, :survey_id, :year, :legacy_path,
    :record_id, :jurisdiction, :affiliation, :archive, :collection, :location, :sources, :notes
  #By having these availalbes as accessor variables, I can make more conherent forms (because I allow user to change the parents and grandparents)
  attr_accessible :surveyor_id
  attr_accessor :surveyor_id

  belongs_to :survey, inverse_of: :survey_seasons
  alias :parent :survey
  has_many :stations, as: :station_owner, dependent: :destroy
  has_many :unsorted_captures, as: :capture_owner, dependent: :destroy, class_name: "Capture"
  has_many :unsorted_hcaptures, as: :capture_owner, dependent: :destroy, class_name: "HistoricCapture"
  has_many :scenics, as: :image_owner, class_name: 'ScenicImage', dependent: :destroy
  has_many :glass_plate_listings, dependent: :destroy
  has_many :maps, dependent: :destroy
  validates :year, presence: true, numericality: {only_integer: true}, inclusion: {in: 1000...9999, message: 'Year must be in integer format XXXX'}
  validates :geographic_coverage, length: { maximum: 255}
  validates :survey, presence: true
  validates_uniqueness_of :year, scope: [:survey_id]

  scope :where_parent, lambda { |obj|
    where({survey_id: obj.id})
  }

  scope :published, lambda { where(published: true) }

  def display_name
    self.year.to_s
  end

  def station?(station)
    stations.find_by_id(station.id)
  end

  def surveyor
    if survey
      survey.surveyor
    else
      nil
    end
  end

private

  def searchable_maps
    #we need to reload the keywords in case multiple keywords have been added
    maps.reload.inject(""){ |memo,v| memo+="#{v.nts_map or ""} #{v.historic_map or ""}" }
  end

  def searchable_glass_plate_listings
    #we need to reload the keywords in case multiple keywords have been added
    glass_plate_listings.reload.inject(""){ |memo,v| memo+="#{v.container or ""} #{v.plates or ""} #{v.notes or ""}" }
  end
end
