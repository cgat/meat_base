class StationsController < ApplicationController

  def show
    @station = Station.includes(:unsorted_captures).find(params[:id])
    @visits = @station.visits.order('date DESC').includes(:locations)
    @historic_captures = @station.historic_captures.includes(:capture_images)
    @metadata_files = @station.metadata_files
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

end
