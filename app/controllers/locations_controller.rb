class LocationsController < ApplicationController

  def show
    @location = Location.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

end
