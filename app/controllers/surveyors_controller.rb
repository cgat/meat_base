class SurveyorsController < ApplicationController

  def show
    @surveyor = Surveyor.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

end
