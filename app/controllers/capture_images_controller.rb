class CaptureImagesController < ApplicationController
  #see comments in the set_current_nodes method of the application controller
  before_filter :set_current_tree_node

  def show
    @capture_image = CaptureImage.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def set_current_tree_node
    if params.has_key?(:id) && (capture_image=CaptureImage.find_by_id(params[:id]))
      parent = capture_image.captureable
      session[:current_tree_node]=parent.class.name+'_'+parent.id.to_s
      session[:current_tree_node_type] = parent.class.name
      session[:current_tree_node_id] = parent.id.to_s
    end
  end

end
