class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery
  #by prepending this before filter, we give other controllers the chance
  #to redefine the current_node and current_tree_node session variables
  prepend_before_filter :set_current_nodes
  #Pjax requests change the state of the data pane, not nav pane, and therefore
  #we do not want to run a costly navigation_setup when pjax request is made.
  before_filter :navigation_setup, :except => [:map, :search, :tree, :filters], :unless => proc { is_pjax? }

  #redirect to the current node if one exists.
  #If it doesn't exists and stations exist, redirect to the first station
  #otherwise go to the root_url
  def redirect_to_current_node
    if current_node.present?
      redirect_to current_node
    else
      station = Station.first
      redirect_to station.present? ? Rails.application.routes.url_helpers.station_path(station) : root_url
    end
  end

  def search
    session[:nav_state] = "search"
    session[:query] = params[:query]
    search_setup
    redirect_to_current_node
  end

  def map
    session[:nav_state] = "map"
    map_setup
    redirect_to_current_node
  end

  def default_nav_state
    "map"
  end

  def navigation_setup
    session[:nav_state] ||= default_nav_state

    if session[:nav_state]=="search"
      search_setup
    elsif session[:nav_state]=='map'
      map_setup
    end
  end

  def search_setup
    @search = session[:query].present? ? PgSearch.multisearch(session[:query]) : []
  end

  def map_setup
    gon.blue_map_marker_path = ActionController::Base.helpers.asset_path("meat_base/blue_map_marker.png")
    gon.red_map_marker_path = ActionController::Base.helpers.asset_path("meat_base/red_map_marker.png")
    gon.gray_map_marker_path = ActionController::Base.helpers.asset_path("meat_base/gray_map_marker.png")
    gon.map = mark_current_station(gmaps_markers)
  end

  def mark_current_station(gmaps_marker_array)
    return gmaps_marker_array if gmaps_marker_array.blank? || current_station.blank?
    gmaps_marker_array = Array(gmaps_marker_array)
    index = gmaps_marker_array.index{|m| m[:id]==current_station.id }
    if index.present?
      gmaps_marker_array[index][:picture][:url] = gon.blue_map_marker_path
      gmaps_marker_array[index][:current] = true
    end
    gmaps_marker_array
  end

  def gmaps_markers
    Gmaps4rails.build_markers(stations_to_map) do |station, marker|
        marker_picture, json = nil, nil
        historic_year, survey, surveyor, project = nil, nil, nil, nil
        surveyor_full_name = ''
        if(station.station_owner_type==='Project')
          survey = station.station_owner.name
          surveyor = 'mlp';
          surveyor_full_name = 'Mountain Legacy Project'
        else
          historic_year = station.station_owner.year
          survey = station.station_owner.survey.name
          surveyor = station.station_owner.survey.surveyor.last_name ||
            station.station_owner.survey.surveyor.affiliation
          surveyor_full_name = station.station_owner.survey.surveyor.full_name ||
            station.station_owner.survey.surveyor.affiliation
        end
        json = {:id => station.id,
                    link: station_url(station),
                    shadow: {url: ''},
                    current: false,
                    historic_year: historic_year,
                    survey: survey,
                    surveyor: surveyor,
                    surveyor_full_name: surveyor_full_name
                  }
        marker.lat station.latitude
        marker.lng station.longitude
        if(station.unsorted_captures.empty? && station.located_captures.empty? &&
            station.unsorted_in_visit_captures.empty?)
          marker.picture({
                    :url => gon.gray_map_marker_path,
                    :width   => 21,
                    :height  => 34,
                    :anchor => [10,34]
                   })
          json[:repeated] = false;
        else
          marker.picture({
                    :url => gon.red_map_marker_path,
                    :width   => 21,
                    :height  => 34,
                    :anchor => [10,34]
                   })
          json[:repeated] = true;
        end
        marker.json(json)
      end
  end

  #Only map stations which have a latitude or longitude. Note that these methods will call descendent objects
  #to find a coordinate if the station doesn't have one (the station properties are lat and long)
  def stations_to_map
    survey_based = Station.includes(:locations,:unsorted_captures,:located_captures, :unsorted_in_visit_captures,station_owner: [survey: [:surveyor]]).where('station_owner_type = ?', 'SurveySeason').reject do |station|
      station.latitude.blank? || station.longitude.blank?
    end

    project_based = Station.includes(:locations,:unsorted_captures,:located_captures, :unsorted_in_visit_captures, :station_owner).where('station_owner_type = ?', 'Project').reject do |station|
      station.latitude.blank? || station.longitude.blank?
    end
    survey_based.to_a+project_based.to_a
  end

  def current_station
    if current_node.class==Station
      return current_node
    else
       return current_node.respond_to?(:station) ? current_node.station : nil
     end
  end

  #Any object that has a show page and any time of route in the form of /<object>/:id should go in the white list
  CURRENT_NODE_TYPE_WHITE_LIST = ["Surveyor", "Project", "Survey", "SurveySeason", "Station", "Visit", "Location", "HistoricVisit", "Capture", "HistoricCapture", "CaptureImage", "LocationImage"]
  #we keep track of the current_node (node in this case is just an object in our system) and
  #the current_tree_node. They will usually be the same, but since all objects aren't represented
  #in the navigation tree, the tree node selected may not match the currently viewed node (object).
  #The simplest example is a capture image, which is not represented in the tree. When viewing this object,
  #its capture will be selected. To change the current_tree_node you must add a before_filter
  #in the controller of the requesting object that sets the session to the correct current_tree_node.
  def set_current_nodes
    #if we are destroying a node, we don't want to set the tree node to it because it is getting deleted.
    if params[:action]=='destroy'
      session[:current_node]=""
      session[:current_node_type] = ""
      session[:current_node_id] = ""
    elsif params.has_key?(:id) && Object.const_defined?(params[:controller].singularize.camelize) && CURRENT_NODE_TYPE_WHITE_LIST.include?(params[:controller].singularize.camelize)
      session[:current_node]=params[:controller].singularize.camelize+'_'+params[:id].to_s
      session[:current_node_type] = params[:controller].singularize.camelize
      session[:current_node_id] = params[:id].to_s
      #set current_tree_node, override in specific controller by adding before_filter
      session[:current_tree_node] = session[:current_node]
      session[:current_tree_node_type] = session[:current_node_type]
      session[:current_tree_node_id] = session[:current_node_id]
    end
  end

  def current_node
    if session[:current_node_type].present? && session[:current_node_id].present?
      @current_node_ivar ||= session[:current_node_type].constantize.find(session[:current_node_id].to_i)
    end
  end

  def current_tree_node
    if session[:current_node]==session[:current_tree_node]
      @current_tree_node_ivar ||= current_node
    elsif session[:current_tree_node_type].present? && session[:current_tree_node_id].present?
      @current_tree_node_ivar ||= session[:current_tree_node_type].constantize.find(session[:current_tree_node_id].to_i)
    end
  end

end
