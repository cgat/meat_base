class CapturesController < ApplicationController

  def show
    @capture = Capture.find(params[:id])
    parent = @capture.parent
    if parent.class==Location
      captures_sequence = parent.visit.captures
    elsif parent.class==Visit
      captures_sequence = parent.captures
    else
      captures_sequence = parent.unsorted_captures
    end
    captures_sequence = view_context.sort_captures(captures_sequence)
    cap_index = captures_sequence.find_index{|cap| cap.id==@capture.id }
    @next =  cap_index==(captures_sequence.length-1) ? nil : captures_sequence[cap_index+1]
    @prev =  cap_index==0 ? nil : captures_sequence[cap_index-1]

    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def comparisons
    capture = Capture.find(params[:id])
    if capture.has_viewable_comparisons?
      respond_to do |format|
        format.html { render template: 'shared/comparisons', locals: {base_comparison: capture, comparisons: capture.comparisons.with_capture_images } }
        format.js { render template: 'shared/comparisons' }
      end
    else
      @capture = capture
      render 'show'
    end
  end

end
