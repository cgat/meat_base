class SurveySeasonsController < ApplicationController

  def show
    @survey_season = SurveySeason.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

end
