class HistoricCapturesController < ApplicationController

  def show
    @historic_capture = HistoricCapture.find(params[:id])
    parent=@historic_capture.parent
    if parent.class==HistoricVisit
      historic_captures_sequence = parent.historic_captures
    else
      historic_captures_sequence = parent.unsorted_hcaptures
    end
    historic_captures_sequence = view_context.sort_historic_captures(historic_captures_sequence)
    hcap_index = historic_captures_sequence.find_index{|hcap| hcap.id==@historic_capture.id }
    @next =  hcap_index==(historic_captures_sequence.length-1) ? nil : historic_captures_sequence[hcap_index+1]
    @prev =  hcap_index==0 ? nil : historic_captures_sequence[hcap_index-1]

    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

  def comparisons
    @historic_capture = HistoricCapture.find(params[:id])
    if @historic_capture.has_viewable_comparisons?
      respond_to do |format|
        format.html { render template: 'shared/comparisons', locals: {base_comparison: @historic_capture, comparisons: @historic_capture.comparisons.with_capture_images } } #
        format.js { render template: 'shared/comparisons' }
      end
    else
      render 'show'
    end
  end


end
