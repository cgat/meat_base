class SurveysController < ApplicationController

  def show
    @survey = Survey.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

end
