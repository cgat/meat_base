class LocationImagesController < ApplicationController

  #see comments in the set_current_nodes method of the application controller
  before_filter :set_current_tree_node

  def show
    @location_image = LocationImage.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show' }
    end
  end

  def set_current_tree_node
    if params.has_key?(:id) && (location_image=LocationImage.find_by_id(params[:id]))
      parent = location_image.image_owner
      session[:current_tree_node]=parent.class.name+'_'+parent.id.to_s
      session[:current_tree_node_type] = parent.class.name
      session[:current_tree_node_id] = parent.id.to_s
    end
  end

end
