class HistoricVisitsController < ApplicationController

  def show
    @historic_visit = HistoricVisit.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show'}
    end
  end

end
