class ProjectsController < ApplicationController

  def show
    @project = Project.find(params[:id])
    respond_to do |format|
      format.html { render 'show' }
      format.js { render 'show' }
    end
  end

end
