module ApplicationHelper
  #override this is application to expose admin specific fields and features
  #exposed in the meat_base engine pages
  #see slide 36 http://www.slideshare.net/AndyMaleh/revised-rails-engine-patterns-for-montrealrb-meetup-oct-16-2012
  #expose helper pattern
  def admin_app?
    false
  end

  def get_title_page(current_node=nil)
    controller = params[:controller]
    action = params[:action]
    model_title_name = current_node.present? ? "#{current_node.class.name.titleize} | #{current_node.display_name}" : controller.classify.titleize
    if (action=='edit')
      return "Edit #{model_title_name}"
    elsif(action=='new')
      return "Add New #{model_title_name}"
    elsif controller=="static_pages"
      return action.titleize
    else
      return model_title_name
    end
  end

  #Creates a link that will prompt the user to save as on their local machine
  #This works by defining the response content disposition in the s3 query string
  #to 'attachment'. The trick here, however, is that if fog_public is set to true
  #(which it is in our case), carrierwave fog won't append the query string. To
  #get around this I temporarily change fog_public to true.
  def link_to_s3_download(uploader, filename, link_string, options={})
    if uploader.class.superclass==CarrierWave::Uploader::Base
      uploader.fog_public = false
      url = uploader.url(:query => {"response-content-disposition" => "attachment; filename='#{filename}'"})
      uploader.fog_public = true
      link_to(link_string.html_safe, url, options).html_safe
    end
  end

  def sort_captures(captures)
    captures.sort_by{|cap| cap.display_name }
  end

  def sort_historic_captures(hcaptures)
    hcaptures.sort_by{|hcap| hcap.display_name }
  end

  #While the not_mastered? methods below seem like they would be a better
  #fit as a model method, this may be called several times on each request
  #and the not_mastered query is a fairly heavey one (whether it's called on the
  # entire set or just for a specfic capture). This ensures that the query will be
  #made once for each request. Don't call this method on the public app
  def cap_not_mastered?(capture)
    return false if capture.blank? || capture.class!=Capture
    @not_mastered_captures ||= Capture.not_mastered
    @not_mastered_captures.any?{|cap| cap.id==capture.id}
  end

  def hcap_not_mastered?(hcapture)
    return false if hcapture.blank? || hcapture.class!=HistoricCapture
    @not_mastered_hcaptures ||= HistoricCapture.not_mastered
    @not_mastered_hcaptures.any?{|hcap| hcap.id==hcapture.id}
  end

  def breadcrumb_div(obj, options={})
    options.reverse_merge!({ max_relatives: -1,separator: ' > ',with_self: false, truncate: 0, suffix: false})
    ancestors = get_relatives(obj,options[:max_relatives])
    if !options[:with_self]
      ancestors.delete_at(0)
    end
    out_string = "<ul class='breadcrumb'>"
    ancestors.reverse_each do |a|
      li_id = "#{a.class.name.downcase}_breadcrumb"
      if a==ancestors.first
        if options[:suffix]
          out_string+="<li id='#{li_id}'>"
          out_string+=object_type_icon(a)
          out_string+=link_to(a.display_name, url_for(a))
          out_string+="<span class='divider'>"+options[:separator]+"</span>"
          out_string+="</li>"
          out_string+="<li class='active'>"
          out_string+=options[:suffix]
          out_string+="<li>"
          break
        else
          out_string+="<li class='active'>"
          out_string+=object_type_icon(a)
          out_string+=a.display_name
          out_string+="</li>"
          break
        end
      end
      out_string+="<li id='#{li_id}'>"
      out_string+= object_type_icon(a)
      if options[:truncate] != 0
        out_string+=link_to(truncate(a.display_name,length: options[:truncate]), url_for(a))
      else
        out_string+=link_to(a.display_name, url_for(a))
      end
      out_string+="<span class='divider'>"+options[:separator]+"</span>"
      out_string+="</li>"
    end
    out_string.chomp!(options[:separator])
    out_string += "</ul>"
    return out_string.html_safe
  end

  def object_type_icon(obj)
    case obj
    when Surveyor
      "<span class='icomoon-surveyor_icon breadcrumb_icon' title='Surveyor'></span>"
    when Survey
      "<span class='icomoon-survey breadcrumb_icon' title='Survey'></span>"
    when SurveySeason
      "<span class='icomoon-calendar-empty breadcrumb_icon' title='Year'></span>"
    when Station
      "<span class='icomoon-station_icon breadcrumb_icon' title='Station'></span>"
    when Visit
      "<span class='icomoon-calendar breadcrumb_icon' title='Visit'></span>"
    when Location
      "<span class='icomoon-location breadcrumb_icon' title='Station Sub-location'></span>"
    when Capture, HistoricCapture
      "<span class='icomoon-camera breadcrumb_icon' title='#{obj.class.name.titleize}'></span>"
    when CaptureImage
      "<span class='icomoon-image breadcrumb_icon' title='Capture Image'></span>"
    else
      ""
    end
  end

  def get_relatives(obj,max_relatives=-1)
    return [] if obj.blank? || !obj.respond_to?(:parent)
    relatives = [obj]
    count=0
    while parent = relatives.last.parent
      relatives << parent
      count += 1
      if count == max_relatives
        break
      end
    end
    relatives.compact!
    return relatives
  end

  def max_relatives_to_show(parent_type, controller_type)
    if controller_type=='visits'
      return 1
    elsif controller_type=='stations'
      case parent_type
      when 'Station','Visit'
        return 1
      when 'Location'
        return 2
      end
    end
    return 3 #defaults to 3
  end

  def link_to_ancestors(obj, options={})
    options.reverse_merge!({ max_relatives: -1,separator: ' > ',with_self: false, truncate: 0})
    ancestors = get_relatives(obj,options[:max_relatives])
    if !options[:with_self]
      ancestors.delete_at(0)
    end
    out_string = "<div>"
    ancestors.reverse_each do |a|
      if options[:truncate] != 0
        out_string+=link_to(truncate(a.display_name,length: options[:truncate]), url_for(a))
      else
        out_string+=link_to(a.display_name, url_for(a))
      end
      out_string+=options[:separator]
    end
    out_string.chomp!(options[:separator])
    out_string += "</div>"
    return out_string.html_safe
  end
  #show_value is a helper used to help display value of model attributes.
  #If the value is blank, it will return "N/A"
  #If the second parameter is present, that value will be appended to the value string
  #If the option param "prepend" is set to true, the value will be prepended to the value string
  def show_value(value,append="",options={})
    if value.blank? && value.class!=FalseClass
      return "N/A"
    else
      if value.class==DateTime || value.class==Date || value.class==ActiveSupport::TimeWithZone
        value=value.to_s(:long)
      elsif value.class==Time
        value=value.to_s(:time)
      end
      if options.present? && options[:prepend]==true
        return "#{append}#{value.to_s}".strip
      else
        return "#{value.to_s}#{append}".strip
      end
    end
  end

  #dd_tag helps create dd tags (defintion descriptin) for a corresponding dt tag (definition term)
  #One dt can have many dd's. The benefit of using this method to create the dd_tag is that
  #you can pass in a collection of objects coupled with a block. The block will get called for
  #each object in the collection. If comma_separated is false, each block output will be wrapped
  #in a dd tag. If comma_separated is true, the list will be concatenated withe commas and wrapped
  #in a dd tag. If the collection is empty, the value in the dd tag whatever show_value decides
  def dd_tag(collection=nil, options={}, &blk)
    options.reverse_merge!({comma_separated: false})
    return "<dd>#{show_value(nil)}</dd>".html_safe unless collection.present?
    collection = [collection] unless collection.respond_to?(:each)
    if options[:comma_separated]
      dd_comma_list(collection, &blk)
    else
      dd_list(collection, &blk)
    end
  end

  def dd_list(collection, &blk)
    collection.inject("") do |output, item|
      output += content_tag :dd do
        block_given? ? blk.call(item) : item.to_s
      end
    end.html_safe
  end

  def dd_comma_list(collection, &blk)
    content_tag :dd do
      collection.inject("") do |output, item|
        output += "#{block_given? ? blk.call(item) : item.to_s}, "
      end.chomp(", ")
    end.html_safe
  end

  def is_pjax?
    request.xhr? || request.headers['X-PJAX'] ? true : false
  end

  #### tabs ####
  #tabs if a nifty helper to create tabs for a page.
  #
  #What is a tab?
  #tabs are typically created by first using an unordered list to list out the tab headers
  #followed by a series of div blocks containing the tab content. The appearance of the tab
  #header and content different depending on if it is active or not.
  #
  #The Problem
  #The tab header and the tab content are single unit, yet their existence is spread out
  #and intermixed with other markup. Because of this, it is difficult to insert and take out
  #tabs without editing the template for which the tab markup is written.
  #
  #The Solution
  #tabs creates tabs based on partial files in a specified tab folder. The partial files
  #contain content_for blocks which will later be yielded into the proper tab structure,
  #allowing us to keep on tab logic in one place.
  #
  #Examples:
  #Consider the views for the visit model are located at app/views/visits
  #We would like to show two captures: captures and comparisons
  #1. Create the folder app/views/visits/tabs
  #2. Add the files _1_captures.html.erb, _2_comparisons.html.erb
  #The numbers determine order.
  #3. In each file, follow the format
  # <%
  # content_for "#{tab_name}_title" do
  #   "<tab title used in header>"
  # end
  # content_for "#{tab_name}_id" do
  #   "<some_identitfier>_tab_#{location.id}"
  # end
  # content_for "#{tab_name}_pane" do
  #   <p>some content to render</p>
  # end
  # %>
  #4. Then in your show.html.erb view add the line
  #<%= tabs('tabs') %>
  #5. You should be good to go
  #
  #Other options
  #Any options passed in the options hash will be passed ot the tab partial,
  #just like you would if you were rendering a partial via 'render'
  #You can also specify a homogeneous collection. In this case
  #a tab partial will be used that matches the class name (underscored version)
  #of the objects in the collection. For example, if you had a collection of Visit
  #objects, the tab partial '_1_visit.html.erb' would be required. This tab partial
  #would receive a local variable 'visit' which is associated with each object in
  #the collection (each time the partial is called)
  #
  #Bootstrap
  #tabs currently only supports bootstrap tabs styling
  def tabs(tabs_folder, options={})
    active_tab = options.fetch(:active_tab){ "" }
    tabs_identifier = options.fetch(:tabs_identifier){ rand(100000) }
    collection = options.fetch(:collection) { false }
    tag_classes = options.fetch(:tag_classes) { {nav_tabs: "", tab_content: "" } }
    tabs_folder = File.join(controller.class.controller_path,tabs_folder) unless tabs_folder=~/(\/|\\)/
    tab_files = self.view_paths.map{|view_path| Dir.glob(File.join(view_path.to_s,tabs_folder,"*"))}.flatten
    raise ArgumentError, "The tab_folder specified (#{tab_folder}) does not contain any files." if tab_files.blank?

    #get rid of files that start with dot
    tab_files.reject!{|tf| tf=~/.*\/\.[^\/]*$/ }

    tab_files.sort!{|a,b| Pathname.new(a).basename.to_s <=> Pathname.new(b).basename.to_s}

    tab_names = tab_files.map{|tf| Pathname.new(tf).basename.to_s.gsub(/(^_|\d+_|\.html\.\w+)/,"")}
    tab_tuples = tab_files.zip(tab_names)
    tab_tuples = tab_collectify(tab_tuples, collection) if collection
    tab_tuples.each{|tf,tn,col_opt| render(file: abs_path_to_partial_path(tf), handlers: [:erb], formats: [:html], locals: options.merge({tab_name: tn}).merge(col_opt || {}) ) }

    output = ""
    tab_counter = 0
    output += content_tag :ul, class: "nav nav-tabs #{tag_classes[:nav_tabs]}", id: "tabs_nav_#{tabs_identifier}" do
      tab_tuples.map do |_, tab_name, _|
        if content_for?("#{tab_name}_title")
          tab_counter += 1
          concat(content_tag(:li, class: tab_name==active_tab || (active_tab.blank? && tab_counter==1) ? "active" : nil) do
            link_to( '#'+content_for("#{tab_name}_id"), data: { toggle: "tab" } ) do
              content_for("#{tab_name}_icon").to_s + \
              content_for("#{tab_name}_title").to_s
            end
          end )
          #deletes the content_for so that it can be redefined for another tab of the same name
          #useful for nested tabs
          @view_flow.content.delete("#{tab_name}_title")
          @view_flow.content.delete("#{tab_name}_icon")
        end
      end
    end
    #If we iterate through the tabs and there no content, then we can exit now with an empty string
    if tab_counter==0
      return ""
    end
    tab_counter = 0
    output += content_tag :div, class: "tab-content #{tag_classes[:tab_content]}", id: "tabs_content_#{tabs_identifier}" do
      tab_tuples.map do |_, tab_name, _|
        if content_for?("#{tab_name}_pane")
          tab_counter += 1
          concat(content_tag(:div, class: "tab-pane #{tab_name==active_tab || (active_tab.blank? && tab_counter==1) ? "fade active in" : "fade"}", id: content_for("#{tab_name}_id")) { content_for("#{tab_name}_pane") } )
          #deletes the content_for so that it can be redefined for another tab of the same name
          #useful for nested tabs
          @view_flow.content.delete("#{tab_name}_pane")
          @view_flow.content.delete("#{tab_name}_id")
        end
      end
    end

    return output.html_safe
  end

  private

  def tab_collectify(tab_tuples, collection)
    return tab_tuples unless collection  && collection.present?
    collection_type = collection.first.class.name.underscore
    homogeneous_collection = proc {|col| col.all?{|item| item.class.name.underscore==collection_type }}
    raise ArgumentError, "Collection must be homogeneous in type" unless homogeneous_collection.call(collection)

    missing_tab_partial_exception = proc { raise StandardError, "Excepted to find tab partial with the format '_<num>_#{collection_type}'"}
    matching_tuple = tab_tuples.find(missing_tab_partial_exception){|tab_file, tab_name| tab_name==collection_type}

    collection_tuples = collection.map do |item|
      new_tuple = matching_tuple.dup
      new_tuple << { collection_type.to_sym => item }
      new_tuple[1] = "#{collection_type}_#{item.id}"
      new_tuple
    end

    #example replace_with_array.call([1,3,4], ['a','b'], 1) --> [1,'a','b',4]
    #given an array (the receiver) and an index within the array
    #insert the elements of another array into the receiver
    replace_with_array = proc do |receiver, insertion, index|
      a_start = index>0 ? receiver.slice(0..(index-1)) : []
      a_end = index<receiver.count ? receiver.slice((index+1)..-1) : []
      receiver = a_start+insertion+a_end
    end
    tab_tuples = replace_with_array.call(tab_tuples,collection_tuples,tab_tuples.index(matching_tuple))
  end

  def abs_path_to_partial_path(abs_path)
    abs_path.sub(/.*\/app\/views\//, "").sub(/.html.*/,"")
  end

end
