module ComparisonHelper
  def comparison_date_formatted(comparison)
    case comparison.class.name
    when "Capture"
      comparison.date.to_s(:long)
    when "HistoricCapture"
      comparison.year
    end
  end
end
