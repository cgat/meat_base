## BaseCapture ##
#This module is used to share common behaviour and associations between
#the Capture and Historic Capture models.

module BaseCapture
  extend ActiveSupport::Concern
  included do
    has_many :capture_images, -> { order(:image_state, :image) }, as: :captureable,  dependent: :destroy
    accepts_nested_attributes_for :capture_images, :allow_destroy => true, :reject_if => lambda { |a| a[:image].blank? && a[:image_cache].blank? && a[:image_state].blank? }
    validates :comments, length: { maximum: 255 }
    validates :fn_photo_reference, length: { maximum: 255}
    scope :remotely_uploaded, lambda { joins{capture_images}.merge(CaptureImage.remotely_uploaded).uniq }
    scope :with_capture_images, lambda { joins(:capture_images).uniq }
    scope :where_parent, lambda { |obj|
      where({capture_owner_id: obj.id, capture_owner_type: obj.class.name})
    }
    scope :published, lambda { where(published: true) }
  end

  def merge(merge_cap)
    raise ArgumentError, "Argument must be the of the same type as the object (#{self.class.name})" if self.class!=merge_cap.class
    #we don't validate because because fn_photo_reference may be
    #copied to this capture, but it validates as unique. In this case
    #we save this capture, then delete the other capture that was
    #merged, so the uniqueness remains.
    merge_result = merge_attributes(merge_cap) and self.save(validate: false) and merge_associations(merge_cap)
    return false unless merge_result
    if merge_cap.reload.capture_images.present?
      raise StandError, "The capture being merged (id: #{merge_cap.id} type: #{merge_cap.class.name}) was about to be deleted, but still has capture images"
    else
      merge_cap.destroy
    end
    return true
  end

  #Merge the attributes from given capture into this capture
  #Will return false if a both the captures have a value set for the
  #same attribute. Otherwise, set the value of this capture
  #if the attribute is not present.
  def merge_attributes(cap)
    mergeable=attributes.reject{|name,_| ['created_at','updated_at','id','capture_owner_type','capture_owner_id','condition'].include?(name)}
    if mergeable.any?{|name,_| self.send(name).present? && cap.send(name).present? }
      return false
    else
      mergeable.each do |name,_|
        cap_value = cap.send(name)
        self.send(name+"=", cap_value) unless cap_value.nil? || cap_value=="" #do note use .blank? beacuase false.blank? ==true
      end
    end
    return true
  end

  def merge_associations(cap)
    capture_images_to_add = cap.capture_images
    capture_images_to_add.each{ |ci| ci.captureable=self }
    if capture_images_to_add.any?{ |ci| !ci.valid? }
      return false
    else
      capture_images_to_add.each{ |ci| ci.do_not_save_metadata = true; ci.save }
    end
    cap.comparisons.each{ |comp| add_comparison(comp) }
    return true
  end

  def display_name
    if self.fn_photo_reference.present?
      self.fn_photo_reference
    elsif capture_images.present?
      #this will find the first capture image that is in the image state MASTER or return the
      #first capture image if there are no MASTERs
      display_name = capture_images.find(proc{capture_images.order("file_size ASC").first}){|ci|ci.image_state=="MASTER"}.display_name
      display_name = display_name.sub(/\.[^\/]*$/,"") #remove extensions
    else
      self.class.name.titleize
    end
  end

  #used to determine if a capture/historic capture is related to a historic capture/capture that has images to view
  #(as opposed to having a historic capture / captures with no images)
  def has_viewable_comparisons?
    self.capture_images.count!=0 && self.comparisons.with_capture_images.count!=0
  end

  def has_aligned_comparisons?
    self.comparisons.any?{|comp| self.aligned?(comp)}
  end

  #captures/historic_captures are conisdered aligned if they contain at least one pair of capture_images that are aligned
  def aligned?(cap)
    self.capture_images.any?{|ci| cap.capture_images.any?{|cap_ci| ci.aligned?(cap_ci)}}
  end

  def preview_capture_image
    @preview_capture_image if @preview_capture_image.present?
    #we don't want to use an image that is processing for the preview
    cap_img_array = capture_images.not_processing
    #but if the only capture image of the capture is processing
    #use that, which will will display a fallback image
    cap_img_array = capture_images if cap_img_array.blank?
    return nil if cap_img_array.empty?
    sort_order = ["MASTER","INTERIM","MISC","RAW","GRIDDED"]
    sorted = cap_img_array.sort_by{|a| [sort_order.index(a.image_state), a.x_dim || 0]}
    @preview_capture_image = sorted.first
  end

  def station
    case self.capture_owner_type
    when 'HistoricVisit'
      self.capture_owner.station
    when 'Location'
      self.capture_owner.visit.station
    when 'Visit'
      self.capture_owner.station
    when 'Station'
      self.capture_owner
    else
      nil
    end
  end

   #niffty little struct that gets passed out of preview_image
   #when a capture has no capture images.
   NoImage = Struct.new(:version) do
    def url
      case version
      when :thumb
        "meat_base/no_images_thumb.gif"
      else
        "meat_base/no_images_medium.gif"
      end
    end
    def thumb
      self.version = :thumb
      self
    end
    def medium
      self.version = :medium
      self
    end
  end

  def preview_image
    @preview_image ||= preview_capture_image.present? ? preview_capture_image.image : NoImage.new
  end

end
