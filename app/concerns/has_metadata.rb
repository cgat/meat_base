require 'active_support/concern'
module HasMetadata
  extend ActiveSupport::Concern
  included do
    attr_accessible :metadata_files, :metadata_files_attributes
    has_many :metadata_files, -> { order :metadata_file },  as: :metadata_owner, class_name: "MetadataFile", dependent: :destroy
    accepts_nested_attributes_for :metadata_files, :allow_destroy => true
  end

  def metadata_files=(attrs)
    attrs.each { |attr| self.metadata_files.build(metadata_file: attr )}
  end

end

