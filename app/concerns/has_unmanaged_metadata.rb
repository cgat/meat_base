require 'active_support/concern'
#Unmanaged Metadata refers to metadata files and folders that are associated with an object (currently survey, survey_season, and projects),
#but are not explicitly managed by this application. Specifically, including this module to a filesystem active record
#gives the applications the ability to track a "Metadata" folder, which can hold unmanaged content. This allows us to handles the
#rather disorganized contents within the Metadata folders in the old MLP Library file structure. A user can add, edit, delete, and view content from this
#folder using Finder (or any other file manager).
module HasUnmanagedMetadata
  extend ActiveSupport::Concern
  included do
      #this handles the rare case where a has_unmanaged_metadata object has
      #had a unmanaged metadata folder created, but does not have any other
      #associated files or subfolder (like capture images). Normally, the clean_up
      #of a container folder is done by first deleting its dependent files (dependent destroy)
      #then recursively cleaning up the parent folders if they are empty.
      after_destroy :clean_up, if: 'Dir.exists?(umetadata_path)'
  end

  def open_metadata_folder
    if Dir.exists?(umetadata_path)
      `open "#{umetadata_path}"`
    else
      FileUtils.mkpath umetadata_path
      `open "#{umetadata_path}"`
    end
  end

  def create_umetadata_folder
    FileUtils.mkpath(File.join(self.filesystem_path,"Metadata"))
  end

  def umetadata_path
    return File.join(self.filesystem_path,"Metadata")
  end

  def umetadata_empty?
    #folder is empty when it has no file or folder entries OR if the only files are hidden files created
    #by the file system (prefixed with .)
    Dir.entries(umetadata_path).reject{|i| i=~/^\./ }.empty?
  end

  def clean_up
    FileUtils.rm_rf(umetadata_path)
    clean(self.filesystem_path)
  end
end

