require 'active_support/concern'


module Coordinateable
  extend ActiveSupport::Concern

  NUM_DECIMAL_PLACES = 6

  included do
    attr_accessible :lat_d, :lat_m, :lat_s, :lat_dir, :long_d, :long_m, :long_s, :long_dir, :lat, :long, :elevation
    attr_accessible :easting, :northing, :zone
    attr_accessor :lat_d, :lat_m, :lat_s, :lat_dir, :long_d, :long_m, :long_s, :long_dir
    attr_accessor :easting, :northing, :zone
    attr_accessor :coord_input_mode

    before_validation :set_coord_input_mode
    before_validation :convert_coordinates_to_numbers
    before_validation :convert_coordinates_to_decimal_degree, if: :dms_or_mindec?
    before_validation :convert_UTM_to_decimal_degree, if: :utm?
    after_initialize :load_DMS_variables, :set_coord_input_mode
    with_options if: :dms_or_mindec? do |notDecimal|
      notDecimal.validate  :known_format_long
      notDecimal.validate  :known_format_lat
      notDecimal.validates :lat_s, numericality: { less_than: 60, greater_than_or_equal_to: 0, message: "Latitude seconds can be equal or greater than 0 and less than 60"}, allow_blank: true
      #this is a custom validator, located in config/initializers/number_of_decimals_validator.rb
      notDecimal.validates_num_decimals :lat_s, max_decimals_places: NUM_DECIMAL_PLACES, if: :lat_DMS?
      notDecimal.validates :long_s, numericality: { less_than: 60, greater_than_or_equal_to: 0, message: "Longitude seconds can be equal or greater than 0 and less than 60"}, allow_blank: true
      notDecimal.validates_num_decimals :long_s, max_decimals_places: NUM_DECIMAL_PLACES, if: :long_DMS?
      notDecimal.validates :lat_m, numericality: { less_than: 60, greater_than_or_equal_to: 0, message: "Latitude minutes can be equal or greater than 0 and less than 60"}, allow_blank: true
      notDecimal.validates_num_decimals :lat_m, max_decimals_places: NUM_DECIMAL_PLACES, if: :lat_MinDec?
      notDecimal.validates :long_m, numericality: { less_than: 60, greater_than_or_equal_to: 0, message: "Longitude minutes can be equal or greater than 0 and less than 60"}, allow_blank: true
      notDecimal.validates_num_decimals :lat_m, max_decimals_places: NUM_DECIMAL_PLACES, if: :lat_MinDec?
      notDecimal.validates :lat_d, numericality: {  less_than_or_equal_to: 90, greater_than_or_equal_to: 0, message: "Latitude degrees can be between 0 and 90 (inclusive)"}, allow_blank: true
      notDecimal.validates :long_d, numericality: {  less_than_or_equal_to: 180, greater_than_or_equal_to: 0, message: "Longitude degrees can be between 0 and 180 (inclusive)"}, allow_blank: true
    end
    with_options if: :utm? do |utm|
      utm.validates_format_of :zone, with: /\d{2}[a-zA-Z]/
      utm.validates :easting, numericality: {integer_only: true}
      utm.validates :northing, numericality: {integer_only:true, greater_than: 5400000, less_than: 7000000}
    end
    validates :elevation, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 12000}, allow_blank: true
    validates :lat, numericality: {  less_than_or_equal_to: 90.0, greater_than_or_equal_to: -90.0}, allow_blank: true
    validates :long, numericality: { less_than_or_equal_to: 180.0, greater_than_or_equal_to: -180.0}, allow_blank: true
    validates :lat, presence: true, if: 'self.long.present?'
    validates :long, presence: true, if: 'self.lat.present?'
  end


=begin
We have three forms of submitting coordinates: via the lat and long attributes (which are associated with db columns),
the lat_d,lat_m,lat_s, and lat_d (respectively with long_d, long_m...) for DMS and MinDec, or UTM coords. To distinguish between these
modes, we assume that the mode of entry is using lat long if there has been any change in these attributes, otherwise, use
the other inputs, which will be converted to lat and long for db storage. UTM coords are not supported at the user interface level
=end
  def set_coord_input_mode
    if self.lat_changed? || self.long_changed?
      @coord_input_mode = :LAT_LONG_DECIMAL
    elsif self.zone.present? && self.easting.present? && self.northing.present?
      @coord_input_mode = :UTM
    else
      @coord_input_mode = :LAT_LONG_DMS
    end
  end

  def convert_DMS_to_decimal_degree(deg,min,sec,dir)
    total_seconds = sec + min*60
    fractional = total_seconds/3600.0
    deg_decimal = deg + fractional
    deg_decimal = dir == 2 ? -deg_decimal : deg_decimal
    return deg_decimal
  end

  def convert_MinDec_to_decimal_degree(deg,min,dir)
    fractional = min/60.0
    deg_decimal = deg + fractional
    deg_decimal = dir == 2 ? -deg_decimal : deg_decimal
    return deg_decimal
  end

  def convert_UTM_to_decimal_degree
    latlong = GeoUtm::UTM.new(self.zone,self.easting,self.northing).to_lat_lon
    self.lat = latlong.lat
    self.long = latlong.lon
  end

  def get_dms(coord)
     #check coord for nil
     if coord.nil?
       return nil
     end
     dir = coord>0 ? 1 : 2
     #if negative, make coord positive because +/- part is associated with the cardinal direction of the Degree/Minute/Second coord format
     coord = coord>0 ? coord : -coord
     deg = int_part(coord)
     min_float = 60*(coord-deg)
      min = int_part(min_float)
      sec_float = 60*(min_float-min)
      sec_float = sec_float.round(NUM_DECIMAL_PLACES)
      if sec_float == 60
        sec_float = 0
        min += 1
      end
      if min == 60
        min = 0
        deg += 1
      end
     return [deg,min,sec_float,dir]
   end

   def get_degree(coord)
     values = get_dms(coord)
     values[0]
   end

   def get_minute(coord)
     values = get_dms(coord)
     values[1]
   end

   def get_second(coord)
     values = get_dms(coord)
     values[2]
   end

   def get_dir(coord)
     values = get_dms(coord)
     values[3]
   end

   def lat_formatted_DMS
     cardinal_directions=['N','S']
     dir = cardinal_directions[get_dir(self.lat)-1] if !self.lat.nil?
     if self.lat.nil?
       return nil
     else
       "#{get_degree(self.lat).to_s}&deg;#{get_minute(self.lat).to_s}&acute;#{get_second(self.lat).to_s}&acute;&acute;#{dir}".html_safe
     end
   end

   def long_formatted_DMS
     cardinal_directions=['E','W']
     dir = cardinal_directions[get_dir(self.long)-1] if !self.long.nil?
     if self.long.nil?
       return nil
     else
       "#{get_degree(self.long).to_s}&deg;#{get_minute(self.long).to_s}&acute;#{get_second(self.long).to_s}&acute;&acute;#{dir}".html_safe
     end
   end

  private

  #converts user specified coordinates from string (as they are received from the browser) to integers or floats.
  #If they are neither an integer or a float, then leave and let validation take car of it.
  def convert_coordinates_to_numbers
    self.lat = self.lat.to_f if float?(self.lat)
    self.lat = self.lat.to_i if integer?(self.lat)
    self.lat_d = self.lat_d.to_f if float?(self.lat_d)
    self.lat_d = self.lat_d.to_i if integer?(self.lat_d)
    self.lat_m = self.lat_m.to_f if float?(self.lat_m)
    self.lat_m = self.lat_m.to_i if integer?(self.lat_m)
    self.lat_s = self.lat_s.to_f if float?(self.lat_s)
    self.lat_s = self.lat_s.to_i if integer?(self.lat_s)
    self.lat_dir = self.lat_dir.to_f if float?(self.lat_dir)
    self.lat_dir = self.lat_dir.to_i if integer?(self.lat_dir)
    self.long = self.long.to_f if float?(self.long)
    self.long = self.long.to_i if integer?(self.long)
    self.long_d = self.long_d.to_f if float?(self.long_d)
    self.long_d = self.long_d.to_i if integer?(self.long_d)
    self.long_m = self.long_m.to_f if float?(self.long_m)
    self.long_m = self.long_m.to_i if integer?(self.long_m)
    self.long_s = self.long_s.to_f if float?(self.long_s)
    self.long_s = self.long_s.to_i if integer?(self.long_s)
    self.long_dir = self.long_dir.to_f if integer?(self.long_dir)
    self.long_dir = self.long_dir.to_i if integer?(self.long_dir)
  end

  #gets the coordinate format given a degree, minute, second, and cordinal dir
  def get_coord_format(degree,minute,second,cardinal_dir)
    if integer?(degree) && integer?(minute) && (integer?(second) || float?(second)) && integer?(cardinal_dir)
      return :DMS
    elsif integer?(degree) && (integer?(minute) || float?(minute)) && second.blank? && integer?(cardinal_dir)
      return :MinDec
    elsif (integer?(degree) || float?(degree)) && minute.blank? && second.blank? && integer?(cardinal_dir)
      return :Decimal
    else
      return :Unknown
    end
  end

  # #this is used as a conditional in validations.
  #   #If either the lat or the long attributes have been changed, it returns false.
  #   #In a coordinateable object, we potentially have multiple ways to input coordinates (DMS or lat long directly)
  #   #To deal the situation where both forms of input are entered into our system, we will always choose the base lat and long changes
  #   #if they have been changed. This methods allows us to ignore certain functions that are related to the other forms of input.
  #   def lat_long_unchanged?
  #     return !self.lat_changed? && !self.long_changed?
  #   end
  def dms_or_mindec?
    return self.coord_input_mode==:LAT_LONG_DMS
  end

  def utm?
    return self.coord_input_mode==:UTM
  end
  def lat_DMS?
    return get_coord_format_lat == :DMS
  end

  def long_DMS?
    return get_coord_format_long == :DMS
  end

  def lat_MinDec?
    return get_coord_format_lat == :MinDec
  end

  def long_MinDec?
    return get_coord_format_long == :MinDec
  end

  def get_coord_format_lat
    get_coord_format(self.lat_d,self.lat_m,self.lat_s,self.lat_dir)
  end

  def get_coord_format_long
    get_coord_format(self.long_d,self.long_m,self.long_s,self.long_dir)
  end

  def convert_coordinates_to_decimal_degree
    case get_coord_format(self.long_d,self.long_m,self.long_s,self.long_dir)
    when :DMS
      self.long = convert_DMS_to_decimal_degree(self.long_d,self.long_m,self.long_s,self.long_dir)
    when :MinDec
      self.long = convert_MinDec_to_decimal_degree(self.long_d,self.long_m,self.long_dir)
    end
    case get_coord_format(self.lat_d,self.lat_m,self.lat_s,self.lat_dir)
    when :DMS
      self.lat = convert_DMS_to_decimal_degree(self.lat_d,self.lat_m,self.lat_s,self.lat_dir)
    when :MinDec
      self.lat = convert_MinDec_to_decimal_degree(self.lat_d,self.lat_m,self.lat_dir)
    end
  end

  def load_DMS_variables
     if self.has_attribute?(:lat) && !self.lat.nil?
       self.lat_d = get_degree(self.lat)
       self.lat_m = get_minute(self.lat)
       self.lat_s = get_second(self.lat)
       self.lat_dir = get_dir(self.lat)
     end
     if self.has_attribute?(:long) && !self.long.nil?
       self.long_d = get_degree(self.long)
       self.long_m = get_minute(self.long)
       self.long_s = get_second(self.long)
       self.long_dir = get_dir(self.long)
     end
   end

   def long_inputs_present?
      return  self.long_d.present? || self.long_m.present? || self.long_s.present?
    end

   def known_format_long
     if long_inputs_present? && get_coord_format(self.long_d,self.long_m,self.long_s,self.long_dir) == :Unknown
       errors[:base] << "Longitude is not correctly formatted. Degrees can only be positive integers. Minutes must be positve, and can only contain decimals if seconds are blank. Seconds, if used, must be positive and may contain up to 6 decimal places."
     end
   end

    def lat_inputs_present?
      return  self.lat_d.present? || self.lat_m.present? || self.lat_s.present?
    end

   def known_format_lat
     if lat_inputs_present? && get_coord_format(self.lat_d,self.lat_m,self.lat_s,self.lat_dir) == :Unknown
       errors[:base] << "Latitude is not correctly formatted. Degrees can only be positive integers. Minutes must be positve, and can only contain decimals if seconds are blank. Seconds, if used, must be positive and may contain up to 6 decimal places."
     end
   end

end


