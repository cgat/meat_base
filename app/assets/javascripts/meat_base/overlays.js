//classcompare functionality
$(window).load(function() {
    setup_classy_compare();
});

$(document).on('pjax:end', function() {
  $('.comparison_before_after').imagesLoaded(setup_classy_compare);
});

$(document).ready(function() {
  $('.toggle_nav').on('click', function() {
    setTimeout(function() {
      $('.comparison_before_after').attr('style','');
      $('.comparison_before_after *').attr('style','');
      setup_classy_compare();
    }, 400);
  })
});

var setup_classy_compare = function() {
  $('.comparison_before_after').ClassyCompare({
        leftgap: 10,
        rightgap: 10,
        caption: false,
        reveal: 0.5
    });
}
