//= require_self
//= require meat_base/overlays
//= require meat_base/thumbnails
//= require meat_base/underscore-min
//= require meat_base/imagesloaded.pkgd
//= require meat_base/jquery.pjax
//= require gmaps/google
//= require meat_base/jquery.classycompare
//= require meat_base/keyboard_navigation

var pjax_functionality = function(){
  $(document).pjax('a:not([data-remote]):not([data-behavior]):not([data-skip-pjax])','[data-pjax-container]');
  //by setting timeout to false, we'll wait indefinitely until the page loads
  $(document).on("pjax:timeout", function() { return false; });
  $(document).on('pjax:success', sizing);
}

//This function sizes the pane-container, nav-pane, and data-pane
//to a size that extends to the bottom of the window.
function sizing() {
  var pane_container_offset=parseInt($("#pane_container").css('top'),10);
  var nav_navbarheight=$("#nav-pane .submenu_bar").outerHeight(true);
  var data_navbarheight = $('#data-pane .submenu_bar').outerHeight(true);
  var nav_pane_container=$(window).height()-pane_container_offset-nav_navbarheight;
  var data_pane_container = $(window).height()-pane_container_offset-data_navbarheight;
  sizing_search_pane(nav_pane_container);
  sizing_map_pane(nav_pane_container);
  sizing_tree_pane(nav_pane_container);
  var data_pane_extras = $('.data-pane-body').outerHeight(true)-$('.data-pane-body').height();
  $("#data-pane .data-pane-body").height((data_pane_container-data_pane_extras)+"px");
}
function sizing_search_pane(pane_container) {
  var search_extras = $('#search-results').outerHeight(true)-$('#search-results').height();
  $("#nav-pane #search-results").height((pane_container-search_extras)+"px");
}
function sizing_map_pane(pane_container) {
  var pane_info=$("#nav-pane .pane_info").outerHeight(true);
  var map_filter_container = $('#nav-pane .map_filter_container').outerHeight(true);
  var map_extras = $('.map_container').outerHeight(true)-$('.map_container').height();
  //parseInt($('.map_container').css('padding'),10)*2+parseInt($('.map_container').css('border-top'),10)*2;
  var map_height = pane_container-pane_info-map_extras - map_filter_container;
  $("#nav-pane .map_container").height(map_height+"px");
  $("#nav-pane .gmaps4rails_map").height(map_height+"px");
}
function sizing_tree_pane(pane_container) {
  var tree_extras = $('#tree_body').outerHeight(true)-$('#tree_body').height();
  $("#nav-pane #tree_body").height((pane_container-tree_extras)+"px");
}

$(document).ready(sizing);
$(document).ready(pjax_functionality);
$(window).resize(sizing);

var toggle_nav = function() {
  $('#nav-pane').toggleClass('span5 span1');
  $('#data-pane').toggleClass('span7 span11');
  if ( $('.collapsed_overlay').css('display') == 'none' ) {
    $('#nav-pane').css('white-space', 'nowrap');
    $('#nav-pane').css('overflow-x', 'scroll');
    $('#nav-pane .submenu_bar .navbar .navbar-inner > .container').hide();
    setTimeout(function() {
      $('.collapsed_overlay').css('display','block');
    }, 300);

  }
  else {
    $('.collapsed_overlay').css('display','none');
    setTimeout(function() {
      $('.submenu_bar .navbar .navbar-inner > .container').show();
      $('#nav-pane').css('white-space', '');
      $('#nav-pane').css('overflow-x', '');
    }, 300);
  }
}
$(document).ready(function() {
  $('#toggle_nav_close').on('click', toggle_nav);
  $('#toggle_nav_open').on('click', toggle_nav);
});





