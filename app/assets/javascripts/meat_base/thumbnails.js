
function setup_thumbnail_adjustment() {
  $('ul.thumbnails:visible').each(function() {
    elem = $(this);
    //wait for images to load because otherwise they won't have the correct height
    imagesLoaded(elem, center_thumbnail_images);
  });
}

function setup_thumbnail_adjustment_on_tabs() {
  $('a[data-toggle="tab"]').on('shown', function (e) {
    var tab_pane_id = $(e.target).attr('href');
    var tab_pane_element = $(tab_pane_id);
    tab_pane_element.find('ul.thumbnails:visible').each(function() {
      elem = $(this);
      //wait for images to load because otherwise they won't have the correct height
      imagesLoaded(elem, center_thumbnail_images);
    });
  });
}

function center_thumbnail_images(instance) {
  $.each(instance.images, function(index, item) {
      var img = $(item.img);
      var width, height;
      width = img.width();
      height = img.height();
      if (width!=undefined && height!=undefined && img.hasClass('thumbnail_img')) {
        if(isLandscape(width,height)) {
          img.css('margin-left', '-25px');
        }
        else {
          img.css('margin-top', '-25px');
        }
      }
  });
}

function isLandscape(width, height) {
  return width>height;
}

$(document).ready(setup_thumbnail_adjustment);
$(document).ready(setup_thumbnail_adjustment_on_tabs);
$(document).on('pjax:success', setup_thumbnail_adjustment);
 $(document).on('pjax:success', setup_thumbnail_adjustment_on_tabs);


