if( typeof(google) != 'undefined' && typeof(gon) != 'undefined' && typeof(gon.map) != 'undefined' && gon.map.length>0) {
  (function() {
    //currentMarker needs to be declared up here so that it gets bound to the marker click functions.
    //this makes swapping icons on click simple
    var currentMarker, handler;
    google.maps.event.addDomListener(window, 'load', function() {
        var filter = { surveyor: null, survey: null, year: null};
        //remove clusterer: undefined to add clustering (also you need to add the markercluster script, see _map.html)
        handler = Gmaps.build('Google', { markers: { maxRandomDistance: null, clusterer: undefined } });
        handler.buildMap({ provider: {mapTypeId: google.maps.MapTypeId.TERRAIN}, internal: {id: 'map'}}, function(){
          markers_json = gon.map;
          markers = _.map(markers_json, function(marker_json){
            marker = handler.addMarker(marker_json);
            _.extend(marker, marker_json);
            return marker;
          });
          for (var i = 0; i <  this.markers.length; ++i) {
              marker = markers[i]
              if (marker.current==true) {
                currentMarker = marker;
              }
              google.maps.event.addListener(
               marker.serviceObject, 'click', (function(i) {
                  return function() {
                    var marker = markers[i];
                    google.maps.event.clearListeners(map, 'bounds_changed');
                    //This will replace the currentMarker's icon with a red marker
                    //and maker the clicked marker the current one (blue marker)
                    if (typeof(currentMarker) == 'undefined' || currentMarker.link!=marker.link) {
                      //if the currentMarker is defined, then we need to swap the icon out
                      if(typeof(currentMarker) != 'undefined' && currentMarker.repeated) {
                        currentMarker.serviceObject.setIcon(gon.red_map_marker_path);
                        currentMarker.serviceObject.setZIndex(undefined);
                        currentMarker.current = false;
                      }
                      else {
                        currentMarker.serviceObject.setIcon(gon.gray_map_marker_path);
                        currentMarker.serviceObject.setZIndex(undefined);
                        currentMarker.current = false;
                      }
                      marker.serviceObject.setIcon(gon.blue_map_marker_path);
                      marker.serviceObject.setZIndex(99999);
                      marker.current = true;
                      currentMarker = marker;
                    }
                    //we make the request after 200 milliseconds. This makes for a
                    //smoother transition when replacing icons above, otherwise
                    // there is a distinct 'flash' on the marker icon when it is replace
                    // (it disappears and then reappears)
                    setTimeout( function() {
                      if ($.pjax !== undefined) {
                        $.pjax({url: marker.link, container: '[data-pjax-container]'})
                      }
                      else {
                        window.location.href = marker.link;
                      }
                    }, 200);

                  }
              })(i));
            }
            if(typeof(currentMarker) != 'undefined') {
              currentMarker.serviceObject.setZIndex(99999);
              handler.map.centerOn([currentMarker.lat, currentMarker.lng]);
              handler.map.serviceObject.setZoom(8);
              handler.fitMapToBounds();
            }
            else {
              handler.bounds.extendWith(markers);
              handler.fitMapToBounds();
            }
            filterOptions();
            filterDisable();
        });
        $('#surveyor_select').on('change', function() {
          if($(this).val()==null || $(this).val()==='') {
            filter.surveyor = null;
          }
          else {
            filter.surveyor = $(this).val();
          }
          filter.survey = null;
          filter.year = null;
          filterMarkers();
          filterOptions();
          filterDisable();
        });
        $('#survey_select').on('change', function() {
          if($(this).val()==null || $(this).val()==='') {
            filter.survey = null;
          }
          else {
            filter.survey = $(this).val();
          }
          filter.year = null;
          filterMarkers();
          filterOptions();
          filterDisable();
        });
        $('#year_select').on('change', function() {
          if($(this).val()==null || $(this).val()==='') {
            filter.year = null;
          }
          else {
            filter.year = parseInt($(this).val());
          }
          filterMarkers();
          filterDisable();
        });
        $('#reset_select').on('click', function(event) {
          event.preventDefault();
          filter.surveyor = null;
          filter.survey = null;
          filter.year = null;
          filterMarkers();
          filterOptions();
          filterDisable();
        });
        function filterMarkers() {
          _.each(markers, function(marker) {
            var surveyorMatch = false, surveyMatch = false, yearMatch = false;
            if(filter.surveyor==null || marker.surveyor === filter.surveyor) {
              surveyorMatch = true;
            }
            if(filter.survey==null || marker.survey === filter.survey) {
              surveyMatch = true;
            }
            if(filter.year==null || marker.historic_year === filter.year) {
              yearMatch = true;
            }
            marker.serviceObject.setVisible(surveyorMatch && surveyMatch && yearMatch);
          });
        }
        function filterDisable() {
          if(!filter.surveyor && !filter.survey && !filter.year) {
            $('#surveyor_select').prop('disabled', false);
            $('#survey_select').prop('disabled', 'disabled');
            $('#year_select').prop('disabled', 'disabled');
          }
          else if(filter.surveyor && !filter.survey && !filter.year) {
            $('#surveyor_select').prop('disabled', false);
            $('#survey_select').prop('disabled', false);
            $('#year_select').prop('disabled', 'disabled');
          }
          else if(filter.surveyor && filter.survey && !filter.year) {
            $('#surveyor_select').prop('disabled', false);
            $('#survey_select').prop('disabled', false);
            $('#year_select').prop('disabled', false);
          }
        }
        function filterOptions() {
          //add filter options to the select menus
            var yearsOptions = '<option value=""' +
              (filter.historic_year ? '' : 'selected')
              + '>By Historic Year</option>';
            _.chain(markers)
                .sortBy(function(marker) { return marker.historic_year; })
                .select(function(marker) {
                  if(filter.survey) {
                    return marker.survey === filter.survey &&
                      marker.surveyor === filter.surveyor;
                  }
                  else {
                    return true;
                  }
                })
                .uniq(true, function(marker) {
                  return marker.historic_year;
                })
                .each(function(marker) {
                  if(marker.historic_year) {
                    yearsOptions += '<option value="' +
                      marker.historic_year + '" '+
                      (filter.historic_year===marker.historic_year ? 'selected' : '') + '>' +
                      marker.historic_year + '</option>';
                  }
                });
            $('#year_select').html(yearsOptions);

            var surveysOptions = '<option value=""' +
              (filter.survey ? '' : 'selected')
              + '>By Survey</option>';
            _.chain(markers)
                .sortBy(function(marker) { return marker.survey; })
                .select(function(marker) {
                  if(filter.surveyor) {
                    return marker.surveyor === filter.surveyor;
                  }
                  else {
                    return true;
                  }
                })
                .uniq(true, function(marker) {
                  return marker.survey;
                })
                .each(function(marker) {
                  if(marker.survey) {
                    surveysOptions += '<option ' +
                      (filter.survey === marker.survey ? 'selected' : '')
                      + ' value="' + marker.survey + '">' +
                      marker.survey + '</option>';
                  }
                });
            $('#survey_select').html(surveysOptions);

            var surveyorsOptions = '<option '+
            (filter.surveyor ? '' : 'selected')
            +' value="">By Surveyor</option>';
            _.chain(markers)
                .sortBy(function(marker) {
                  return marker.surveyor;
                })
                .uniq(true, function(marker) {
                  return marker.surveyor;
                })
                .each(function(marker) {
                  if(marker.surveyor) {
                    surveyorsOptions += '<option '+
                    (filter.surveyor==marker.surveyor ? 'selected' : '')
                    +' value="' + marker.surveyor + '">' +
                      marker.surveyor_full_name + '</option>';
                  }
                });
            $('#surveyor_select').html(surveyorsOptions);

            google.maps.event.addDomListener(window, "resize", function() {
              google.maps.event.trigger(handler.getMap(), "resize");
            });
        }

    });

  })();


}

