(function(){
  $(document).keydown(function(e){
    var rightArrow = 39;
    var leftArrow = 37;
    if(e.which==rightArrow){
      $('#next_link').click();
    }
    else if(e.which==leftArrow) {
      $('#prev_link').click();
    }
  });
})();

