$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "meat_base/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "meat_base"
  s.version     = MeatBase::VERSION
  s.authors     = ["Christopher Gat"]
  s.email       = ["chris.gat@gmail.com"]
  s.homepage    = "http://www.mountainlegacy.ca"
  s.summary     = "Provides a common set of models used in organizing repeat photography datasets."
  s.description = "Provides a common set of models used in organizing repeat photography datasets."
  s.files = Dir["{app,config,db,lib,vendor}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["spec/**/*"]

  s.add_dependency "rails", "~>4.2.0"
  s.add_dependency 'carrierwave', "~> 0.11.0"
  s.add_dependency 'geoutm', '~> 1.0.1'
  s.add_dependency 'uuidtools', '~> 2.1.3'
  s.add_dependency 'pg', '~> 0.18.0'
  s.add_dependency 'mini_exiftool', '~> 2.4.0'
  s.add_dependency 'fog', '~> 1.38.0'
  s.add_dependency 'nokogiri', '~>1.5'
  s.add_dependency 'mime-types', '~> 1.22'
  s.add_dependency 'pg_search', '1.0.5'
  s.add_dependency 'gmaps4rails', '~> 2.0.3'
  s.add_dependency 'rack-pjax', '~> 0.7.0'
  s.add_dependency 'gon', '~> 5.0.4'
  s.add_dependency 'protected_attributes'

  s.add_development_dependency 'rspec-rails', '~> 2.12'
  s.add_development_dependency 'factory_girl_rails', '~> 4.2'
  s.add_development_dependency 'shoulda', '~> 3.5.0'
  s.add_development_dependency 'capybara', '~> 2.2'
  s.add_development_dependency 'byebug', '~> 9.0'
  s.add_development_dependency 'selenium-webdriver', '~> 2.53'
  s.add_development_dependency 'test-unit'
end
